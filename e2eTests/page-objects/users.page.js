import Page from "./page"
import { expect } from 'chai'
import testData from "../constants/testData.json"
import Accounts from "../page-objects/accounts.page";
import tmdRecipant from "./tmdadminrecipant.page"
import tmdAdmin from "./tmdadmin.page"

class users extends Page{
    get accountGroupTab(){
        return $("//a[contains(text(),'Account Groups')]")
    };

    get groupDropdown(){
        return $("#user_account_group_form .form__group:nth-child(4) .choices.choices--single-select .choices__inner")
    };

    addGroupToUser(groupname){
        Accounts.addKiidApprovalBtn.waitForVisible();
        Accounts.addKiidApprovalBtn.click();
        Accounts.addApprovalHeading.waitForVisible();
        expect(Accounts.addApprovalHeading.isVisible()).to.eql(true);
        this.groupDropdown.waitForVisible();
        this.groupDropdown.click();
        Accounts.accountDropdown.waitForVisible();
        browser.setValue(".choices__list.choices__list--dropdown .choices__input.choices__input--cloned",[groupname,'Enter']);
        tmdRecipant.clientSaveBtn.waitForVisible();
        tmdRecipant.clientSaveBtn.click();
    };

};
export default new users();