import Page from "./page"
import testData from "../constants/testData.json"
import { expect } from 'chai'
import Login from "./login.page"
import { waitUntilTrueFalse } from "../page-objects/function";

let i;

class tmdrecordlisting extends Page{
    get kiidsLink(){
        return $(".page-header .col-sm-12.col-lg-auto.ml-lg-auto .nav-item:nth-child(1)")
    }

    getDisseminationLists(index){
        return $(`.page-header .col-sm-12.col-lg-auto .nav-item:nth-child(${index})`)
    }

    get disseminationHistory(){
        return $(".page-header .col-sm-12.col-lg-auto.ml-lg-auto .nav-item:nth-child(3)")
    }

    get zeidlerDropdown(){
        return $(".page-header .container .col-sm-12.col-lg-auto .dropdown-toggle")
    }

    get searchField(){
        return $(".dropdown-choices__menu input")
    }

    get downloadBtn(){
        return $(".page-action.d-none.d-lg-inline:nth-child(1)")
    }

    get approveBtn(){
        return $(".page-action.d-none.d-lg-inline:nth-child(2)")
    }

    get reviewBtn(){
        return $(".page-action.page-action--preview.col-12.col-lg-auto")
    }

    get fundNameFilter(){
        return $(".filters-container .filter.dropdown:nth-child(1) .btn-dark.dropdown-toggle--no-caret")
    }

    getKiidSubfundFilterOption(index){
        return $(`.dropdown-item.sub-fund-filter-item.filter-item:nth-child(${index})`)
    }

    get kiidRecordSubfund(){
        return $("#kiid_list_page > div:nth-child(2) .table__cell--sub-fund")
    }

    getFilterBtn(index){
        return $(`.filters-container>div:nth-child(${index}) .btn span`)
    }

    get kiidIsinFilterOption(){
        return $("#isin_filter_list .dropdown-item.name-filter-item:nth-child(1)")
    }

    get kiidRecordIsin(){
        return $("#kiid_list_page > div:nth-child(2) .table__cell--isin.col")
    }

    get nameFilterBtn(){
        return $(".filters-container>div:nth-child(3) .btn span")
    }

    get nameSearchField(){
        return $("#share_class_search_input")
    }

    getKiidNameFilterOption(index){
        return $(`#name_filter_list a:nth-child(${index})`)
    }

    get kiidRecordName(){
        return $("#kiid_list_page > div:nth-child(2) .table__cell--name.col")
    }

    get currencyFilterBtn(){
        return $(".filters-container>div:nth-child(4) .btn span")
    }

    get currencyFilterUsdCheckbox(){
        return $(".dropdown-menu--large.show .dropdown-menu__inner-scroll")
    }

    get currencyFilterApplyBtn(){
        return $(".dropdown-menu--large.show .dropdown-item--apply.currency-item--apply")
    }

    get kiidRecordUsd(){
        return $("#kiid_list_page > div:nth-child(2) .table__cell--currency.col")
    }

    get languageFilterBtn(){
        return $(".page-actions-accordion--filters.collapse.show .filter.dropdown:nth-child(1)")
    }

    get languageFilterFreCheckbox(){
        return $(".dropdown-menu--large.show .dropdown-item--check:nth-child(1)")
    }

    get LanguageFilterEngCheckbox(){
        return $(".dropdown-menu--large.show .dropdown-item--check:nth-child(2)")
    }

    get LanguageFilterApplyBtn(){
        return $(".dropdown-menu--large.show .dropdown-item--apply.language-item--apply")
    }

    get kiidRecordLanguageFre(){
        return $("//div[contains(text(),'FRE')]")
    }

    get kiidRecordLanguageEng(){
        return $("//div[contains(text(),'ENG')]")
    }

    get statusFilterBtn(){
        return $(".filters-container>div:nth-child(5) .btn span")
    };

    getKiidStatusFilterOption(index){
        return $(`.dropdown-item.status-filter-item:nth-child(${index})`)
    };

    getKiidRecordStatus(index){
        return $(`#kiid_list_page > div:nth-child(${index}) .table__cell--status.col`)
    };

    get countryFilterBtn(){
        return $(".page-actions-accordion--filters.collapse.show .filter.dropdown:nth-child(2)")
    }

    get kiidCountryBeFilterOption(){
        return $(".dropdown-menu--large.show .dropdown-item--check:nth-child(1)")
    }

    get kiidCountryIeFilterOption(){
        return $(".dropdown-menu--large.show .dropdown-item--check:nth-child(2)")
    }

    get kiidCountryGbFilterOption(){
        return $(".dropdown-menu--large.show .dropdown-item--check:nth-child(3)")
    }

    get kiidCountryMtFilterOption(){
        return $(".dropdown-menu--large.show .dropdown-item--check:nth-child(4)")
    }

    get kiidCountryLuFilterOption(){
        return $(".dropdown-menu--large.show .dropdown-item--check:nth-child(5)")
    }

    get kiidCountryApplyBtn(){
        return $(".dropdown-menu--large.show .dropdown-item--apply.country-item--apply")
    }

    get kiidCountryBeRecord(){
        return $("//div[contains(text(),'BE')]")
    }

    get kiidCountryRecord(){
        return $("//div[contains(text(),'GB, IE, MT, LU')]")
    }

    get filterDropdown(){
        return $(".filter-accordion-toggle.d-none.d-lg-inline")
    }

    get subFundCheckbox(){
        return $(".table__header .table__row .table__cell--select.col.is-switch .custom-checkbox")
    }

    get zeidlerQASubFundCheckbox(){
        return $("#kiid_list_page > div:nth-child(2) .custom-checkbox")
    }

    get zeidlerQASubFundCheckboxs(){
        return $("#kiid_list_page > div:nth-child(3) .custom-checkbox")
    }

    getReviewIcon(index){
        return $(`#kiid_list_page .table__row--outer:nth-child(${index}) .action-group .action--arrow-right`)
    };
    
    get readNoteIcon(){
        return $("#kiid_list_page .table__row--outer:nth-child(2) .action-group .action--read-note")
    }

    getCreateNoteIcon(index){
        return $(`#kiid_list_page .table__row--outer:nth-child(${index}) .action-group .action--create-note`)
    }

    get downloadIcon(){
        return $("#kiid_list_page .table__row--outer:nth-child(2) .action-group .action--download")
    }

    get subFundCrossIcon(){
        return $(".page-action--filters .filter.dropdown:nth-child(1) .icon--x-small")
    }

    get isinCrossIcon(){
        return $(".page-action--filters .filter.dropdown:nth-child(2) .icon--x-small")
    }

    get nameCrossIcon(){
        return $(".page-action--filters .filter.dropdown:nth-child(3) .icon--x-small")
    }

    get currencyCrossIcon(){
        return $(".page-action--filters .filter.dropdown:nth-child(4) .icon--x-small")
    }

    getStatusCrossIcon(index){
        return $(`.page-action--filters .filter.dropdown:nth-child(${index}) .icon--x-small`)
    }

    get languageCrossIcon(){
        return $("#pageActionsMore .filter.dropdown:nth-child(1) .icon--x-small")
    }

    get countryCrossIcon(){
        return $("#pageActionsMore .filter.dropdown:nth-child(2) .icon--x-small")
    }

    get createNoteIcon(){
        return $("#kiid_list_page .table__row--outer:nth-child(2) .action--create-note")
    }

    get noteField(){
        return $("#kiid_approval_detail_comment_comment")
    }

    get saveNoteBtn(){
        return $(".form-group .btn.btn-primary:nth-child(1)")
    }

    get zeidlerReviewBtn(){
        return $(".form-group .btn.btn-primary:nth-child(2)")
    }

    getApprovalCreateIcon(index){
        return $(`.card--accordion-list:nth-child(${index}) .action`)
    }

    get backLink(){
        return $(".text-primary")
    }

    get zeilderUmHeading(){
        return $(".btn__content.choices__item.choices__item--selectable")
    };

    get approvalPageHeading(){
        return $("h1.h5.page-header__title")
    };

    getGroupLabelName(index,index_1,index_2){
        return $(`#Approval:nth-child(${index}) .container .row .card--accordion-list:nth-child(${index_1}) .text-left li:nth-child(${index_2}) .card__title`)
    };
    
    getApproveBtn(index,index_1,index_2){
        return $(`#Approval:nth-child(${index}) .container .row .card--accordion-list:nth-child(${index_1}) .text-right li:nth-child(${index_2})`)
    };

    get noApprovalTitle(){
        return $(".card__header")
    };

    get approvalTab(){
        return $("//a[contains(text(),'APPROVAL')]")
    };

    getBulkApprovalLabel(index){
        return $(`.modal-content .modal__list--flex-space-list .modal__list-item:nth-child(${index}) h4.mb-0`)
    };

    getBulkApproveDisableBtn(index){
        return $(`.modal-content .modal__list--flex-space-list .modal__list-item:nth-child(${index}) .enable-disable-approval-btn`)
    };

    getBulkApproveEnabledBtn(index){
        return $(`.modal-content .modal__list--flex-space-list .modal__list-item:nth-child(${index}) .btn-primary.multiple-approved-status-btn`)
    };

    get bulkApprovalCrossIcon(){
        return $(".modal-content .modal__header .action--modal-close")
    };

    get bulkApprovalTitle(){
        return $(".modal-content .modal__header .modal__title")
    };

    get flashMessageForApproval(){
        return $(".flash-message__text")
    };

    get filterSearchField(){
        return $("#exampleSearchInput")
    };

    getCommentName(index,index_1){
        return $(`.card--accordion-list:nth-child(${index}) .approvedBox .row .col-sm-9:nth-child(${index_1}) .noteDeascription`)
    };

    getCommentsText(index,index_1,index_2){
        return $(`.approval-accordian-in > div:nth-child(${index}) .approveBoxWrp > li:nth-child(${index_1}) .row >div:nth-child(${index_2}) .noteDeascription p`)
    };

    get approvalConfirmBtn(){
        return $(".modal-content:nth-child(1) .modal-body .text-right .btn-primary")
    };

    approvalComments(){
        expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
        browser.url(testData.zeidler.qatmdpage);
        browser.waitUntil(
        function() {
        return (
        $(".btn__content.choices__item.choices__item--selectable").isVisible() === true
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
        this.zeidlerDropdown.waitForVisible();
        this.zeidlerDropdown.click();
        this.searchField.waitForVisible();
        browser.setValue(".dropdown-choices__menu input", ['QA Calibre Investment Management','Enter']);
        waitUntilTrueFalse(
        `.filters-container>div:nth-child(2) .btn span`,
        "5000",
        `Filters is not visible even after 5sec`,
        true
        );
        this.getFilterBtn(2).waitForVisible();
        this.getFilterBtn(2).click();
        this.filterSearchField.waitForVisible();
        this.filterSearchField.setValue(testData.tmdapprovaltab.isinname);
        this.kiidIsinFilterOption.waitForVisible();
        browser.waitUntil(
            function() {
            return (
            $("#isin_filter_list .dropdown-item.name-filter-item:nth-child(1)").getText() === testData.tmdapprovaltab.isinname
            )
            },
            5000,
            "Filter is not visible even after 5s"
            );
        this.kiidIsinFilterOption.click();
        waitUntilTrueFalse(
        `#kiid_list_page > div:nth-child(2) .table__cell--status.col`,
        "5000",
        `Filters is not visible even after 5sec`,
        true
        );
        this.kiidRecordIsin.waitForVisible();
        expect(this.kiidRecordIsin.getText()).to.eql(testData.tmdapprovaltab.isinname);
        this.getCreateNoteIcon(2).waitForVisible();
        this.getCreateNoteIcon(2).click();
        this.approvalPageHeading.waitForVisible();
        browser.waitUntil(
        function() {
        return (
            $(`h1.h5.page-header__title`).isVisible() === true
        )
        },
        30000,
        "Approval page is not visible even after 30s"
        );
        for(i = 1; i<=10; i++){
        if(this.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitle1){
            this.getApproveBtn(2,i,2).waitForVisible();
            expect(this.getApproveBtn(2,i,2).isEnabled()).to.eql(true);  
            break
            };
        };
    };

    approvalCommentForApproval(){
        expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
        browser.url(testData.zeidler.qatmdpage);
        browser.waitUntil(
        function() {
        return (
        $(".btn__content.choices__item.choices__item--selectable").isVisible() === true
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
        this.zeidlerDropdown.waitForVisible();
        this.zeidlerDropdown.click();
        this.searchField.waitForVisible();
        browser.setValue(".dropdown-choices__menu input", ['QA Calibre Investment Management','Enter']);
        waitUntilTrueFalse(
        `.filters-container>div:nth-child(2) .btn span`,
        "5000",
        `Filters is not visible even after 5sec`,
        true
        );
        this.getFilterBtn(2).waitForVisible();
        this.getFilterBtn(2).click();
        this.filterSearchField.waitForVisible();
        this.filterSearchField.setValue(testData.tmdapprovaltab.isinname);
        this.kiidIsinFilterOption.waitForVisible();
        browser.waitUntil(
            function() {
            return (
            $("#isin_filter_list .dropdown-item.name-filter-item:nth-child(1)").getText() === testData.tmdapprovaltab.isinname
            )
            },
            5000,
            "Filter is not visible even after 5s"
            );
        this.kiidIsinFilterOption.click();
        waitUntilTrueFalse(
        `#kiid_list_page > div:nth-child(2) .table__cell--status.col`,
        "5000",
        `Filters is not visible even after 5sec`,
        true
        );
        this.kiidRecordIsin.waitForVisible();
        expect(this.kiidRecordIsin.getText()).to.eql(testData.tmdapprovaltab.isinname);
        this.getCreateNoteIcon(2).waitForVisible();
        this.getCreateNoteIcon(2).click();
        this.approvalPageHeading.waitForVisible();
        browser.waitUntil(
        function() {
        return (
            $(`h1.h5.page-header__title`).isVisible() === true
        )
        },
        30000,
        "Approval page is not visible even after 30s"
        );
    };

    addCommentsForRecords(comments){
        browser.waitUntil(
        function() {
        return (
            $(`h1.h5.page-header__title`).isVisible() === true
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        browser.scroll(0,500);
        for(i = 1; i<=10; i++){
        if(this.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitleforkiid){
            this.getApproveBtn(2,i,1).waitForVisible();
            this.getApproveBtn(2,i,1).click(); 
            break
          };
        };
        browser.pause(1000);
        this.noteField.waitForVisible();
        this.noteField.setValue(comments);
    };

    addCommentByClickingCreateIcon(comments){
        for(i = 1; i<=10; i++){
        if(this.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitle1){
            browser.pause(1000);
            this.getApproveBtn(2,i,1).waitForVisible();
            this.getApproveBtn(2,i,1).click(); 
            break
          };
        };
        browser.pause(1000);
        this.noteField.waitForVisible();
        this.noteField.setValue(comments);
        this.saveNoteBtn.waitForVisible();
        this.saveNoteBtn.click();
    };
};
export default new tmdrecordlisting();