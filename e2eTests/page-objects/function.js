export const waitUntilTrueFalse = (locator, time, comment, booleanValue) => {
    browser.pause(2000);
    browser.waitUntil(
      function() {
        return browser.isVisible(locator) === booleanValue;
      },
      time,
      comment
    );
  };