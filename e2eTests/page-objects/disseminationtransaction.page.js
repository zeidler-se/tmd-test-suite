import Page from "./page"
import testData from "../constants/testData.json"
import { expect } from "chai"
import { waitUntilTrueFalse } from "../page-objects/function";

class disseminationtrans extends Page{
    get filterDropdown(){
        return $(".btn.btn--light.text--white.has-icon.has-caret")
    }

    get umbrellaFilter(){
        return $("form > div:nth-child(2) > div:nth-child(1) .choices__input--cloned")
    }

    get umbrellaFilterLabel(){
        return $("table tbody tr:nth-child(1) td:nth-child(4)")
    }

    get transactionDateFilter(){
        return $("#filter_transaction_date")
    }

    getTransactionDate(index){
        return $(`.flatpickr-days .dayContainer .flatpickr-day:nth-child(${index})`)
    }

    get transactionNextDate(){
        return $(".flatpickr-days .dayContainer .flatpickr-day:nth-child(9)")
    }

    get transactionTodayDate(){
        return $(".flatpickr-days .dayContainer .flatpickr-day.today")
    }

    get transactionDateNextDay(){
        return $(".flatpickr-days .dayContainer .flatpickr-day:nth-child(31)")
    }

    get previousMonthIcon(){
        return $(".flatpickr-prev-month")
    }

    get nextMonthIcon(){
        return $(".flatpickr-next-month")
    }

    get applyFilterBtn(){
        return $(".btn--primary.btn--full.text--white")
    }

    get transactionDateValue(){
        return $("table tbody tr:nth-child(1) td:nth-child(2)")
    }

    get ruleNameFilter(){
        return $("form>div:nth-child(2)>div:nth-child(2) .choices__input--cloned")
    }

    getRuleNameLabel(index,index1){
        return $(`table tbody tr:nth-child(${index}) td:nth-child(${index1})`)
    }

    get transactionHeading(){
        return $(".page-details__title")
    }

    get resetAllBtn(){
        return $(".btn--light.btn--full.text--white")
    }

    get transactionEmailFilter(){
        return $("form>div:nth-child(3)>div:nth-child(2) .choices__input--cloned")
    }

    get emailLabel(){
        return $("table tbody tr:nth-child(1) td:nth-child(6)")
    }

    get recipientNameFilter(){
        return $("form>div:nth-child(3)>div:nth-child(3) .choices__input--cloned")
    }

    get nameLabel(){
        return $("table tbody tr:nth-child(1) td:nth-child(6)")
    }

    get statusFilter(){
        return $("form>div:nth-child(3)>div:nth-child(4) .choices__input--cloned")
    }

    get statusLabel(){
        return $("table tbody tr:nth-child(1) td:nth-child(7)")
    }

    get transactionCount(){
        return $(".page-details__tags li")
    }

    get fundGroupName(){
        return $("table tbody tr:nth-child(1) td:nth-child(4)")
    }

    get transRecipientName(){
        return $("table tbody tr:nth-child(1) td:nth-child(6)")
    }

    get editIcon(){
        return $("table tbody tr:nth-child(1) td:nth-child(9) ul li:nth-child(2) a")
    }

    get deleteIcon(){
        return $("table tbody tr:nth-child(1) td:nth-child(9) ul li:nth-child(3) a")
    }

    get emailField(){
        return $("#dissemination_transaction_recipient_email_id")
    }

    get saveBtn(){
        return $("#edit_admin_dissemination_transaction")
    }

    get transactionCheckbox(){
        return $("table tbody tr:nth-child(1) td:nth-child(1)")
    }

    get bulkActionDropdown(){
        return $(".choices.choices--bulk-actions.bulk-actions__select")
    }

    get retryOption(){
        return $("//div[contains(text(), 'Retry')]")
    }

    get applyToBtn(){
        return $(".bulk-actions__apply.show")
    };

    getTmdTab(index){
        return $(`.dropdown-menu .dropdown-menu__item:nth-child(${index}) a`)
    }

    get kiidBtn(){
        return $(".btn-secondary.dropdown-toggle.btn--primary")
    }

    transactionFilterForStatus(){
        waitUntilTrueFalse(
        `form>div:nth-child(2)>div:nth-child(2) .choices__input--cloned`,
        "10000",
        `Rule card is not visible even after 10sec`,
        true
        );
        this.ruleNameFilter.waitForVisible();
        browser.setValue("form>div:nth-child(2)>div:nth-child(2) .choices__input--cloned",['QA KIID Auto','Enter']);
        this.transactionHeading.waitForVisible();
        this.transactionHeading.click();
        this.transactionDateFilter.waitForVisible();
        this.transactionDateFilter.click();
        this.transactionTodayDate.waitForVisible();
        this.transactionTodayDate.click();
        this.transactionDateNextDay.waitForVisible();
        this.transactionDateNextDay.click();
    };

    transactionForEditedRecipient(){
        this.transactionFilterForStatus();
        this.transactionEmailFilter.waitForVisible();
        browser.setValue("form>div:nth-child(3)>div:nth-child(2) .choices__input--cloned",['cnnusindhu92@gmail.com','Enter']);
        this.transactionHeading.waitForVisible();
        this.transactionHeading.click();
        this.applyFilterBtn.waitForVisible();
        this.applyFilterBtn.click();
    };

    transactionForXlsCsv(rulename,email){
        browser.url(testData.zeidler.qatransactionpage);
        expect(browser.getUrl()).to.eql(testData.zeidler.qatransactionpage);
        this.kiidBtn.waitForVisible();
        this.kiidBtn.click();
        this.getTmdTab(5).waitForVisible();
        this.getTmdTab(5).click();
        waitUntilTrueFalse(
        `.btn.btn--light.text--white.has-icon.has-caret`,
        "10000",
        `Rule card is not visible even after 10sec`,
        true
        );
        this.filterDropdown.waitForVisible();
        this.filterDropdown.click();
        this.ruleNameFilter.waitForVisible();
        browser.setValue("form>div:nth-child(2)>div:nth-child(2) .choices__input--cloned",[rulename,'Enter']);
        this.transactionHeading.waitForVisible();
        this.transactionHeading.click();
        this.transactionDateFilter.waitForVisible();
        this.transactionDateFilter.click();
        this.transactionTodayDate.waitForVisible();
        this.transactionTodayDate.click();
        this.transactionDateNextDay.waitForVisible();
        this.transactionDateNextDay.click();
        this.transactionEmailFilter.waitForVisible();
        browser.setValue("form>div:nth-child(3)>div:nth-child(2) .choices__input--cloned",[email,'Enter']);
        this.transactionHeading.waitForVisible();
        this.transactionHeading.click();
        this.applyFilterBtn.waitForVisible();
        this.applyFilterBtn.click();
    };

};
export default new disseminationtrans();

