import Page from "./page"
import { expect } from 'chai'
import testData from "../constants/testData.json"
import Home from "../page-objects/home.page"
import { $CombinedState } from "redux"

class Exavault extends Page{
    get heading(){
        return $(".top-header .brand.pull-left g.letters")
    };

    get userNameForExavault(){
        return $("#loginUsername")
    };

    get passwordForExavault(){
        return $("#loginPassword")
    };

    get loginBtn(){
        return $("//span[contains(text(),'Log in')]")
    };

    getFolderName(index){
        return $(`#file-list > div:nth-child(${index}) .folder.item.draggable .item-name .name-only`)
    };

    get filesName(){
        return $("#file-list .file.item.pdf.draggable .item-name")
    };

    getHomeLink(index){
        return $(`.path-wrapper.header-text.pull-left .path-wrap .crumb:nth-child(${index})`)
    }

    Login(url,username,password,fileurl){
        browser.newWindow('https://zeidlerlegalservices.exavault.com/');
        browser.waitUntil(
        function() {
        return (
            browser.getUrl() === url
        )
        },
        30000,
        "Exavault page is not visible even after 30s"
        );
        expect(browser.getUrl()).to.eql(url);
        this.userNameForExavault.waitForVisible();
        this.userNameForExavault.setValue(username);
        //browser.pause(1000);
        this.passwordForExavault.waitForVisible();
        this.passwordForExavault.setValue(password);
        browser.pause(1000);
        this.loginBtn.waitForVisible();
        this.loginBtn.click();
        browser.waitUntil(
        function() {
        return (
            $(".top-header .brand.pull-left g.letters").isVisible() === true
        )
        },
        30000,
        "Exavault page is not visible even after 30s"
        );
        expect(browser.getUrl()).to.eql(fileurl);
        browser.pause(5000);
        browser.scroll(0,15000);
        };
}
export default new Exavault();