import Page from "./page"
import { expect } from 'chai'
import testData from "../constants/testData.json"
import { waitUntilTrueFalse } from "./function";
import Login from "./login.page"

class tmdadminrecipant extends Page{
    get recipantLink(){
        return $(".nav__link.has-icon.active")
    }

    get sideBar(){
        return $(".sidebar__body")
    }

    get addRecipantBtn(){
        return $(".btn.btn--primary.has-icon")
    }

    get recipantNameField(){
        return $("#recipient_name")
    }

    get recipantContactNameField(){
        return $("#recipient_contact_name")
    }

    get recipantContactEmailField(){
        return $("#recipient_contact_email")
    }

    get recipantContactPhoneNumberField(){
        return $("#recipient_contact_phone_number")
    }

    getRecipantKiidCheckbox(index){
        return $(`.accordion-group > div:nth-child(${index}) .custom-control`)
    }

    get deliveryMethodField(){
        return $("#recipient_recipient_delivery_details_attributes_4_delivery_method_id")
    }

    getDeliveryMethodFtp(index){
        return $(`#recipient_recipient_delivery_details_attributes_4_delivery_method_id option:nth-child(${index})`)
    }

    get hostDetailsField(){
        return $("#recipient_recipient_delivery_details_attributes_4_ftp_host")
    }

    get portNumberField(){
        return $("#recipient_recipient_delivery_details_attributes_4_ftp_port")
    }

    get userNameField(){
        return $("#recipient_recipient_delivery_details_attributes_4_ftp_username")
    }

    get passwordField(){
        return $("#recipient_recipient_delivery_details_attributes_4_ftp_password")
    }

    get rootDirectory(){
        return $("#recipient_recipient_delivery_details_attributes_4_ftp_directory")
    }

    get chargeableCheckbox(){
        return $("#recipient_recipient_delivery_details_attributes_4_chargeable")
    }

    get apiEndPointField(){
        return $("#recipient_recipient_delivery_details_attributes_4_api_endpoint")
    }

    get documentTypeField(){
        return $("#recipient_recipient_delivery_details_attributes_4_document_type")
    }

    get csvOption(){
        return $("#recipient_recipient_delivery_details_attributes_4_document_type option:nth-child(2)")
    }

    get apiKeyField(){
        return $("#recipient_recipient_delivery_details_attributes_4_api_key")
    }

    get apiUserNameField(){
        return $("#recipient_recipient_delivery_details_attributes_4_api_username")
    }

    get apiPasswordField(){
        return $("#recipient_recipient_delivery_details_attributes_4_api_password")
    }

    get emailAddressField(){
        return $(".email_content_5 .choices__input--cloned")
    }

    get privateKeyField(){
        return $(".ftp_content_5 .form__group:nth-child(8) .choices.choices--single-select .choices__inner")
    };

    getEmailAddressName(index){
        return $(`#recipient_recipient_delivery_details_attributes_4_recipient_emails_attributes_0_email .choices__item--selectable:nth-child(${index})`)
    }

    get saveBtn(){
        return $("#save_recipient")
    }

    get emptyFieldErrorMessageLabel(){
        return $(".invalid-feedback.message")
    }

    get totalRecipientNumber(){
        return $(".page-details__tags li")
    }

    get subjectlineField(){
        return $(".email_content_5 > div:nth-child(3) .control")
    }

    get sameRecipientErrorMessage(){
        return $(".invalid-feedback.message")
    }

    get allRecipientsLink(){
        return $("//a[contains(text(),'All Recipients')]")
    }

    get recipientEditIcon(){
        return $("table tbody tr:nth-child(1) td:nth-child(4) ul li:nth-child(1) a")
    }

    get recipientDeleteIcon(){
        return $("table tbody tr:nth-child(1) td:nth-child(4) ul li:nth-child(2) a")
    }

    get editRecipientPage(){
        return $(".page-details__title")
    }

    get clientMappingBtn(){
        return $(".nav.nav--tabs li:nth-child(2) a")
    }

    get clientNameCheckbox(){
        return $(".page-body tr th > div:nth-child(1)")
    }

    get bulkActionDropdown(){
        return $(".choices--bulk-actions.bulk-actions__select")
    }

    get applyToBtn(){
        return $(".bulk-actions__apply.show")
    }

    get deleteOption(){
        return $(".choices__list--dropdown")
    }

    get addClientBtn(){
        return $(".btn.btn--primary.has-icon:nth-child(1)")
    }

    get clientField(){
        return $(".choices__input.choices__input--cloned")
    }

    get clientSaveBtn(){
        return $(".modal-content__footer .btn--primary")
    }

    get addClientPopupHeading(){
        return $(".modal-content__header h3")
    }

    get clientDeleteIcon(){
        return $("table tbody tr:nth-child(1) td:nth-child(4) ul li:nth-child(1) a")
    }

    get clientsDeleteIcon(){
        return $("table tbody tr:nth-child(2) td:nth-child(4) ul li:nth-child(1) a")
    }

    getClientCheckbox(index,index_1){
        return $(`tr:nth-child(${index}) td:nth-child(${index_1}) .custom-control--checkbox`)
    }

    get clientCheckboxs(){
        return $("tr:nth-child(2) td:nth-child(1) .custom-control--checkbox")
    }

    get mapAllClientsCheckbox(){
        return $("#recipient_map_all_clients")
    }

    get noClientsAddedHeading(){
        return $("h3")
    }

    get hostDetailsLabel(){
        return $(".ftp_content_5 .form__group > div:nth-child(3) label")
    }

    get userNameLabel(){
        return $(".ftp_content_5 .form__group > div:nth-child(5) label")
    }

    get passwordLabel(){
        return $(".ftp_content_5 .form__group > div:nth-child(6) label")
    }

    get rootDirectoryLabel(){
        return $(".ftp_content_5 .form__group > div:nth-child(9) label")
    }

    get apiEndPointLabel(){
        return $(".api_content_5 .form__group > div:nth-child(2) label")
    }

    get apiKeyLabel(){
        return $(".api_content_5 .form__group > div:nth-child(3) label")
    }

    get apiTypeLabel(){
        return $(".api_content_5 .form__group > div:nth-child(4) label")
    }

    get apiUserNameLabel(){
        return $(".api_content_5 .form__group > div:nth-child(5) label")
    }

    get apiPasswordLabel(){
        return $(".api_content_5 .form__group > div:nth-child(6) label")
    }

    get qaAmAccountLabel(){
        return $(".trim-long-filenames")
    }
    
    get informationBtn(){
        return $(".nav.nav--tabs li:nth-child(1) a")
    }

    get emailContentLabel(){
        return $(".email_content_5 h3")
    }

    get recipientNameLabel(){
        return $("#recipient_form > div:nth-child(3) label")
    }

    get recipientContactNameLabel(){
        return $("#recipient_form > div:nth-child(4) label")
    }

    get deliveryMethodLabel(){
        return $("#recipient_form > div:nth-child(4) label")
    }

    get signOutBtn(){
        return $(".sidebar__signout .has-icon")
    }

    get searchField(){
        return $("#search")
    }

    get clientQaName(){
        return $("tr:nth-child(1) td:nth-child(2)")
    }

    get protocolField(){
        return $("#recipient_recipient_delivery_details_attributes_4_protocol_type")
    };

    getFtpField(index){
        return $(`#recipient_recipient_delivery_details_attributes_4_protocol_type option:nth-child(${index}`)
    }

    get privateKeyRsa(){
        return $("#choices-recipient_recipient_delivery_details_attributes_4_rsa_private_key_id-item-choice-3")
    };

    getRecipientName(index,index_1){
        return $(`table tbody tr:nth-child(${index}) td:nth-child(${index_1})`)
    }

    returnPublicname(){
        var name = testData.recipient.yopemail + this.randomName() + "@yopmail.com";
        return name;
    }

    createRecipient(name,contactName,contactEmail,contactPhoneNumber,validation){
        this.recipientCreation(name);
        this.recipantContactNameField.waitForVisible();
        this.recipantContactNameField.setValue(contactName);
        this.recipantContactEmailField.waitForVisible();
        this.recipantContactEmailField.setValue(contactEmail);
        this.recipantContactPhoneNumberField.waitForVisible();
        this.recipantContactPhoneNumberField.setValue(contactPhoneNumber);
        this.mapAllClientsCheckbox.waitForVisible();
        this.mapAllClientsCheckbox.click();
        this.getRecipantKiidCheckbox(5).waitForVisible();
        this.getRecipantKiidCheckbox(5).click();
        browser.scroll(0,500);
        browser.pause(1000);
        this.emailAddressField.waitForVisible();
        this.emailAddressField.click();
        browser.setValue(".email_content_5 .choices__input--cloned",['jithendra@calibrecode.com','Enter']);
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        browser.pause(1000);
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
        waitUntilTrueFalse(
        `#search`,
        "10000",
        `Rule card is not visible even after 10sec`,
        true
        );
        this.searchField.waitForVisible();
        browser.setValue("#search",[name,'Enter']);
        this.getRecipientName(1,1).waitForVisible();
        expect(this.getRecipientName(1,1).getText()).to.eql(validation)
    };

    createSameRecipient(name){
        this.recipientCreation(name);
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        browser.pause(1000);
        this.sameRecipientErrorMessage.waitForVisible();
        expect(this.sameRecipientErrorMessage.getText()).to.eql(testData.recipient.samerecipientvalidationmessage);
    };

    editRecipient(name, validation, editname, contactName, contactEmail, contactPhoneNumber){
        browser.refresh();
        this.allRecipientsLink.waitForVisible();
        this.allRecipientsLink.click();
        waitUntilTrueFalse(
        `#search`,
        "10000",
        `Rule card is not visible even after 10sec`,
        true
        );
        this.searchField.waitForVisible();
        browser.setValue("#search",[name,'Enter']);
        this.getRecipientName(1,1).waitForVisible();
        expect(this.getRecipientName(1,1).getText()).to.eql(validation);
        this.recipientEditIcon.waitForVisible();
        this.recipientEditIcon.click();
        browser.pause(1000);
        this.editRecipientPage.waitForVisible();
        expect(this.editRecipientPage.getText()).to.eql(testData.recipient.editrecipientpage);
        this.recipantNameField.waitForVisible();
        this.recipantNameField.setValue(editname);
        this.recipantContactNameField.waitForVisible();
        this.recipantContactNameField.setValue(contactName);
        this.recipantContactEmailField.waitForVisible();
        this.recipantContactEmailField.setValue(contactEmail);
        this.recipantContactPhoneNumberField.waitForVisible();
        this.recipantContactPhoneNumberField.setValue(contactPhoneNumber);
        this.mapAllClientsCheckbox.waitForVisible();
        this.mapAllClientsCheckbox.click();
        browser.scroll(0,500);
        browser.pause(1000);
        this.emailAddressField.waitForVisible();
        this.emailAddressField.click();
        browser.setValue(".email_content_5 .choices__input--cloned",['cnusindhu93@gmail.com','Enter']);
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        browser.pause(1000);
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
        waitUntilTrueFalse(
        `#search`,
        "10000",
        `Rule card is not visible even after 10sec`,
        true
        );
        this.searchField.waitForVisible();
        browser.setValue("#search",[editname,'Enter']);
        this.getRecipientName(1,1).waitForVisible();
        expect(this.getRecipientName(1,1).getText()).to.eql(editname);
    };

    mandatoryRecipient(name){
        this.recipientCreationWithUncheckClient(name);
        browser.pause(1000);
        this.emailAddressField.waitForExist();
        this.emailAddressField.waitForVisible();
        this.emailAddressField.click();
        browser.setValue(".email_content_5 .choices__input--cloned",['jithendra@calibrecode.com','Enter']);
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        browser.pause(1000);
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
    };

    deleteRecipient(recipientName){
        browser.refresh();
        waitUntilTrueFalse(
        `#search`,
        "10000",
        `Rule card is not visible even after 10sec`,
        true
        );
        this.searchField.waitForVisible();
        browser.setValue("#search",[recipientName,'Enter']);
        this.recipientDeleteIcon.waitForVisible();
        this.recipientDeleteIcon.click();
        browser.alertAccept();
    }

    deleteClient(clientName){
        this.searchField.waitForVisible();
        browser.setValue("#search",[clientName,'Enter']);
        this.clientDeleteIcon.waitForVisible();
        this.clientDeleteIcon.click();
        browser.alertAccept();
    }

    logout(){
        this.signOutBtn.waitForVisible();
        this.signOutBtn.click();
    }

    disseminationRunRecipient(name, email, emailauto){
        this.recipientCreationWithUncheckClient(name);
        browser.pause(1000);
        this.emailAddressField.waitForVisible();
        this.emailAddressField.click();
        browser.setValue(".email_content_5 .choices__input--cloned",[email,'Enter']);
        this.emailAddressField.click();
        browser.setValue(".email_content_5 .choices__input--cloned",[emailauto,'Enter']);
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        browser.pause(1000);
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
    }

    disseminationRunRecipient1(name, email, email1){
        this.allRecipientsLink.waitForVisible();
        this.allRecipientsLink.click();
        this.addRecipantBtn.waitForVisible();
        this.addRecipantBtn.click();
        this.recipantNameField.waitForVisible();
        this.recipantNameField.setValue(name);
        this.mapAllClientsCheckbox.waitForVisible();
        this.mapAllClientsCheckbox.click();
        this.getRecipantKiidCheckbox(5).waitForVisible();
        this.getRecipantKiidCheckbox(5).click();
        browser.scroll(0,500);
        browser.pause(1000);
        this.emailAddressField.waitForVisible();
        this.emailAddressField.click();
        browser.setValue(".email_content_5 .choices__input--cloned",[email,'Enter']);
        this.emailAddressField.waitForVisible();
        this.emailAddressField.click();
        browser.setValue(".email_content_5 .choices__input--cloned",[email1,'Enter']);
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        browser.pause(1000);
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
    }

    disseminationRunRecipientCsv(name, email, email1){
        this.recipientCreationWithUncheckClient(name);
        this.documentTypeField.waitForVisible();
        this.documentTypeField.click();
        this.csvOption.waitForVisible();
        this.csvOption.click();
        browser.scroll(0,500);
        browser.pause(1000);
        this.emailAddressField.waitForVisible();
        this.emailAddressField.click();
        browser.setValue(".email_content_5 .choices__input--cloned",[email,'Enter']);
        this.emailAddressField.waitForVisible();
        this.emailAddressField.click();
        browser.setValue(".email_content_5 .choices__input--cloned",[email1,'Enter']);
        this.emailAddressField.waitForVisible();
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        browser.pause(1000);
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
    }

    disseminationRecipient(name, email){
        this.recipientCreationWithUncheckClient(name);
        this.documentTypeField.waitForVisible();
        this.documentTypeField.click();
        browser.pause(1000);
        this.emailAddressField.waitForVisible();
        this.emailAddressField.click();
        browser.setValue(".email_content_1 .choices__input--cloned",[email,'Enter']);
        this.emailAddressField.waitForVisible();
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        browser.pause(1000);
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
    }

    sftpRecipientForDissemination(name,index,ftp_index,host,username,rootdirectory,validation){
        this.recipientForFtp(name,index,ftp_index,host,username);
        browser.scroll(500,900);
        browser.pause(1000);
        this.privateKeyField.waitForVisible();
        this.privateKeyField.click();
        this.privateKeyRsa.waitForVisible();
        this.privateKeyRsa.click();
        this.rootDirectory.waitForVisible();
        this.rootDirectory.setValue(rootdirectory);
        browser.scroll(-0,-1000);
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
        waitUntilTrueFalse(
        `#search`,
        "10000",
        `Rule card is not visible even after 10sec`,
        true
        );
        this.searchField.waitForVisible();
        browser.setValue("#search",[name,'Enter']);
        this.getRecipientName(1,1).waitForVisible();
        expect(this.getRecipientName(1,1).getText()).to.eql(validation);
    };

    ftpRecipientForDissemination(name,index,ftp_index,host,username,password,rootdirectory,validation){
        this.recipientForFtp(name,index,ftp_index,host,username);
        this.passwordField.waitForVisible();
        this.passwordField.setValue(password);
        this.rootDirectory.waitForVisible();
        this.rootDirectory.setValue(rootdirectory);
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        browser.pause(1000);
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
        waitUntilTrueFalse(
        `#search`,
        "10000",
        `Rule card is not visible even after 10sec`,
        true
        );
        this.searchField.waitForVisible();
        browser.setValue("#search",[name,'Enter']);
        this.getRecipientName(1,1).waitForVisible();
        expect(this.getRecipientName(1,1).getText()).to.eql(validation);
    };

    recipientCreation(name){
        this.addRecipantBtn.waitForVisible();
        this.addRecipantBtn.click();
        this.recipantNameField.waitForVisible();
        this.recipantNameField.setValue(name);
    };

    recipientCreationWithUncheckClient(name){
        this.recipientCreation(name);
        this.mapAllClientsCheckbox.waitForVisible();
        this.mapAllClientsCheckbox.click();
        this.getRecipantKiidCheckbox(5).waitForVisible();
        this.getRecipantKiidCheckbox(5).click();
        browser.scroll(0,500);
    };

    recipientForFtp(name,index,ftp_index,host,username){
        this.recipientCreationWithUncheckClient(name);
        this.deliveryMethodField.waitForVisible();
        this.deliveryMethodField.click();
        this.getDeliveryMethodFtp(index).waitForVisible();
        this.getDeliveryMethodFtp(index).click();
        this.protocolField.waitForVisible();
        this.protocolField.click();
        this.getFtpField(ftp_index).waitForVisible();
        this.getFtpField(ftp_index).click();
        this.hostDetailsField.waitForVisible();
        this.hostDetailsField.setValue(host);
        this.userNameField.waitForVisible();
        this.userNameField.setValue(username);
    };

    addRecipient(){
        this.allRecipientsLink.waitForVisible();
        this.allRecipientsLink.click();
        this.addRecipantBtn.waitForVisible();
        this.addRecipantBtn.click();
    };

    ftpRecipientWithoutRootDirec(name,index,ftp_index,host,username,password,validation){
        this.recipientForFtp(name,index,ftp_index,host,username);
        this.passwordField.waitForVisible();
        this.passwordField.setValue(password);
        this.rootDirectory.waitForVisible();
        expect(this.rootDirectory.isVisible()).to.eql(true);
        this.saveBtn.waitForVisible();
        this.saveBtn.click();
        browser.pause(1000);
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
        waitUntilTrueFalse(
        `#search`,
        "10000",
        `Rule card is not visible even after 10sec`,
        true
        );
        this.searchField.waitForVisible();
        browser.setValue("#search",[name,'Enter']);
        this.getRecipientName(1,1).waitForVisible();
        expect(this.getRecipientName(1,1).getText()).to.eql(validation);
    };

};
export default new tmdadminrecipant();