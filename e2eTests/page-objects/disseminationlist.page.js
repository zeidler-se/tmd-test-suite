import Page from "./page"
import testData from "../constants/testData.json"
import { relativeTimeRounding } from "moment"
import { expect } from "chai"
import { waitUntilTrueFalse } from "../page-objects/function";
import tmdRecordingList from "./tmdrecordlisting.page";

class disseminationlist extends Page{
    get totalCountKiid(){
        return $(".detail.total_kiids")
    }

    get sortByDropdown(){
        return $(".btn-outline-primary.dropdown-toggle")
    }

    get ascendingOption(){
        return $(".dropdown-menu.show a:nth-child(1)")
    }

    get descendingOption(){
        return $(".dropdown-menu.show a:nth-child(2)")
    }

    get searchField(){
        return $("#dissemination_rule_list_search")
    }

    get ruleLabelName(){
        return $(".card--accordion-list:nth-child(1) .card__title")
    }

    get createNewRuleBtn(){
        return $(".btn-primary.create-new-rule")
    }
    
    get ruleNameField(){
        return $("#dissemination_rule_rule_name")
    }

    get ruleAllKiidCheckbox(){
        return $("#dissemination_rule_all_kiids")
    }

    get recipientSearchField(){
        return $(".choices__input--cloned")
    }

    get saveChangesBtn(){
        return $(".justify-content-between.d-flex .form-submit-rules:nth-child(2)")
    }

    get kiidSubFundFilterDropdown(){
        return $(".dropdown--form-control > div:nth-child(1)")
    }

    get codeFilterDropdown(){
        return $("//div[contains(text(),' Product APIR Code')]")
    }

    get nameFilterDropdown(){
        return $("//div[contains(text(),' Share Class Name')]")
    }

    getAllFiltersOnCreateRule(index,index_1){
        return $(`.dropdown-menu__inner-scroll:nth-child(${index}) .dropdown-item.choices__item--selectable:nth-child(${index_1})`)
    };

    getAllFilterDropdown(index){
        return $(`.dropdown-item.choices__item--selectable:nth-child(${index})`)
    }

    get languageFilterDropdown(){
        return $(".dropdown-item.choices__item--selectable:nth-child(5)")
    }

    get countryFilterDropdown(){
        return $(".dropdown-item.choices__item--selectable:nth-child(6)")
    }

    get selectKiidsFilterDropdown(){
        return $(".form-control--bordered.dropdown-toggle")
    }

    getKiidFilterOptionCheckbox(index){
        return $(`.dropdown-menu--large.show .dropdown-item--check:nth-child(${index})`)
    }

    get selectLink(){
        return $(".dropdown-menu--large.show .dropdown-item--apply")
    }

    get emptyFiltervalidationMessage(){
        return $(".invalid-feedback.message:nth-child(3)")
    }

    get validationMessage(){
        return $(".invalid-feedback.message:nth-child(2)")
    }

    get countryBeFilterOption(){
        return $(".dropdown-item--check:nth-child(1)")
    }

    get countryGbFilterOption(){
        return $(".dropdown-item--check:nth-child(2)")
    }

    get countryIeFilterOption(){
        return $(".dropdown-item--check:nth-child(3)")
    }

    get countryLuFilterOption(){
        return $(".dropdown-item--check:nth-child(4)")
    }

    get countryMtFilterOption(){
        return $(".dropdown-item--check:nth-child(5)")
    }
 
    get selectDropdownSearchField(){
        return $("#checkbox-search")
    }

    getTotalNumberRecipient(index){
        return $(`.card--accordion-list:nth-child(${index}) .detail.share-classes`)
    }

    get totalNumRecipientUat(){
        return $(".page-header .details__list .detail.total_kiids")
    }

    getRuleEditIcon(index){
        return $(`.dissemination-list .card--accordion-list:nth-child(${index}) .edit-new-rule`)
    }

    get editIconRule(){
        return $(".dissemination-list .card--accordion-list:nth-child(1) .edit-new-rule")
    }

    getRuleNameLabel(index){
        return $(`.card--accordion-list:nth-child(${index}) .card__title`)
    }

    get ruleCardName(){
        return $(".card--accordion-list:nth-child(3) .card__title")
    }

    get ruleName(){
        return $(".card--accordion-list:nth-child(4) .card__title")
    }

    get ruleDeleteIcon(){
        return $(".card--accordion-list:nth-child(1) .card__actions .action:nth-child(1)")
    }

    get removeBtn(){
        return $(".btn-primary.remove-rule")
    }

    get plusBtn(){
        return $(".btn-primary.new-condition")
    }

    get selectIsinFilterDropdown(){
        return $(".all_rule_conditions > div:nth-child(2) .dropdown-toggle")
    }

    get selectNameFilterDropdown(){
        return $(".all_rule_conditions > div:nth-child(3) .dropdown-toggle")
    }

    get selectCurrencyFilterDropdown(){
        return $(".all_rule_conditions > div:nth-child(4) .dropdown-toggle")
    }

    getSelectCurrencyFilterOption(index){
        return $(`.dropdown-menu--large.show .dropdown-item--check:nth-child(${index})`)
    }

    get selectCurrencyOption(){
        return $(".dropdown-menu--large.show .dropdown-item--check:nth-child(2)")
    }

    get selectLanguageFilterDropdown(){
        return $(".all_rule_conditions > div:nth-child(5) .dropdown-toggle")
    }

    get selectCountryFilterDropdown(){
        return $(".all_rule_conditions > div:nth-child(6) .dropdown-toggle")
    }

    get ruleCardDropdownIcon(){
        return $(".dissemination-list>div:nth-child(2) .card-onclick__target")
    }

    get allKiidLabel(){
        return $(".dissemination-list>div:nth-child(2) .rule-detail")
    }

    get chargableResult(){
        return $(".table__body .table__row--outer .table__cell--chargeable.col")
    }

    get filterSearchField(){
        return $("#checkbox-search")
    }

    get countryNameLabel(){
        return $(".dropdown-item--check:nth-child(4) label")
    }

    get ruleHeading(){
        return $("Add recipients to this rule by searching by name, company or organisation")
    }

    get ruleCrossIcon(){
        return $(".action--modal-close.action--modal-rules-close .icon")
    };

    get ruleWindowHeading(){
        return $("#kiid-create-rule-modal-body .modal__header .modal__title")
    };

    get messageForValidation(){
        return $(".invalid-feedback.message")
    };

    get noFoundResult(){
        return $(".choices__item.choices__item--choice.has-no-results")
    };

    get cancelBtn(){
        return $(".justify-content-between.d-flex .btn-outline-secondary.form-cancel-rules")
    };

    get editRuleWindowTitle(){
        return $(".modal-content:nth-child(3) .modal__header .modal__title")
    };

    getFiltersName(index){
        return $(`.card--accordion-list .rule-details.container .row .rule-detail:nth-child(${index}) .text--strong`)
    };

    ruleCreationByClickingOnRuleBtn(ruleName){
        this.createNewRuleBtn.waitForVisible();
        this.createNewRuleBtn.click();
        browser.pause(2000);
        this.ruleWindowHeading.waitForVisible();
        expect(this.ruleWindowHeading.isVisible()).to.eql(true);
        browser.pause(1000);
        this.ruleNameField.waitForVisible();
        this.ruleNameField.setValue(ruleName);
    };

    createRule(ruleName, name, reciname){
        this.ruleForFree(ruleName,name);
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[reciname,'Enter']);
        browser.pause(2000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        waitUntilTrueFalse(
        `#dissemination_rule_list_search`,
        "5000",
        `Filters is not visible even after 5sec`,
        true
        );
        browser.pause(1000);
        this.searchField.waitForVisible();
        this.searchField.setValue(ruleName);
        this.getRuleNameLabel(1).waitForVisible();
        browser.waitUntil(
        function() {
        return (
            $(".card--accordion-list:nth-child(1) .card__title").getText() === ruleName
        )
        },
        30000,
        "Rule card is not visible even after 30s"
        );
        expect(this.getRuleNameLabel(1).getText()).to.eql(ruleName);
    };

    createRuleForDissemination(ruleName,name,searchname,validation){
        this.ruleForFree(ruleName,name);
        browser.pause(2000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(searchname);
        this.getRuleNameLabel(1).waitForVisible();
        browser.waitUntil(
        function() {
        return (
            $(`.card--accordion-list:nth-child(1) .card__title`).getText() === searchname
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        expect(this.getRuleNameLabel(1).getText()).to.eql(validation);
    };

    ruleCreatation(ruleName, currencyName, name){
        this.rule(ruleName);
        this.filterSearchField.waitForVisible();
        this.filterSearchField.setValue(currencyName);
        this.selectCurrencyFilterOption.waitForVisible();
        this.selectCurrencyFilterOption.click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        browser.pause(1000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
    };

    createRuleFree(ruleName, name,searchname){
        this.ruleForFree(ruleName,name);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(searchname);
        this.getRuleNameLabel(1).waitForVisible();
        browser.waitUntil(
        function() {
        return (
            $(`.card--accordion-list:nth-child(1) .card__title`).getText() === searchname
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        this.getRuleNameLabel(1).waitForVisible();
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewithfree);
    };

    ruleForFree(ruleName,name){
        this.ruleCreationByClickingOnRuleBtn(ruleName);
        this.ruleAllKiidCheckbox.waitForVisible();
        this.ruleAllKiidCheckbox.click();
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
    };

    ruleCreate(ruleName, name){
        this.ruleCreateForSingleOption(ruleName);
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        browser.pause(1000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewithallfilters);
        browser.waitUntil(
        function() {
        return (
            $(`.card--accordion-list:nth-child(1) .card__title`).getText() === ruleName
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        this.getRuleNameLabel(1).waitForVisible();
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewithallfilters);
    };

    chargableRule(ruleName, name){
        this.ruleCreteFirDifferentFilters(ruleName);
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        browser.pause(2000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewithchargable);
        browser.waitUntil(
        function() {
        return (
            $(`.card--accordion-list:nth-child(1) .card__title`).getText() === ruleName
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        this.getRuleNameLabel(1).waitForVisible();
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewithchargable);
    };

    bulkRecipients(ruleName, name, Name){
        this.ruleCreteFirDifferentFilters(ruleName);
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[Name,'Enter']);
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[Name,'Enter']);
        browser.pause(1000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewithbulk);
        browser.waitUntil(
        function() {
        return (
            $(`.card--accordion-list:nth-child(1) .card__title`).getText() === ruleName
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        this.getRuleNameLabel(1).waitForVisible();
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewithbulk);
    };

    freeChargRecipients(ruleName, name, Name){
        this.ruleCreteFirDifferentFilters(ruleName);
        browser.pause(1000);
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[Name,'Enter']);
        browser.pause(1000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewithfreechargable);
        browser.waitUntil(
        function() {
        return (
            $(`.card--accordion-list:nth-child(1) .card__title`).getText() === ruleName
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        this.getRuleNameLabel(1).waitForVisible();
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewithfreechargable);
    };
    
    deleteRule(name){
        browser.refresh();
        waitUntilTrueFalse(
        `#dissemination_rule_list_search`,
        "20000",
        `Rule card is not visible even after 20sec`,
        true
        );
        this.searchField.waitForVisible();
        this.searchField.setValue(name);
        browser.waitUntil(
            function() {
            return (
                $(`.card--accordion-list:nth-child(1) .card__title`).getText() === name
            )
            },
            30000,
            "Kiid listing page is not visible even after 30s"
            );
        waitUntilTrueFalse(
        `.card--accordion-list:nth-child(1) .card__actions .action:nth-child(1)`,
        "20000",
        `Rule card is not visible even after 20sec`,
        true
        );
        this.ruleDeleteIcon.waitForVisible();
        this.ruleDeleteIcon.click();
        this.removeBtn.waitForVisible();
        this.removeBtn.click();
    };

    ruleCreateMultiCurrency(ruleName, name){
        this.rule(ruleName);
        this.selectCurrencyFilterOption.waitForVisible();
        this.selectCurrencyFilterOption.click();
        // this.selectCurrencyOption.waitForVisible()
        // this.selectCurrencyOption.click()
        this.selectLink.waitForVisible();
        this.selectLink.click();
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        browser.pause(1000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewitlmulticurrency);
        this.getRuleNameLabel(1).waitForVisible();
        browser.pause(1000);
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewitlmulticurrency);
    };

    rule(ruleName){
        this.ruleCreationByClickingOnRuleBtn(ruleName);
        this.kiidSubFundFilterDropdown.waitForVisible();
        this.kiidSubFundFilterDropdown.click();
        this.currencyFilterDropdown.waitForVisible();
        this.currencyFilterDropdown.click();
        browser.pause(1000);
        this.selectKiidsFilterDropdown.waitForVisible();
        this.selectKiidsFilterDropdown.click();
    };

    ruleCreateMultiLanguage(ruleName, name){
        this.ruleCreationByClickingOnRuleBtn(ruleName);
        waitUntilTrueFalse(
            `.dropdown--form-control > div:nth-child(1)`,
            "10000",
            `Filters is not visible even after 10sec`,
            true
          );
        this.kiidSubFundFilterDropdown.waitForVisible();
        this.kiidSubFundFilterDropdown.click();
        this.languageFilterDropdown.waitForVisible();
        this.languageFilterDropdown.click();
        browser.pause(1000);
        this.selectKiidsFilterDropdown.waitForVisible();
        this.selectKiidsFilterDropdown.click();
        this.selectCurrencyFilterOption.waitForVisible();
        this.selectCurrencyFilterOption.click();
        this.selectCurrencyOption.waitForVisible();
        this.selectCurrencyOption.click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        browser.pause(1000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewitlmultilanguage);
        browser.pause(1000);
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewitlmultilanguage);
     };

    ruleCreateMultiCountry(ruleName, name){
        this.ruleCreationByClickingOnRuleBtn(ruleName);
        this.kiidSubFundFilterDropdown.waitForVisible();
        this.kiidSubFundFilterDropdown.click();
        this.countryFilterDropdown.waitForVisible();
        this.countryFilterDropdown.click();
        browser.pause(1000);
        this.selectKiidsFilterDropdown.waitForVisible();
        this.selectKiidsFilterDropdown.click();
        this.countryBeFilterOption.waitForVisible();
        this.countryBeFilterOption.click();
        this.countryGbFilterOption.waitForVisible();
        this.countryGbFilterOption.click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        browser.pause(1000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewitlmulticountry);
        browser.pause(1000);
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewitlmulticountry);
    };
    
    ruleCreateMultiSubfund(ruleName, name){
        this.ruleCreationByClickingOnRuleBtn(ruleName);
        this.kiidSubFundFilterDropdown.waitForVisible();
        this.kiidSubFundFilterDropdown.click();
        browser.pause(1000);
        this.selectKiidsFilterDropdown.waitForVisible();
        this.selectKiidsFilterDropdown.click();
        this.getKiidFilterOptionCheckbox(1).waitForVisible();
        this.getKiidFilterOptionCheckbox(1).click();
        this.getKiidFilterOptionCheckbox(2).waitForVisible();
        this.getKiidFilterOptionCheckbox(2).click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        browser.pause(1000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewitlmultisubfund);
        browser.waitUntil(
        function() {
        return (
            $(`.card--accordion-list:nth-child(1) .card__title`).getText() === ruleName
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        this.getRuleNameLabel(1).waitForVisible();
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewitlmultisubfund);
    };

    ruleCreateMultiIsin(ruleName, name){
        this.ruleCreationByClickingOnRuleBtn(ruleName);
        this.kiidSubFundFilterDropdown.waitForVisible();
        this.kiidSubFundFilterDropdown.click();
        waitUntilTrueFalse(
        `//div[contains(text(),' Product APIR Code')]`,
        "10000",
        `Filters is not visible even after 10sec`,
        true
        );
        this.codeFilterDropdown.waitForVisible();
        this.codeFilterDropdown.click();
        browser.pause(15000);
        this.selectKiidsFilterDropdown.waitForVisible();
        this.selectKiidsFilterDropdown.click();
        this.getKiidFilterOptionCheckbox(1).waitForVisible();
        this.getKiidFilterOptionCheckbox(1).click();
        this.getKiidFilterOptionCheckbox(2).waitForVisible();
        this.getKiidFilterOptionCheckbox(2).click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        browser.pause(2000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewitlmultiisin);
        browser.waitUntil(
        function() {
        return (
            $(`.card--accordion-list:nth-child(1) .card__title`).getText() === ruleName
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        this.getRuleNameLabel(1).waitForVisible();
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewitlmultiisin);
    };

    ruleCreateMultiName(ruleName, name){
        this.ruleCreationByClickingOnRuleBtn(ruleName);
        browser.pause(1000);
        this.kiidSubFundFilterDropdown.waitForVisible();
        this.kiidSubFundFilterDropdown.click();
        waitUntilTrueFalse(
        `//div[contains(text(),' Share Class Name')]`,
        "10000",
        `Filters is not visible even after 10sec`,
        true
        );
        this.nameFilterDropdown.waitForVisible();
        this.nameFilterDropdown.click();
        browser.pause(10000);
        this.selectKiidsFilterDropdown.waitForVisible();
        this.selectKiidsFilterDropdown.click();
        this.getKiidFilterOptionCheckbox(1).waitForVisible();
        this.getKiidFilterOptionCheckbox(1).click();
        this.getKiidFilterOptionCheckbox(2).waitForVisible();
        this.getKiidFilterOptionCheckbox(2).click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        browser.pause(1000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewitlmulticlass);
        browser.waitUntil(
        function() {
        return (
            $(`.card--accordion-list:nth-child(1) .card__title`).getText() === ruleName
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        this.getRuleNameLabel(1).waitForVisible();
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewitlmulticlass);
    };

    createRuleMulti(ruleName, name){
        this.ruleCreateForMultipleOption(ruleName);
        this.recipientSearchField.waitForVisible();
        browser.setValue(".choices__input--cloned",[name,'Enter']);
        browser.pause(1000);
        this.saveChangesBtn.waitForVisible();
        this.saveChangesBtn.click();
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.rulenamewithallmultifilter);
        browser.waitUntil(
        function() {
        return (
            $(`.card--accordion-list:nth-child(1) .card__title`).getText() === ruleName
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        this.getRuleNameLabel(1).waitForVisible();
        expect(this.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewithallmultifilter);
    };

    errorForRule(){
        this.ruleCrossIcon.waitForVisible();
        this.ruleCrossIcon.click();
        browser.refresh();
        waitUntilTrueFalse(
        `.btn-primary.create-new-rule`,
        "20000",
        `Rule button is not visible even after 20sec`,
        true
        );
        this.createNewRuleBtn.waitForVisible();
        this.createNewRuleBtn.click();
        this.ruleWindowHeading.waitForVisible();
        expect(this.ruleWindowHeading.isVisible()).to.eql(true);
    };

    ruleCreteFirDifferentFilters(ruleName){
        this.ruleCreationByClickingOnRuleBtn(ruleName);
        this.selectKiidsFilterDropdown.waitForVisible();
        this.selectKiidsFilterDropdown.click();
        this.getKiidFilterOptionCheckbox(1).waitForVisible();
        this.getKiidFilterOptionCheckbox(1).click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
    };

    ruleCreatForDifferentFilter(ruleName){
        this.ruleCreationByClickingOnRuleBtn(ruleName);
        this.selectKiidsFilterDropdown.waitForVisible();
        this.selectKiidsFilterDropdown.click();
        this.getKiidFilterOptionCheckbox(1).waitForVisible();
        this.getKiidFilterOptionCheckbox(1).click();
        this.getKiidFilterOptionCheckbox(2).waitForVisible();
        this.getKiidFilterOptionCheckbox(2).click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
    };


    ruleCreateForSingleOption(ruleName){
        this.ruleCreteFirDifferentFilters(ruleName);
        this.plusBtn.waitForVisible();
        this.plusBtn.click();
        browser.pause(15000);
        this.selectIsinFilterDropdown.waitForVisible();
        this.selectIsinFilterDropdown.click();
        this.getKiidFilterOptionCheckbox(1).waitForVisible();
        this.getKiidFilterOptionCheckbox(1).click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
        this.plusBtn.waitForVisible();
        this.plusBtn.click();
        browser.pause(10000);
        this.selectNameFilterDropdown.waitForVisible();
        this.selectNameFilterDropdown.click();
        this.getKiidFilterOptionCheckbox(1).waitForVisible();
        this.getKiidFilterOptionCheckbox(1).click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
    };

    ruleCreateForMultipleOption(ruleName){
        this.ruleCreatForDifferentFilter(ruleName);
        this.plusBtn.waitForVisible();
        this.plusBtn.click();
        browser.pause(15000);
        this.selectIsinFilterDropdown.waitForVisible();
        this.selectIsinFilterDropdown.click();
        this.getKiidFilterOptionCheckbox(1).waitForVisible();
        this.getKiidFilterOptionCheckbox(1).click();
        this.getKiidFilterOptionCheckbox(2).waitForVisible();
        this.getKiidFilterOptionCheckbox(2).click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
        this.plusBtn.waitForVisible();
        this.plusBtn.click();
        browser.pause(10000);
        this.selectNameFilterDropdown.waitForVisible();
        this.selectNameFilterDropdown.click();
        this.getKiidFilterOptionCheckbox(1).waitForVisible();
        this.getKiidFilterOptionCheckbox(1).click();
        this.getKiidFilterOptionCheckbox(2).waitForVisible();
        this.getKiidFilterOptionCheckbox(2).click();
        this.selectLink.waitForVisible();
        this.selectLink.click();
    };

};
export default new disseminationlist();