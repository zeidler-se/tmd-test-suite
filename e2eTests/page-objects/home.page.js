import Page from "./page"
import { expect } from 'chai'
import testData from "../constants/testData.json"

class Home extends Page{
    
    homePageCard(index){
        return $(`.row.home-sections-list>div:nth-child(${index}) a`)
    }

    get popupCloseBtn(){
        return $(".icon");
    }

    get homePageHeading(){
        return $("h1")
    }

    get avatarImage(){
        return $(".navbar-nav > ul > a");
    }

    get qaAccountAvatar(){
        return $(".dropdown-menu .dropdown-item.py-3:nth-child(4)")
    }

    get settingBtn(){
        return $(".btn.btn-primary");
    }

    get logOutBtn(){
        return $(".btn.btn-outline-primary.ml-2.sign--out");
    }

    get adminPanel(){
        return $(".dropdown-item.d-flex.align-items-center.text-secondary.px-3");
    }

    get popupClose(){
        return $("#zeidlerServicesPromotionClose");
    }

    get kiidGeneratorCard(){
        return $(".container .row.home-sections-list .col-sm-12.col-md-6.col-lg-4.mb-4:nth-child(4) .card__footer .btn")
    }
}

export default new Home();
