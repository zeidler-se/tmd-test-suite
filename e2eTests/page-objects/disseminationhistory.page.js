import Page from "./page"
import testData from "../constants/testData.json"
import disseminationLists from "../page-objects/disseminationlist.page"

class disseminationhistory extends Page{
    get disseminationHistoryLink(){
        return $(".page-header__nav .nav-item:nth-child(3)")
    }

    get sortByDropdownBtn(){
        return $(".btn-outline-primary.dropdown-toggle")
    }

    get ruleAscendingOption(){
        return $(".dropdown-menu.show :nth-child(1)")
    }

    get ruleDescendingOption(){
        return $(".dropdown-menu.show :nth-child(2)")
    }

    get ruleDateAscendingOption(){
        return $(".dropdown-menu.show :nth-child(3)")
    }

    get ruleDateDescendingOption(){
        return $(".dropdown-menu.show :nth-child(4)")
    }

    get searchField(){
        return $("#dissemination_history_list_search")
    }

    get disseminationDate(){
        return $(".dissemination-history-list>div:nth-child(1) .details__list")
    }

    get disseiminationHistoryCardIcon(){
        return $(".dissemination-history-list>div:nth-child(1) .card-onclick__target")
    }

    get ruleRecipientName(){
        return $(".dissemination-history-list>div:nth-child(1) .col-md-6:nth-child(1) p")
    }

    get ruleCardRecipientName(){
        return $(".dissemination-history-list>div:nth-child(1) .col-md-6:nth-child(1) span:nth-child(1)")
    }

    get cardRecipientName(){
        return $(".dissemination-history-list>div:nth-child(1) .col-md-6:nth-child(1) span:nth-child(2)")
    }

    get totalNumberDocument(){
        return $(".dissemination-history-list>div:nth-child(1) .col-md-6:nth-child(2) p")
    }

    get successIcon(){
        return $(".dissemination-history-list>div:nth-child(1) span:nth-child(1) svg")
    }

    get failedIcon(){
        return $(".dissemination-history-list>div:nth-child(1) span:nth-child(2) svg")
    }

    get downloadDocumentBtn(){
        return $(".dissemination-history-list>div:nth-child(1) .collapse.show:nth-child(2) .btn.ml-3")
    }

    get downloadBtn(){
        return $(".dissemination-history-list>div:nth-child(1) .btn-outline-primary:nth-child(1)")
    }

    get titleHeading(){
        return $(".page-header .container .col-sm-12.col-lg-auto .btn__content.choices__item--selectable")
    };

    disseminationStatus(){
        browser.refresh();
        this.searchField.waitForVisible();
        this.searchField.setValue(testData.disseminationlist.kiidsrecordnamelabel);
        browser.waitUntil(
        function() {
        return (
            disseminationLists.ruleLabelName.getText() === testData.disseminationlist.kiidsrecordnamelabel
        )
        },
        30000,
        "Kiid listing page is not visible even after 30s"
        );
        this.disseiminationHistoryCardIcon.waitForVisible();
        this.disseiminationHistoryCardIcon.click();
    };

}
export default new disseminationhistory();