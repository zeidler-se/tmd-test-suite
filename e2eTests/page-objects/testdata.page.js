import Page from "./page"
class Login extends Page{
    get emailField(){
        return $("#user_email.form-control")
    }

    get passwordField(){
        return $("#user_password")
    }

    get signInBtn(){
        return $(".col-sm-auto.btn.btn-primary")
    }

    get homePageHeading(){
        return $("h1")
    }

    get avatarImage(){
        return $(".avatar.avatar--no-image");
    }

    get settingButton(){
        return $(".btn.btn-primary");
    }

    get logOut(){
        return $(".btn.btn-outline-primary.ml-2.sign--out");
    }

    get adminPanel(){
        return $(".dropdown-item.d-flex.justify-content-between.align-items-center.text-secondary.px-3");
    }

    get popupClose(){
        return $("#zeidlerServicesPromotionClose");
    }

    get kiidGeneratorCard(){
        return $(".container .row.home-sections-list .col-sm-12.col-md-6.col-lg-4.mb-4:nth-child(4) .card__footer .btn")
    }

    get kiidsLink(){
        return $(".page-header .col-sm-12.col-lg-auto.ml-lg-auto.align-self-end.page-header__nav .nav-item:nth-child(1)")
    }

    get disseminationLists(){
        return $(".page-header .col-sm-12.col-lg-auto.ml-lg-auto.align-self-end.page-header__nav .nav-item:nth-child(2)")
    }

    get disseminationHistory(){
        return $(".page-header .col-sm-12.col-lg-auto.ml-lg-auto.align-self-end.page-header__nav .nav-item:nth-child(3)")
    }

    get zeidlerDropdown(){
        return $(".page-header .container .col-sm-12.col-lg-auto.page-header__details.kiid_generator_header .dropdown-toggle")
    }

    get searchField(){
        return $(".dropdown-choices__menu input")
    }

    get downloadBtn(){
        return $(".page-action.d-none.d-lg-inline:nth-child(1)")
    }

    get approveBtn(){
        return $(".page-action.d-none.d-lg-inline:nth-child(2)")
    }

    get reviewBtn(){
        return $(".page-action.page-action--preview.col-12.col-lg-auto")
    }

    get subFundFilter(){
        return $(".page-actions.page-actions--dark .page-action.page-action--filters.col-12.col-md-auto .filter.dropdown:nth-child(1)")
    }

    get isinFilter(){
        return $(".page-actions.page-actions--dark .page-action.page-action--filters.col-12.col-md-auto .filter.dropdown:nth-child(2)")
    }

    get nameFilter(){
        return $(".page-actions.page-actions--dark .page-action.page-action--filters.col-12.col-md-auto .filter.dropdown:nth-child(3)")
    }

    get currencyFilter(){
        return $(".page-actions.page-actions--dark .page-action.page-action--filters.col-12.col-md-auto .filter.dropdown:nth-child(4)")
    }

    get languageFilter(){
        return $(".page-actions.page-actions--dark .page-action.page-action--filters.col-12.col-md-auto .filter.dropdown:nth-child(5)")
    }

    get statusFilter(){
        return $(".page-actions-accordion.page-actions-accordion--filters.collapse.show .filter.dropdown:nth-child(1)")
    }

    get countryFilter(){
        return $(".page-actions-accordion.page-actions-accordion--filters.collapse.show .filter.dropdown:nth-child(2)")
    }

    get filterDropdown(){
        return $(".btn.btn-secondary.btn-caret")
    }

    get subFundCheckbox(){
        return $(".table__header .table__row--outer .table__row.row .table__cell.table__cell--select.col.is-switch .custom-control.custom-checkbox")
    }

    get reviewIcon(){
        return $("#kiid_list_page .table__row--outer:nth-child(2) .action-group .action.action--arrow-right")
    }
    
    get readNoteIcon(){
        return $("#kiid_list_page .table__row--outer:nth-child(2) .action-group .action.action--read-note")
    }

    get createNoteIcon(){
        return $("#kiid_list_page .table__row--outer:nth-child(2) .action-group .action.action--create-note")
    }

    get downloadIcon(){
        return $("#kiid_list_page .table__row--outer:nth-child(2) .action-group .action.action--download")
    }

    get reviewPageHeading(){
        return $(".h5.page-header__title")
    }
    get approvalbtn(){
        return $(".kiid-app-tab-in li:nth-child(2) a")
    }
}
export default new Login();