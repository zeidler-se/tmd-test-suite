import Page from "./page"
import testData from "../constants/testData.json"
import Users from "../page-objects/users.page";
import tmdRecipant from "./tmdadminrecipant.page"
import tmdAdmin from "./tmdadmin.page"
import { expect } from 'chai'
import Login from "../page-objects/login.page";
import { waitUntilTrueFalse } from "../page-objects/function";

class accounts extends Page{
    get accountsHeading(){
        return $(".page-details__title")
    };

    getEditIcon(index,index_1,index_2){
        return $(`table tbody tr:nth-child(${index}) td:nth-child(${index_1}) ul li:nth-child(${index_2}) a`)
    };

    get groupTab(){
        return $("//a[contains(text(),'Groups')]")
    };

    get fundName(){
        return $("//a[contains(text(),'QA Calibre Investment Management')]")
    };

    get kiidApprovalTab(){
        return $("//a[contains(text(),'Kiid Approvers')]")
    };

    get addKiidApprovalBtn(){
        return $(".btn--primary.has-icon")
    };

    get addApprovalHeading(){
        return $(".modal-content .modal-content__header")
    };

    get accountGroupDropdown(){
        return $(`#kiid_approver_form .form__group:nth-child(2) .choices.choices--single-select .choices__inner`)
    };

    get approvalLevelDropdown(){
        return $("#kiid_approver_level")
    };

    get titleField(){
        return $("input#kiid_approver_title.control")
    };

    get accountDropdown(){
        return $(".choices__list.choices__list--dropdown .choices__input.choices__input--cloned")
    };

    getApprovalTitle(index,index_1){
        return $(`tr:nth-child(${index}) td:nth-child(${index_1})`)
    };

    get groupName(){
        return $("input#account_group_name")
    };

    get groupTypeDropdown(){
        return $("#account_group_group_type_id.choices.choices--single-select .choices__inner")
    };

    get cancelButton(){
        return $("//button[contains(text(),'Cancel')]")
    };

    getGroupsEyeIcon(index,index_1,index_2){
        return $(`tr:nth-child(${index}) td:nth-child(${index_1}) ul .actions__item:nth-child(${index_2})`)
    };

    get usersField(){
        return $("input.choices__input.choices__input--cloned")
    };

    get duplicateApprovalFlashMessage(){
        return $("label.invalid-feedback.message:nth-child(2)")
    };

    createApproval(groupname,level,grouptitle){
        this.userGroupCreation();
        this.accountGroupDropdown.waitForVisible();
        this.accountGroupDropdown.click();
        this.accountDropdown.waitForVisible();
        browser.setValue(".choices__list.choices__list--dropdown .choices__input.choices__input--cloned",[groupname,'Enter']);
        this.approvalLevelDropdown.waitForVisible();
        browser.setValue("#kiid_approver_level",[level,'Enter']);
        this.titleField.waitForVisible();
        this.titleField.setValue(grouptitle);
        tmdRecipant.clientSaveBtn.waitForVisible();
        tmdRecipant.clientSaveBtn.click();
    };

    createGroup(groupname,grouptype){
        this.userGroupCreation();
        this.groupName.waitForVisible();
        this.groupName.setValue(groupname);
        this.groupTypeDropdown.waitForVisible();
        this.groupTypeDropdown.click();
        this.accountDropdown.waitForVisible();
        browser.setValue(".choices__list.choices__list--dropdown .choices__input.choices__input--cloned",[grouptype,'Enter']);
        tmdRecipant.clientSaveBtn.waitForVisible();
        tmdRecipant.clientSaveBtn.click();
    };

    addUserToGroup(useremail){
        this.userGroupCreation();
        this.usersField.waitForVisible();
        this.usersField.click();
        browser.setValue("input.choices__input.choices__input--cloned",[useremail,'Enter']);
        this.addApprovalHeading.waitForVisible();
        this.addApprovalHeading.click();
        browser.pause(1000);
        tmdRecipant.clientSaveBtn.waitForVisible();
        tmdRecipant.clientSaveBtn.click();
    };

    userGroupCreation(){
        this.addKiidApprovalBtn.waitForVisible();
        this.addKiidApprovalBtn.click();
        this.addApprovalHeading.waitForVisible();
        expect(this.addApprovalHeading.isVisible()).to.eql(true);
    };

    accountGroupTab(){
        Login.adminLogin();
        browser.url(testData.zeidler.accountspagelink);
        expect(browser.getUrl()).to.eql(testData.zeidler.accountspagelink);
        this.accountsHeading.waitForVisible();
        expect(this.accountsHeading.getText()).to.eql(testData.pageheadings.accounts);
        tmdAdmin.searchField.waitForVisible();
        browser.setValue("#search",['QA Calibre Investment Management','Enter']);
        browser.waitUntil(
        function() {
        return (
            tmdRecipant.clientQaName.getText() === testData.kiidfilteroption.fundname
        )
        },
        30000,
        "Accounts page is not visible even after 30s"
        );
        tmdRecipant.clientQaName.waitForVisible();
        expect(tmdRecipant.clientQaName.getText()).to.eql(testData.kiidfilteroption.fundname);
        waitUntilTrueFalse(
        `.sidebar__signout .has-icon`,
        "5000",
        `Edit icon is not visible even after 5sec`,
        true
        );
        this.getEditIcon(1,9,2).waitForVisible();
        this.getEditIcon(1,9,2).click();
        browser.waitUntil(
        function() {
        return (
            $(`.page-details__title`).getText() === testData.kiidfilteroption.fundname
        )
        },
        30000,
        "Accounts page is not visible even after 30s"
        );
        this.accountsHeading.waitForVisible();
        expect(this.accountsHeading.getText()).to.eql(testData.kiidfilteroption.fundname);
        this.groupTab.waitForVisible();
        this.groupTab.click();
    };

};
export default new accounts();