import Page from "./page"
import { expect } from 'chai'
import testData from "../constants/testData.json"
import Home from "../page-objects/home.page"

class Login extends Page{
    get emailField(){
        return $("#user_email")
    }

    get passwordField(){
        return $("#user_password")
    }

    get signInBtn(){
        return $(".col-sm-auto.btn.btn-primary")
    }

    get homePageHeading(){
        return $(".h1")
    }

    get flashMsgText(){
        return $(".flash-message__text");
    }

    get userAvatar(){
        return $(".navbar-nav.ml-auto")
    }

    get settingsBtn(){
        return $(".border-bottom .btn.btn-primary")
    }

    get logOutBtn(){
        return $(".sign--out");
    }

    get adminPanelBtn(){
        return $(".text-secondary.px-3")
    }
    get acceptCookies(){
        return $("#closeCookies")
    };

    login(url,userName, password){
        this.loginMethod(url);
        browser.pause(2000);
        this.emailField.waitForVisible();
        this.emailField.setValue(userName);
        this.passwordField.waitForVisible();
        this.passwordField.setValue(password);
        this.signInBtn.waitForVisible();
        this.signInBtn.click();
        this.homePageHeading.waitForVisible();
        //this.checkEnvironment();
    };

    loginwithcookies(url,userName, password){
        this.loginMethod(url);
        this.acceptCookies.waitForVisible();
        this.acceptCookies.click();
        browser.pause(2000);
        this.emailField.waitForVisible();
        this.emailField.setValue(userName);
        this.passwordField.waitForVisible();
        this.passwordField.setValue(password);
        this.signInBtn.waitForVisible();
        this.signInBtn.click();
        this.homePageHeading.waitForVisible();
    };

    loginMethod(url){
        browser.url(url);
        // browser.windowHandleFullscreen()
        var windowHandle = browser.windowHandle();
        console.log(windowHandle);
        browser.windowHandleMaximize('{'+windowHandle.value+'}');
    };

    logout(){
        Home.avatarImage.waitForVisible();
        Home.avatarImage.click();
        Home.logOutBtn.waitForVisible();
        Home.logOutBtn.click();
        browser.pause(1000);
        expect(browser.getUrl()).to.eql(testData.zeidler.qaadmin);
    };

    adminLogin(){
        this.login(testData.zeidler.qaadminhome, testData.login.qaadminusername, testData.login.qaadminpwd);
        expect(browser.getUrl()).to.eql(testData.zeidler.qaadminhome);
        Home.avatarImage.waitForVisible();
        Home.avatarImage.click();
        Home.adminPanel.waitForVisible();
        Home.adminPanel.click();
        expect(browser.getUrl()).to.eql(testData.zeidler.qaadminpage);
    };

    adminLoginWithCookies(){
        this.loginwithcookies(testData.zeidler.qaadminhome, testData.login.qaadminusername, testData.login.qaadminpwd);
        expect(browser.getUrl()).to.eql(testData.zeidler.qaadminhome);
        Home.avatarImage.waitForVisible();
        Home.avatarImage.click();
        Home.adminPanel.waitForVisible();
        Home.adminPanel.click();
        expect(browser.getUrl()).to.eql(testData.zeidler.qaadminpage);
    };

    adminLogOut(){
        browser.url(testData.zeidler.qaadminhome);
        this.userAvatar.waitForVisible();
        this.userAvatar.click();
        this.logOutBtn.waitForVisible();
        this.logOutBtn.click();
        this.emailField.waitForVisible();
        expect(browser.getUrl()).to.eql(testData.zeidler.qaadmin);
        expect(this.emailField.getAttribute("placeholder")).to.eql("name@email.com");
    };

    GotoEMTManager(){
        browser.url(testData.zeidler.qaadminhome);
        Home.homePageCard(1).waitForVisible();
        browser.url(testData.zeidler.qaemt);
        this.checkEnvironment();
        browser.pause(5000);
        // this.userAvatar.waitForVisible();
        if(this.userAvatar.isVisible() == true){
            this.userAvatar.click();
            this.adminPanelBtn.waitForVisible();
            this.adminPanelBtn.click();
        }
    };

    checkEnvironment(){
        var url = browser.getUrl();
        url = url.slice(8, 11);
        if(url != "staging"){
            browser.close();
        }
    };

    launchUrl(){
        browser.url(testData.zeidler.qakiidrecipientpage);
        expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
    };

};

export default new Login();
