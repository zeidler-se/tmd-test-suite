import Page from "./page"
import testData from "../constants/testData.json"

class tmdadmin extends Page{
    get searchField(){
        return $("#search")
    }

    get totalKiidsCount(){
        return $(".page-details__tags li:nth-child(1)")
    }

    get showDropdown(){
        return $("#page_size")
    }

    get showDropdownOption(){
        return $("#page_size option:nth-child(2)")
    }

    get approvedKiidCheckbox(){
        return $("tr:nth-child(1) td:nth-child(1) .custom-control--checkbox")
    }

    get approvedKiidsCheckbox(){
        return $("tr:nth-child(2) td:nth-child(1) .custom-control--checkbox")
    }

    get kiidBulkActionDropdown(){
        return $(".choices--bulk-actions.bulk-actions__select")
    }

    get kiidDeleteOption(){
        return $(".choices__list--dropdown")
    }

    get kiidApplyBtn(){
        return $(".bulk-actions__apply.show")
    }

    get approvedKiidsBtn(){
        return $(".nav.nav--tabs li:nth-child(1) a")
    }

    get kiidApprovalBtn(){
        return $(".nav.nav--tabs li:nth-child(2) a")
    }

    get filterBtn(){
        return $("#filter-button")
    }

    accountFilterDropdown(index){
        return $(`.page-collapses__item .page-collapses__item-inner .row .filter.col-6:nth-child(${index}) input`)
    }

    get applyFilterBtn(){
        return $(".btn--full.text--white.remote-filters-submit-button")
    }

    get umbrellaFundDropdown(){
        return $("#filter_fund")
    }

    getEyeIcon(index,index1,index2){
        return $(`table tbody tr:nth-child(${index}) td:nth-child(${index1}) .actions .actions__item:nth-child(${index2})`)
    }

    get historyPageHeading(){
        return $(".content__header .container .page-details__title")
    }

    get previousIcon(){
        return $(".pagination li:nth-child(1)")
    }

    get nextIcon(){
        return $(".pagination li:nth-child(14)")
    }

    get secondNextIcon(){
        return $(".pagination li:nth-child(3)")
    }

    get firstPreviousIcon(){
        return $(".pagination li:nth-child(3)")
    }

    get versionsValue(){
        return $("table tbody tr:nth-child(1) td:nth-child(9)")
    }

    get historyPageUpArrowBtn(){
        return $(".btn--icon-only.has-icon:nth-child(1)")
    }

    get changeStatusOption(){
        return $("//div[contains(text(), 'Change Status')]")
    }

    get downloadSummaryBtn(){
        return $(".btn-download-summary")
    }

    get reportsDropdown(){
        return $(".btn-secondary.dropdown-toggle.btn--primary")
    }

    get kiidRecordCheckbox(){
        return $("tr:nth-child(1) td:nth-child(1) .custom-control--checkbox")
    };

    getStatusChangeOption(index){
        return $(`.choices--single-select .choices__item--selectable:nth-child(${index})`)
    };

    get StatusChangeOption(){
        return $(".choices--single-select .choices__item--selectable:nth-child(7)")
    };

    getFiledOption(index){
        return $(`.choices--single-select .choices__item--selectable:nth-child(${index})`)
    };

    get selectBtn(){
        return $(".choices--single-select.bulk-actions__select")
    };

    get applyToBtn(){
        return $(".bulk-actions__apply.kiid_generate_apply_btn")
    };

    get allKiidLink(){
        return $(".breadcrumb li:nth-child(3)")
    }

    get loadingStyle(){
        return $(".filters-loading")
    }

    get adminPageHeading(){
        return $(".page-details__title")
    };

    get generatedKidsTab(){
        return $("//a[contains(text(),'Generated TMDs')]")
    };

    get kiidDocumentTab(){
        return $("//a[contains(text(),'TMDs Document Comments')]")
    };

    getApprovalGroupName(index){
        return $(`.content__body .row .card--accordion-list:nth-child(${index}) h4.card__title.legal_heading`)
    };

    getApprovalComments(index,index_1,index_2){
        return $(`.content__body .row .card--accordion-list:nth-child(${index}) .card__body tbody tr:nth-child(${index_1}) td:nth-child(${index_2})`)
    };

    getAddCommentBtn(index){
        return $(`.content__body .row .card--accordion-list:nth-child(${index}) .btn--primary.add_comment_margin`)
    };

    get importDataBtn(){
        return $(".btn--primary.has-icon");
    };

    get chooseFileBtn(){
        return $("input#file-upload-button")
    };

    get importBtn(){
        return $(".btn--primary")
    };

    getIsinNameInApproval(index,index1){
        return $(`tr:nth-child(${index}) td:nth-child(${index1})`)
    };

    get generateTMD(){
        return $("//div[contains(text(), 'Generate TMDs')]")
    };
    
    get commenTextAreaField(){
        return $(".modal-content .modal-content__body #comment")
    };

    get commentSaveBtn(){
        return $(".modal-content__footer .btn--primary")
    };

    get isinLink(){
        return $("tbody tr:nth-child(1) th")
    };

    getDeleteIcon(index,index1,index2){
        return $(`tr:nth-child(${index}) td:nth-child(${index1}) ul li:nth-child(${index2})`)
    }

    get noRecordFoundMessage(){
        return $("#kiids-data-table .page-empty h3")
    }
};
export default new tmdadmin();