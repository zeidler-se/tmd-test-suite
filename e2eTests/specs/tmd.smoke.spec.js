import testData from "../constants/testData.json";
import { expect } from 'chai'
import Login from "../page-objects/login.page"
import {addTestRailResult} from "../helpers/apihelpers"
import Home from "../page-objects/home.page"
import tmdRecordingList from "../page-objects/tmdrecordlisting.page"
import tmdRecipant from "../page-objects/tmdadminrecipant.page"
import tmdAdmin from "../page-objects/tmdadmin.page"
import disseminationLists from "../page-objects/disseminationlist.page"
import disseminationHistory from "../page-objects/disseminationhistory.page"
import disseminationTransaction from "../page-objects/disseminationtransaction.page"
import { waitUntilTrueFalse } from "../page-objects/function";
import Accounts from "../page-objects/accounts.page";
import Users from "../page-objects/users.page";
import disseminationhistoryPage from "../page-objects/disseminationhistory.page"


var passObj = {
  status_id: 1,
  comment: "Pass - Automation"
}
var failObj = {
  status_id: 5,
  comment: "Fail - Automation"
}
var pass = 0

var userEmail;
var csvEmail;
var sftkeyEmail;
var sftpwdEmail;

let i;

const fsExtra = require("fs-extra");
const pathfile = require("path");
const folderpath = pathfile.join(
  __dirname,
  "..",
  "downloads"
); 

describe.skip("Uploading Standing data", ()=>{
  it("Admin user is able to upload the TMD v1.3 standing data", ()=>{
    Login.adminLoginWithCookies();
    browser.url(testData.zeidler.tmdapprovallink);
    expect(browser.getUrl()).to.eql(testData.zeidler.tmdapprovallink);
    tmdAdmin.importDataBtn.waitForVisible();
    tmdAdmin.importDataBtn.click();
    browser.waitUntil(
      function() {
      return (
          browser.isVisible(
          '.page-details__title'
          ) === true
      );
      },
      20000,
      "talk popup not displayed after 20s"
    );
    tmdAdmin.adminPageHeading.waitForVisible();
    expect(tmdAdmin.adminPageHeading.getText()).to.eql(testData.tmdstandingdata.headingtitle);
    const path = require("path");
    const toUpload = path.join(__dirname, "..", "..", "resources", "TMD_Standing_Data_v1.3.xlsx");
    browser.chooseFile('input[type="file"]', toUpload);
    tmdAdmin.importBtn.waitForVisible();
    tmdAdmin.importBtn.click();
    browser.waitUntil(
      function() {
      return (
          browser.isVisible(
          '.page-details__title'
          ) === true
      );
      },
      20000,
      "talk popup not displayed after 20s"
    );
    tmdAdmin.adminPageHeading.waitForVisible();
    expect(tmdAdmin.adminPageHeading.getText()).to.eql(testData.tmdstandingdata.alltmdheading);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QCE030000007AU','Enter']);
    // tmdAdmin.isinLink.waitForVisible();
    // expect(tmdAdmin.isinLink.getText()).to.eql(testData.tmdapprovaltab.isinname);
    tmdAdmin.getIsinNameInApproval(1,8).waitForVisible();
    expect(tmdAdmin.getIsinNameInApproval(1,8).getText()).to.eql(testData.tmdapprovaltab.dataadded);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79434",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79434",
        failObj
        )
      }
  })

  it("Admin user is able to upload the updated TMD v1.3 standing data", ()=>{
    tmdAdmin.importDataBtn.waitForVisible();
    tmdAdmin.importDataBtn.click();
    browser.waitUntil(
      function() {
      return (
          browser.isVisible(
          '.page-details__title'
          ) === true
      );
      },
      20000,
      "talk popup not displayed after 20s"
    );
    tmdAdmin.adminPageHeading.waitForVisible();
    expect(tmdAdmin.adminPageHeading.getText()).to.eql(testData.tmdstandingdata.headingtitle);
    const path = require("path");
    const toUpload = path.join(__dirname, "..", "..", "resources", "TMD-Standing_data_updated.xlsx");
    browser.chooseFile('input[type="file"]', toUpload);
    tmdAdmin.importBtn.waitForVisible();
    tmdAdmin.importBtn.click();
    browser.waitUntil(
      function() {
      return (
          browser.isVisible(
          '.page-details__title'
          ) === true
      );
      },
      20000,
      "talk popup not displayed after 20s"
    );
    tmdAdmin.adminPageHeading.waitForVisible();
    expect(tmdAdmin.adminPageHeading.getText()).to.eql(testData.tmdstandingdata.alltmdheading);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QCE030000007AU','Enter']);
    // tmdAdmin.isinLink.waitForVisible();
    // expect(tmdAdmin.isinLink.getText()).to.eql(testData.tmdapprovaltab.isinname);
    tmdAdmin.getIsinNameInApproval(1,8).waitForVisible();
    expect(tmdAdmin.getIsinNameInApproval(1,8).getText()).to.eql(testData.tmdapprovaltab.dataupdated);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79420",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79420",
        failObj
        )
      }
  })

  it("Admin is able to see the Generating pdf status When the user change the status to Generating pdf in TMD Approval page", ()=>{
    tmdAdmin.approvedKiidCheckbox.waitForVisible();
    tmdAdmin.approvedKiidCheckbox.click();
    tmdAdmin.kiidBulkActionDropdown.waitForVisible();
    tmdAdmin.kiidBulkActionDropdown.click();
    tmdAdmin.generateTMD.waitForVisible();
    tmdAdmin.generateTMD.click();
    browser.pause(1000);
    tmdAdmin.applyToBtn.waitForVisible();
    tmdAdmin.applyToBtn.click();
    browser.waitUntil(
      function() {
      return (
          browser.isVisible(
          '.page-details__title'
          ) === true
      );
      },
      20000,
      "talk popup not displayed after 20s"
    );
    tmdAdmin.getIsinNameInApproval(1,8).waitForVisible();
    expect(tmdAdmin.getIsinNameInApproval(1,8).getText()).to.eql(testData.tmdapprovaltab.generatingpdf);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79432",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79432",
        failObj
        )
      }
  })

  it("Admin user is able to generate the pdf for new uploaded records and status should be changed to 'Awaiting Zeidler review'", ()=>{
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QCE030000007AU','Enter']);
    tmdAdmin.getIsinNameInApproval(1,8).waitForVisible();
    expect(tmdAdmin.getIsinNameInApproval(1,8).getText()).to.eql(testData.tmdapprovaltab.zeilderreview);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79421",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79421",
        failObj
        )
      }
  })

  it("Admin user is able to change the status from 'Awaiting Zeidler review' to 'Awaiting client approval'", ()=>{
    tmdAdmin.approvedKiidCheckbox.waitForVisible();
    tmdAdmin.approvedKiidCheckbox.click();
    tmdAdmin.kiidBulkActionDropdown.waitForVisible();
    tmdAdmin.kiidBulkActionDropdown.click();
    tmdAdmin.changeStatusOption.waitForVisible();
    tmdAdmin.changeStatusOption.click();
    browser.pause(1000);
    tmdAdmin.selectBtn.waitForVisible();
    tmdAdmin.selectBtn.click();
    tmdAdmin.getStatusChangeOption(7).waitForVisible();
    tmdAdmin.getStatusChangeOption(7).click();
    tmdAdmin.applyToBtn.waitForVisible();
    tmdAdmin.applyToBtn.click();
    browser.waitUntil(
      function() {
      return (
          browser.isVisible(
          '.page-details__title'
          ) === true
      );
      },
      20000,
      "Approval page not displayed after 20s"
    );
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QCE030000007AU','Enter']);
    tmdAdmin.getIsinNameInApproval(1,8).waitForVisible();
    expect(tmdAdmin.getIsinNameInApproval(1,8).getText()).to.eql(testData.tmdapprovaltab.awaitingclientapproval);
    tmdRecipant.logout();
    Login.login(testData.zeidler.uatadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", ['QA Calibre Investment Management','Enter']);
    waitUntilTrueFalse(
      `.filters-container>div:nth-child(2) .btn span`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getFilterBtn(2).waitForVisible();
    tmdRecordingList.getFilterBtn(2).click();
    tmdRecordingList.filterSearchField.waitForVisible();
    tmdRecordingList.filterSearchField.setValue(testData.tmdapprovaltab.isinname);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.kiidIsinFilterOption.getText() === testData.tmdapprovaltab.isinname
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    tmdRecordingList.kiidIsinFilterOption.waitForVisible();
    tmdRecordingList.kiidIsinFilterOption.click();
    waitUntilTrueFalse(
      `#kiid_list_page > div:nth-child(2) .table__cell--status.col`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getKiidRecordStatus(2).waitForVisible();
    expect(tmdRecordingList.getKiidRecordStatus(2).getText()).to.eql(testData.kiidfilteroption.status);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79422",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79422",
        failObj
        )
      }
  })

  it("User should be able to add the comments on Client side", ()=>{
    tmdRecordingList.zeidlerQASubFundCheckbox.waitForVisible();
    tmdRecordingList.zeidlerQASubFundCheckbox.click();
    tmdRecordingList.getCreateNoteIcon(2).waitForVisible();
    tmdRecordingList.getCreateNoteIcon(2).click();
    tmdRecordingList.addCommentsForRecords(testData.comments.approvalcomments);
    tmdRecordingList.saveNoteBtn.waitForVisible();
    tmdRecordingList.saveNoteBtn.click();
    tmdRecordingList.approvalPageHeading.waitForVisible();
    browser.waitUntil(
      function() {
      return (
        tmdRecordingList.approvalPageHeading.isVisible() === true
      )
    },
    30000,
    "Approval page is not visible even after 30s"
    );
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitleforkiid){
        tmdRecordingList.getGroupLabelName(2,i,2).waitForVisible();
        tmdRecordingList.getGroupLabelName(2,i,2).click(); 
        tmdRecordingList.getCommentName(i,3).waitForVisible();
        expect(tmdRecordingList.getCommentName(i,3).getText()).to.eql(testData.comments.approvalcomments);
        break
        };
      };
    browser.scroll(-0,-500);
    waitUntilTrueFalse(
      `.text-primary`,
      "5000",
      `Back link is not visible even after 5sec`,
      true
    );
    tmdRecordingList.backLink.waitForVisible();
    tmdRecordingList.backLink.click();
    browser.waitUntil(
      function() {
      return (
        tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79423",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79423",
        failObj
        )
      }
  })

  it("Status of the TMD is not changed when a Notes added is saved as a note by clicking on the Save Note button", ()=>{
    waitUntilTrueFalse(
      `#kiid_list_page > div:nth-child(2) .table__cell--status.col`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getKiidRecordStatus(2).waitForVisible();
    expect(tmdRecordingList.getKiidRecordStatus(2).getText()).to.eql(testData.kiidfilteroption.status);
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "20000",
      `avatar image is not visible even after 20sec`,
      true
    );
    Login.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79431",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79431",
        failObj
        )
      }
  })
});

describe.skip("Added comments to the records", ()=>{
  it("Admin is able to view comments added by clients only for respective awaiting client approval status records", ()=>{
    Login.adminLogin();
    browser.url(testData.zeidler.tmdapprovallink);
    expect(browser.getUrl()).to.eql(testData.zeidler.tmdapprovallink);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QCE030000007AU','Enter']);
    tmdAdmin.kiidRecordCheckbox.waitForVisible();
    tmdAdmin.kiidRecordCheckbox.click();
    tmdAdmin.getEyeIcon(1,10,4).waitForVisible();
    tmdAdmin.getEyeIcon(1,10,4).click();
    waitUntilTrueFalse(
      `//a[contains(text(),'Generated TMDs')]`,
      "10000",
      `Generated tab icon is not visible even after 10sec`,
      true
    );
    tmdAdmin.generatedKidsTab.waitForVisible();
    tmdAdmin.generatedKidsTab.click();
    tmdAdmin.getEyeIcon(1,6,2).waitForVisible();
    tmdAdmin.getEyeIcon(1,6,2).click();
    browser.waitUntil(
      function() {
      return (
        tmdAdmin.kiidDocumentTab.isVisible() === true
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    expect(tmdAdmin.kiidDocumentTab.isVisible()).to.eql(true);
    for(i = 1; i<=10; i+=2){
      if(tmdAdmin.getApprovalGroupName(i).getText() === testData.accounts.approvalnameforcomment){
        tmdAdmin.getApprovalGroupName(i).waitForVisible();
        tmdAdmin.getApprovalGroupName(i).click();
        tmdAdmin.getApprovalComments(i,1,3).waitForVisible();
        expect(tmdAdmin.getApprovalComments(i,1,3).getText()).to.eql(testData.comments.approvalcomments);
        break
      };
    };
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79429",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79429",
        failObj
        )
      }
  })

  it("Admin is able to view the comments added by the clients for awaiting client approval status records", ()=>{
    browser.refresh();
    for(i = 1; i<=10; i+=2){
      if(tmdAdmin.getApprovalGroupName(i).getText() === testData.accounts.approvalnameforcomment){
        tmdAdmin.getApprovalGroupName(i).waitForVisible();
        tmdAdmin.getApprovalGroupName(i).click();
        tmdAdmin.getApprovalComments(i,1,3).waitForVisible();
        expect(tmdAdmin.getApprovalComments(i,1,3).getText()).to.eql(testData.comments.approvalcomments);
        break
      };
    };
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79428",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79428",
        failObj
        )
      }
  })

  it("Admin should be able to add the comments for the records in TMDs Document Comments page", ()=>{
    for(i = 1; i<=10; i+=2){
      if(tmdAdmin.getApprovalGroupName(i).getText() === testData.accounts.approvalnameforcomment){
        tmdAdmin.getAddCommentBtn(i).waitForVisible();
        tmdAdmin.getAddCommentBtn(i).click();
        break
      };
    };
    tmdAdmin.commenTextAreaField.waitForVisible();
    tmdAdmin.commenTextAreaField.setValue(testData.comments.approvecomment);
    tmdAdmin.commentSaveBtn.waitForVisible();
    tmdAdmin.commentSaveBtn.click();
    browser.waitUntil(
      function() {
      return (
        tmdAdmin.kiidDocumentTab.isVisible() === true
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    for(i = 1; i<=10; i+=2){
      if(tmdAdmin.getApprovalGroupName(i).getText() === testData.accounts.approvalnameforcomment){
        tmdAdmin.getApprovalGroupName(i).waitForVisible();
        tmdAdmin.getApprovalGroupName(i).click();
        tmdAdmin.getApprovalComments(i,1,3).waitForVisible();
        expect(tmdAdmin.getApprovalComments(i,1,3).getText()).to.eql(testData.comments.approvecomment);
        break
      };
    };
    tmdRecipant.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79424",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79424",
        failObj
        )
      }
  })

  it("Client should be able to view the comments added by admin user", ()=>{
    Login.login(testData.zeidler.uatadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerQASubFundCheckbox.waitForVisible();
    tmdRecordingList.zeidlerQASubFundCheckbox.click();
    tmdRecordingList.getCreateNoteIcon(2).waitForVisible();
    tmdRecordingList.getCreateNoteIcon(2).click();
    tmdRecordingList.approvalPageHeading.waitForVisible();
    browser.waitUntil(
      function() {
      return (
        tmdRecordingList.approvalPageHeading.isVisible() === true
      )
    },
    30000,
    "Approval page is not visible even after 30s"
    );
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitleforkiid){
        tmdRecordingList.getGroupLabelName(2,i,2).waitForVisible();
        tmdRecordingList.getGroupLabelName(2,i,2).click(); 
        browser.scroll(0,200);
        tmdRecordingList.getCommentsText(i,1,3).waitForVisible();
        expect(tmdRecordingList.getCommentsText(i,1,3).getText()).to.eql(testData.comments.approvecomment);
        break
      };
    };
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79425",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79425",
        failObj
        )
      }
  })

  it("Status of records should be changed to Awaiting for comment review when user clicked on zeidler review button", ()=>{
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitleforkiid){
        browser.pause(1000);
        tmdRecordingList.getApproveBtn(2,i,1).waitForVisible();
        tmdRecordingList.getApproveBtn(2,i,1).click(); 
          break
        };
      };
    browser.pause(1000);
    tmdRecordingList.noteField.waitForVisible();
    tmdRecordingList.noteField.setValue(testData.comments.approvecomment);
    tmdRecordingList.zeidlerReviewBtn.waitForVisible();
    tmdRecordingList.zeidlerReviewBtn.click();
    browser.alertAccept();
    tmdRecordingList.approvalPageHeading.waitForVisible();
    browser.waitUntil(
      function() {
      return (
        tmdRecordingList.approvalPageHeading.isVisible() === true
      )
    },
    30000,
    "Approval page is not visible even after 30s"
    );
    browser.scroll(-0,-500);
    waitUntilTrueFalse(
      `.text-primary`,
      "5000",
      `Back link is not visible even after 5sec`,
      true
    );
    tmdRecordingList.backLink.waitForVisible();
    tmdRecordingList.backLink.click();
    browser.waitUntil(
      function() {
      return (
        tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    tmdRecordingList.getKiidRecordStatus(2).waitForVisible();
    expect(tmdRecordingList.getKiidRecordStatus(2).getText()).to.eql(testData.kiidfilteroption.statuslabel);
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "20000",
      `avatar image is not visible even after 20sec`,
      true
    );
    Login.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79427",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79427",
        failObj
        )
      }
  })

  it("User should be able to review the status and change the status from 'Awaiting comments review' to 'Awaiting client approval'", ()=>{
    Login.adminLogin();
    browser.url(testData.zeidler.tmdapprovallink);
    expect(browser.getUrl()).to.eql(testData.zeidler.tmdapprovallink);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QCE030000007AU','Enter']);
    tmdAdmin.approvedKiidCheckbox.waitForVisible();
    tmdAdmin.approvedKiidCheckbox.click();
    tmdAdmin.getIsinNameInApproval(1,8).waitForVisible();
    expect(tmdAdmin.getIsinNameInApproval(1,8).getText()).to.eql(testData.tmdapprovaltab.commentsreview);
    tmdAdmin.kiidBulkActionDropdown.waitForVisible();
    tmdAdmin.kiidBulkActionDropdown.click();
    tmdAdmin.changeStatusOption.waitForVisible();
    tmdAdmin.changeStatusOption.click();
    browser.pause(1000);
    tmdAdmin.selectBtn.waitForVisible();
    tmdAdmin.selectBtn.click();
    tmdAdmin.getStatusChangeOption(7).waitForVisible();
    tmdAdmin.getStatusChangeOption(7).click();
    tmdAdmin.applyToBtn.waitForVisible();
    tmdAdmin.applyToBtn.click();
    browser.waitUntil(
      function() {
      return (
          browser.isVisible(
          '.page-details__title'
          ) === true
      );
      },
      20000,
      "Approval page not displayed after 20s"
    );
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QCE030000007AU','Enter']);
    tmdAdmin.getIsinNameInApproval(1,8).waitForVisible();
    expect(tmdAdmin.getIsinNameInApproval(1,8).getText()).to.eql(testData.tmdapprovaltab.awaitingclientapproval);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79426",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79426",
        failObj
        )
      }
  })

  it("Admin is able to view multiple comments added by clients for awaiting client approval status records", ()=>{
    tmdAdmin.kiidRecordCheckbox.waitForVisible();
    tmdAdmin.kiidRecordCheckbox.click();
    tmdAdmin.getEyeIcon(1,10,4).waitForVisible();
    tmdAdmin.getEyeIcon(1,10,4).click();
    waitUntilTrueFalse(
      `//a[contains(text(),'Generated TMDs')]`,
      "10000",
      `Generated tab icon is not visible even after 10sec`,
      true
    );
    tmdAdmin.generatedKidsTab.waitForVisible();
    tmdAdmin.generatedKidsTab.click();
    tmdAdmin.getEyeIcon(1,6,2).waitForVisible();
    tmdAdmin.getEyeIcon(1,6,2).click();
    browser.waitUntil(
      function() {
      return (
        tmdAdmin.kiidDocumentTab.isVisible() === true
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    expect(tmdAdmin.kiidDocumentTab.isVisible()).to.eql(true);
    for(i = 1; i<=10; i+=2){
      if(tmdAdmin.getApprovalGroupName(i).getText() === testData.accounts.approvalnameforcomment){
        tmdAdmin.getApprovalGroupName(i).waitForVisible();
        tmdAdmin.getApprovalGroupName(i).click();
        tmdAdmin.getApprovalComments(i,1,3).waitForVisible();
        expect(tmdAdmin.getApprovalComments(i,1,3).getText()).to.eql(testData.comments.approvecomment);
        tmdAdmin.getApprovalComments(i,3,3).waitForVisible();
        expect(tmdAdmin.getApprovalComments(i,3,3).getText()).to.eql(testData.comments.approvalcomments);
        break
      };
    };
    tmdRecipant.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79430",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79430",
        failObj
        )
      }
  })
});

describe("Filters on Client side", ()=>{
  it("User is able to filter the Funds name from the list displayed when clicked on the Fund name Filter button", ()=>{
    Login.loginwithcookies(testData.zeidler.uatadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", ['QA Calibre Investment Management','Enter']);
    waitUntilTrueFalse(
      `.filters-container .filter.dropdown:nth-child(1) .btn-dark.dropdown-toggle--no-caret`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.fundNameFilter.waitForVisible();
    tmdRecordingList.fundNameFilter.click();
    tmdRecordingList.getKiidSubfundFilterOption(7).waitForVisible();
    tmdRecordingList.getKiidSubfundFilterOption(7).click();
    waitUntilTrueFalse(
      `#kiid_list_page > div:nth-child(2) .table__cell--sub-fund`,
      "10000",
      `Filters is not visible even after 10sec`,
      true
    );
    tmdRecordingList.kiidRecordSubfund.waitForVisible();
    expect(tmdRecordingList.kiidRecordSubfund.getText()).to.eql(testData.kiidfilteroption.fundname);
    waitUntilTrueFalse(
      `.page-action--filters .filter.dropdown:nth-child(1) .icon--x-small`,
      "5000",
      `Cross icon is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getStatusCrossIcon(1).waitForVisible();
    tmdRecordingList.getStatusCrossIcon(1).click();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79457",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79457",
        failObj
        )
      }
    })

  it("User is able to filter the Product APIR code from the list displayed when clicked on Filter button", ()=>{
    waitUntilTrueFalse(
      `.filters-container>div:nth-child(2) .btn span`,
      "10000",
      `Filters is not visible even after 10sec`,
      true
    );
    browser.pause(1000);
    tmdRecordingList.getFilterBtn(2).waitForVisible();
    tmdRecordingList.getFilterBtn(2).click();
    tmdRecordingList.filterSearchField.waitForVisible();
    tmdRecordingList.filterSearchField.setValue(testData.tmdapprovaltab.isinname);
    browser.pause(4000);
    tmdRecordingList.kiidIsinFilterOption.waitForVisible();
    tmdRecordingList.kiidIsinFilterOption.click();
    waitUntilTrueFalse(
      `#kiid_list_page > div:nth-child(2) .table__cell--status.col`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.kiidRecordIsin.waitForVisible();
    expect(tmdRecordingList.kiidRecordIsin.getText()).to.eql(testData.tmdapprovaltab.isinname);
    waitUntilTrueFalse(
      `.page-action--filters .filter.dropdown:nth-child(2) .icon--x-small`,
      "5000",
      `Cross icon is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getStatusCrossIcon(2).waitForVisible();
    tmdRecordingList.getStatusCrossIcon(2).click();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79458",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79458",
        failObj
        )
      }
  })

  it("User is able to search and filter the Name from the list displayed when clicked on the Name Filter", ()=>{
    waitUntilTrueFalse(
      `.filters-container>div:nth-child(3) .btn span`,
      "10000",
      `Filters is not visible even after 10sec`,
      true
    );
    browser.pause(2000);
    tmdRecordingList.getFilterBtn(3).waitForVisible();
    tmdRecordingList.getFilterBtn(3).click();
    //browser.pause(1000);
    tmdRecordingList.nameSearchField.waitForVisible();
    tmdRecordingList.nameSearchField.setValue(testData.filters.namefilter);
    browser.pause(2000);
    tmdRecordingList.getFilterBtn(3).waitForVisible();
    tmdRecordingList.getFilterBtn(3).click();
    tmdRecordingList.getKiidNameFilterOption(1).waitForVisible();
    browser.waitUntil(
      function() {
        return (
          tmdRecordingList.getKiidNameFilterOption(1).getText() === testData.filters.namefilter
      )
    },
    20000,
    "Filter did not load even after 20s"
    );
    //tmdRecordingList.getKiidNameFilterOption(1).waitForVisible();
    tmdRecordingList.getKiidNameFilterOption(1).click();
    waitUntilTrueFalse(
      `#kiid_list_page > div:nth-child(2) .table__cell--name.col`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.kiidRecordName.waitForVisible();
    expect(tmdRecordingList.kiidRecordName.getText()).to.eql(testData.filters.namefilter);
    waitUntilTrueFalse(
      `.page-action--filters .filter.dropdown:nth-child(3) .icon--x-small`,
      "5000",
      `Cross icon is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getStatusCrossIcon(3).waitForVisible();
    tmdRecordingList.getStatusCrossIcon(3).click();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79459",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79459",
        failObj
        )
      }
    })

  it("user is able to select the Status from the list and filter when clicked on the Status filter button", () =>{
    waitUntilTrueFalse(
      `.filters-container>div:nth-child(4) .btn span`,
      "10000",
      `Filters is not visible even after 10sec`,
      true
    );
    browser.pause(1000);
    tmdRecordingList.getFilterBtn(4).waitForVisible();
    tmdRecordingList.getFilterBtn(4).click();
    tmdRecordingList.getKiidStatusFilterOption(6).waitForVisible();
    tmdRecordingList.getKiidStatusFilterOption(6).click();
    waitUntilTrueFalse(
      `#kiid_list_page > div:nth-child(2) .table__cell--status.col`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getKiidRecordStatus(2).waitForVisible();
    expect(tmdRecordingList.getKiidRecordStatus(2).getText()).to.eql(testData.kiidfilteroption.status);
    waitUntilTrueFalse(
      `.page-action--filters .filter.dropdown:nth-child(4) .icon--x-small`,
      "5000",
      `Cross icon is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getStatusCrossIcon(4).waitForVisible();
    tmdRecordingList.getStatusCrossIcon(4).click();
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "20000",
      `avatar image is not visible even after 20sec`,
      true
    );
    Login.logout();
    pass = 1
   });

   it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79460",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79460",
        failObj
        )
      }
  })
});

describe.skip("Approval process", ()=>{
  it("Admin user can add the User Group Type on the Admin site", ()=>{
    Accounts.accountGroupTab();
    browser.waitUntil(
    function() {
    return (
        Accounts.accountsHeading.isVisible() === true
    )
    },
    10000,
    "Users page is not visible even after 10s"
    );
    Accounts.createGroup(testData.accounts.groupname,testData.accounts.grouptype);
    browser.waitUntil(
    function() {
    return (
        Accounts.accountsHeading.isVisible() === true
    )
    },
    10000,
    "Users page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.pageheadings.groups);
    // Accounts.getApprovalTitle(1,1).waitForVisible();
    // expect(Accounts.getApprovalTitle(1,1).getText()).to.eql(testData.accounts.groupname);
    browser.pause(1000);
    for(i = 1; i<=10; i++){
      if(Accounts.getApprovalTitle(i,1).getText() === testData.accounts.groupname){
      };
    };
    waitUntilTrueFalse(
      `.btn--primary.has-icon`,
      "5000",
      `Add group btn is not visible even after 5sec`,
      true
    );
    Accounts.createGroup(testData.accounts.groupname1,testData.accounts.grouptype);
    browser.waitUntil(
    function() {
    return (
        Accounts.accountsHeading.isVisible() === true
    )
    },
    10000,
    "Users page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.pageheadings.groups);
    Accounts.getApprovalTitle(1,1).waitForVisible();
    expect(Accounts.getApprovalTitle(1,1).getText()).to.eql(testData.accounts.groupname1);
    // for(i = 1; i<=10; i++){
    //   if(Accounts.getApprovalTitle(i,1).getText() === testData.accounts.groupname1){
    //   };
    // };
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79463",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79463",
        failObj
        )
      }
  })

  it.skip("Added User Group to the account is displayed on the respective fund on the Admin side", ()=>{
    //Login.adminLogin();
    browser.url(testData.zeidler.fundpagelink);
    expect(browser.getUrl()).to.eql(testData.zeidler.fundpagelink);
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.pageheadings.fund);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QA Calibre Investment Management','Enter']);
    browser.waitUntil(
      function() {
      return (
        Accounts.fundName.getText() === testData.kiidfilteroption.fundname
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    waitUntilTrueFalse(
      `table tbody tr:nth-child(1) td:nth-child(8) ul li:nth-child(1)`,
      "5000",
      `Edit icon is not visible even after 5sec`,
      true
    );
    Accounts.getEditIcon(1,8,1).waitForVisible();
    Accounts.getEditIcon(1,8,1).click();
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.kiidfilteroption.fundname
      )
    },
    10000,
    "Accounts page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.kiidfilteroption.fundname);
    Accounts.kiidApprovalTab.waitForVisible();
    Accounts.kiidApprovalTab.click();
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.kiidfilteroption.fundname
      )
    },
    10000,
    "Accounts page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.kiidfilteroption.fundname);
    Accounts.addKiidApprovalBtn.waitForVisible();
    Accounts.addKiidApprovalBtn.click();
    Accounts.addApprovalHeading.waitForVisible();
    expect(Accounts.addApprovalHeading.isVisible()).to.eql(true);
    Accounts.accountGroupDropdown.waitForVisible();
    Accounts.accountGroupDropdown.click();
    Accounts.accountDropdown.waitForVisible();
    browser.setValue(".choices__list.choices__list--dropdown .choices__input.choices__input--cloned",[testData.accounts.groupname,'Enter']);
    Accounts.accountGroupDropdown.waitForVisible();
    expect(Accounts.accountGroupDropdown.getText()).to.eql(testData.accounts.groupnametitle);
    Accounts.cancelButton.waitForVisible();
    Accounts.cancelButton.click();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79464",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79464",
        failObj
        )
      }
  })

  it.skip("Admin user can add the TMD Approver to the fund", ()=>{
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.kiidfilteroption.fundname
      )
    },
    10000,
    "Accounts page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.kiidfilteroption.fundname);
    Accounts.createApproval(testData.accounts.groupname,testData.accounts.grouplevel,testData.accounts.grouptitle);
    waitUntilTrueFalse(
      `#search`,
      "10000",
      `Search field is not visible even after 10sec`,
      true
    );
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['TMD Approval automation','Enter']);
    browser.waitUntil(
      function() {
      return (
        Accounts.getApprovalTitle(1,3).getText() === testData.accounts.grouptitle
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    Accounts.getApprovalTitle(1,3).waitForVisible();
    expect(Accounts.getApprovalTitle(1,3).getText()).to.eql(testData.accounts.grouptitle);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79465",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79465",
        failObj
        )
      }
  })
  
  it.skip("Error message is displayed when Admin user tries to add the duplicate TMD Approver to the fund", ()=>{
    Accounts.createApproval(testData.accounts.groupname,testData.accounts.grouplevel,testData.accounts.grouptitle);
    browser.pause(1000);
    Accounts.duplicateApprovalFlashMessage.waitForVisible();
    expect(Accounts.duplicateApprovalFlashMessage.getText()).to.eql(testData.flashmessage.duplicateapprovals);
    Accounts.cancelButton.waitForVisible();
    Accounts.cancelButton.click();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79466",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79466",
        failObj
        )
      }
  })

  it.skip("Admin can give the same level priority to the different Approval Groups on the fund TMD Approvals page", ()=>{
    Accounts.createApproval(testData.accounts.groupname1,testData.accounts.grouplevel,testData.accounts.grouptitle1);
    Accounts.createApproval(testData.accounts.approvalforwithoutaccess,testData.accounts.grouplevel,testData.accounts.grouptitleforwithoutaccess);
    tmdRecipant.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79467",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79467",
        failObj
        )
      }
  })

  it.skip("Approvers Groups are displayed on the client side when Admin adds the Approvers Groups to the Fund on the Admin side",()=>{
    Login.login(testData.zeidler.uatadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", ['QA Calibre Investment Management','Enter']);
    waitUntilTrueFalse(
      `.filters-container>div:nth-child(2) .btn span`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getFilterBtn(2).waitForVisible();
    tmdRecordingList.getFilterBtn(2).click();
    tmdRecordingList.filterSearchField.waitForVisible();
    tmdRecordingList.filterSearchField.setValue(testData.tmdapprovaltab.isinname);
    browser.pause(2000);
    tmdRecordingList.kiidIsinFilterOption.waitForVisible();
    tmdRecordingList.kiidIsinFilterOption.click();
    waitUntilTrueFalse(
      `#kiid_list_page > div:nth-child(2) .table__cell--status.col`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.kiidRecordIsin.waitForVisible();
    expect(tmdRecordingList.kiidRecordIsin.getText()).to.eql(testData.tmdapprovaltab.isinname);
    tmdRecordingList.getCreateNoteIcon(2).waitForVisible();
    tmdRecordingList.getCreateNoteIcon(2).click();
    tmdRecordingList.approvalPageHeading.waitForVisible();
      browser.waitUntil(
      function() {
      return (
          $(`h1.h5.page-header__title`).isVisible() === true
      )
      },
      30000,
    "Approval page is not visible even after 30s"
    );
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitle1){
        tmdRecordingList.getGroupLabelName(2,i,2).waitForVisible();
        expect(tmdRecordingList.getGroupLabelName(2,i,2).isVisible()).to.eql(true);  
        break
        };
      };
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitle){
        tmdRecordingList.getGroupLabelName(2,i,2).waitForVisible();
        expect(tmdRecordingList.getGroupLabelName(2,i,2).isVisible()).to.eql(true);  
        break
      };
    };
    pass = 1
  });
  
  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79468",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79468",
        failObj
        )
      }
  })

  it.skip("Approve buttons on the approval level cards on the client side id disabled if the approval levels are not assigned to the user", ()=>{
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitleforwithoutaccess){
        tmdRecordingList.getApproveBtn(2,i,2).waitForVisible();
        expect(tmdRecordingList.getApproveBtn(2,i,2).isEnabled()).to.eql(true);  
        break
        };
      };
      browser.scroll(-0,-500);
      waitUntilTrueFalse(
        `.text-primary`,
        "5000",
        `Back link is not visible even after 5sec`,
        true
      );
      tmdRecordingList.backLink.waitForVisible();
      tmdRecordingList.backLink.click();
      browser.waitUntil(
        function() {
        return (
          tmdRecordingList.zeilderUmHeading.isVisible() === true
        )
      },
      30000,
      "Kiid listing page is not visible even after 30s"
      );
      waitUntilTrueFalse(
        `.navbar-nav > ul > a`,
        "20000",
        `avatar image is not visible even after 20sec`,
        true
      );
      Login.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79469",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79469",
        failObj
        )
      }
  })

  it.skip("Admin User can assign the same level priority User Approval Group to the same user", ()=>{
    Login.adminLogin();
    browser.url(testData.zeidler.userlink);
    expect(browser.getUrl()).to.eql(testData.zeidler.userlink);
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.pageheadings.users);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['Sindhu M','Enter']);
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.pageheadings.users
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    waitUntilTrueFalse(
      `table tbody tr:nth-child(1) td:nth-child(9) ul li:nth-child(1)`,
      "5000",
      `Edit icon is not visible even after 5sec`,
      true
    );
    Accounts.getEditIcon(1,9,1).waitForVisible();
    Accounts.getEditIcon(1,9,1).click();
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.isVisible() === true
      )
    },
    10000,
    "Users page is not visible even after 10s"
    );
    Users.accountGroupTab.waitForVisible();
    Users.accountGroupTab.click();
    browser.waitUntil(
    function() {
    return (
        Accounts.accountsHeading.isVisible() === true
    )
    },
    10000,
    "Users page is not visible even after 10s"
    );
    Users.addGroupToUser(testData.accounts.groupname);
    waitUntilTrueFalse(
      `.btn--primary.has-icon`,
      "5000",
      `Edit icon is not visible even after 5sec`,
      true
    );
    Users.addGroupToUser(testData.accounts.groupname1);
    tmdRecipant.logout();
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79470",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79470",
        failObj
        )
      }
  })

  it.skip("Client side of the user both Approval buttons are enabled when Admin assign the multiple Approval groups with same level approval to the same user", ()=>{
    Login.login(testData.zeidler.uatadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    tmdRecordingList.approvalComments();
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitle){
        tmdRecordingList.getApproveBtn(2,i,2).waitForVisible();
        expect(tmdRecordingList.getApproveBtn(2,i,2).isEnabled()).to.eql(true);  
        break
      };
    };
    browser.scroll(-0,-500);
    waitUntilTrueFalse(
      `.text-primary`,
      "5000",
      `Back link is not visible even after 5sec`,
      true
    );
    tmdRecordingList.backLink.waitForVisible();
    tmdRecordingList.backLink.click();
    browser.waitUntil(
      function() {
      return (
        tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "20000",
      `avatar image is not visible even after 20sec`,
      true
    );
    Login.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79472",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79472",
        failObj
        )
      }
  })

  it.skip("Admin can assign the same level priority User Approval Group to the different users and Approve buttons for both the users is enabled", ()=>{
    Accounts.accountGroupTab();
    browser.waitUntil(
    function() {
    return (
        Accounts.accountsHeading.isVisible() === true
    )
    },
    10000,
    "Users page is not visible even after 10s"
    );
    Accounts.getGroupsEyeIcon(1,3,2).waitForVisible();
    Accounts.getGroupsEyeIcon(1,3,2).click(); 
    browser.waitUntil(
    function() {
    return (
        Accounts.accountsHeading.getText() === testData.pageheadings.accountgroupuser
    )
    },
    10000,
    "Users page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.pageheadings.accountgroupuser);
    Accounts.addUserToGroup(testData.login.clientun);
    browser.pause(1000);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79473",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79473",
        failObj
        )
      }
  })

  it.skip("Account Group is linked to all the user when the Admin user add multiple users to the account group on the Account Group Users tab", ()=>{
    browser.url(testData.zeidler.userlink);
    expect(browser.getUrl()).to.eql(testData.zeidler.userlink);
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.pageheadings.users);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QAtest_tmd','Enter']);
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.pageheadings.users
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    waitUntilTrueFalse(
      `table tbody tr:nth-child(1) td:nth-child(9) ul li:nth-child(1)`,
      "5000",
      `Edit icon is not visible even after 5sec`,
      true
    );
    Accounts.getEditIcon(1,9,1).waitForVisible();
    Accounts.getEditIcon(1,9,1).click();
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.isVisible() === true
      )
    },
    10000,
    "Users page is not visible even after 10s"
    );
    Users.accountGroupTab.waitForVisible();
    Users.accountGroupTab.click();
    browser.waitUntil(
    function() {
    return (
        Accounts.accountsHeading.isVisible() === true
    )
    },
    10000,
    "Users page is not visible even after 10s"
    );
    Accounts.getApprovalTitle(1,1).waitForVisible();
    expect(Accounts.getApprovalTitle(1,1).getText()).to.eql(testData.accounts.groupname1);
    tmdRecipant.logout();
    Login.login(testData.zeidler.uatadminhome, testData.login.clientun, testData.login.clientpwd);
    tmdRecordingList.approvalComments();
    browser.scroll(-0,-500);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79687",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79687",
        failObj
        )
      }
  })

  it.skip("Bulk approval popup the Approve buttons of the account groups to which the user is having access are enabled when the CP has selected only one record which is in the Awaiting Client Approval", ()=>{
    waitUntilTrueFalse(
      `.text-primary`,
      "20000",
      `Cross icon is not visible even after 20sec`,
      true
    );
    tmdRecordingList.backLink.waitForVisible();
    tmdRecordingList.backLink.click();
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    tmdRecordingList.zeidlerQASubFundCheckbox.waitForVisible();
    tmdRecordingList.zeidlerQASubFundCheckbox.click();
    tmdRecordingList.approveBtn.waitForVisible();
    tmdRecordingList.approveBtn.click();
    browser.pause(1000);
    tmdRecordingList.bulkApprovalTitle.waitForVisible();
    expect(tmdRecordingList.bulkApprovalTitle.isVisible()).to.eql(true);
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getBulkApprovalLabel(i).getText() === testData.accounts.grouptitle1){
        tmdRecordingList.getBulkApproveEnabledBtn(i).waitForVisible();
        expect(tmdRecordingList.getBulkApproveEnabledBtn(i).isEnabled()).to.eql(true);  
        break
      };
    };
    tmdRecordingList.bulkApprovalCrossIcon.waitForVisible();
    tmdRecordingList.bulkApprovalCrossIcon.click();
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79860",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79860",
        failObj
        )
      }
  })

  it.skip("Bulk approval popup the Approve buttons of the account groups to which the user is having access are enabled when the cp has selected multiple record which are in the Awaiting Client Approval", ()=>{
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    tmdRecordingList.approveBtn.waitForVisible();
    tmdRecordingList.approveBtn.click();
    browser.pause(1000);
    tmdRecordingList.bulkApprovalTitle.waitForVisible();
    expect(tmdRecordingList.bulkApprovalTitle.isVisible()).to.eql(true);
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getBulkApprovalLabel(i).getText() === testData.accounts.grouptitle1){
        tmdRecordingList.getBulkApproveEnabledBtn(i).waitForVisible();
        expect(tmdRecordingList.getBulkApproveEnabledBtn(i).isEnabled()).to.eql(true); 
        break 
      };
    };
    tmdRecordingList.bulkApprovalCrossIcon.waitForVisible();
    tmdRecordingList.bulkApprovalCrossIcon.click();
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79861",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79861",
        failObj
        )
      }
  })

  it.skip("Bulk approval popup the Approve buttons of the account groups to which the user is not having access are disabled when the cp has selected only one record which is in the Awaiting Client Approval", ()=>{
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "TMD listing page is not visible even after 30s"
    );
    tmdRecordingList.approveBtn.waitForVisible();
    tmdRecordingList.approveBtn.click();
    browser.pause(1000);
    tmdRecordingList.bulkApprovalTitle.waitForVisible();
    expect(tmdRecordingList.bulkApprovalTitle.isVisible()).to.eql(true);
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getBulkApprovalLabel(i).getText() === testData.accounts.grouptitleforwithoutaccess){
        tmdRecordingList.getBulkApproveDisableBtn(i).waitForVisible();
        expect(tmdRecordingList.getBulkApproveDisableBtn(i).isEnabled()).to.eql(true); 
        break
      };
    };
    tmdRecordingList.bulkApprovalCrossIcon.waitForVisible();
    tmdRecordingList.bulkApprovalCrossIcon.click();
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79862",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79862",
        failObj
        )
      }
  })

  it.skip("Counter-party can approve the KIIDs records from Bulk Approve button", ()=>{
    browser.waitUntil(
      function() {
      return (
        tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    tmdRecordingList.approveBtn.waitForVisible();
    tmdRecordingList.approveBtn.click();
    browser.pause(1000);
    tmdRecordingList.bulkApprovalTitle.waitForVisible();
    expect(tmdRecordingList.bulkApprovalTitle.isVisible()).to.eql(true);
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getBulkApprovalLabel(i).getText() === testData.accounts.grouptitle1){
        tmdRecordingList.getBulkApproveEnabledBtn(i).waitForVisible();
        tmdRecordingList.getBulkApproveEnabledBtn(i).click();
        browser.alertAccept();
        tmdRecordingList.flashMessageForApproval.waitForVisible();
        expect(tmdRecordingList.flashMessageForApproval.getText()).to.eql(testData.kiidforapproval.flashmessageforapprovedstatus);
        break
      };
    };
    browser.refresh();
    waitUntilTrueFalse(
      `.page-action--filters .filter.dropdown:nth-child(2) .icon--x-small`,
      "5000",
      `Cross icon is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getStatusCrossIcon(2).waitForVisible();
    tmdRecordingList.getStatusCrossIcon(2).click();
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79863",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79863",
        failObj
        )
      }
  })

  it.skip("Bulk approval button is disabled when the client user selects a single TMDs record whose status is other than Awaiting client approval", ()=>{
    waitUntilTrueFalse(
      `.filters-container>div:nth-child(4) .btn span`,
      "10000",
      `Filters is not visible even after 10sec`,
      true
    );
    browser.pause(1000);
    tmdRecordingList.getFilterBtn(4).waitForVisible();
    tmdRecordingList.getFilterBtn(4).click();
    tmdRecordingList.getKiidStatusFilterOption(6).waitForVisible();
    tmdRecordingList.getKiidStatusFilterOption(6).click();
    waitUntilTrueFalse(
      `#kiid_list_page > div:nth-child(2) .table__cell--status.col`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getKiidRecordStatus(2).waitForVisible();
    expect(tmdRecordingList.getKiidRecordStatus(2).getText()).to.eql(testData.kiidfilteroption.approved);
    tmdRecordingList.zeidlerQASubFundCheckbox.waitForVisible();
    tmdRecordingList.zeidlerQASubFundCheckbox.click();
    tmdRecordingList.approveBtn.waitForVisible();
    expect(tmdRecordingList.approveBtn.isEnabled()).to.eql(true);
    waitUntilTrueFalse(
      `.page-action--filters .filter.dropdown:nth-child(4) .icon--x-small`,
      "5000",
      `Cross icon is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getStatusCrossIcon(4).waitForVisible();
    tmdRecordingList.getStatusCrossIcon(4).click();
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79882",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79882",
        failObj
        )
      }
  })

  it.skip("Bulk approval button is disabled when the client user selects multiple TMD records Awaiting client approval and others are having different status", ()=>{
    tmdRecordingList.zeidlerQASubFundCheckbox.waitForVisible();
    tmdRecordingList.zeidlerQASubFundCheckbox.click();
    tmdRecordingList.zeidlerQASubFundCheckboxs.waitForVisible();
    tmdRecordingList.zeidlerQASubFundCheckboxs.click();
    tmdRecordingList.approveBtn.waitForVisible();
    expect(tmdRecordingList.approveBtn.isEnabled()).to.eql(true);
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "5000",
      `Avatar image is not visible even after 5sec`,
      true
    );
    Login.logout();
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79883",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79883",
        failObj
        )
      }
  })

  it.skip("Admin user can delete the TMD Approval Level of the Fund if no users have Approved the TMD record", ()=>{
    Login.adminLogin();
    browser.url(testData.zeidler.fundpagelink);
    expect(browser.getUrl()).to.eql(testData.zeidler.fundpagelink);
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.pageheadings.fund);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QA Calibre Investment Management','Enter']);
    browser.waitUntil(
      function() {
      return (
        Accounts.fundName.getText() === testData.kiidfilteroption.fundname
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    waitUntilTrueFalse(
      `table tbody tr:nth-child(1) td:nth-child(8) ul li:nth-child(1)`,
      "5000",
      `Edit icon is not visible even after 5sec`,
      true
    );
    Accounts.getEditIcon(1,8,1).waitForVisible();
    Accounts.getEditIcon(1,8,1).click();
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.kiidfilteroption.fundname
      )
    },
    10000,
    "Accounts page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.kiidfilteroption.fundname);
    Accounts.kiidApprovalTab.waitForVisible();
    Accounts.kiidApprovalTab.click();
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.kiidfilteroption.fundname
      )
    },
    10000,
    "Accounts page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.kiidfilteroption.fundname);
    waitUntilTrueFalse(
      `#search`,
      "10000",
      `Search field is not visible even after 10sec`,
      true
    );
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['TMD Approval automation','Enter']);
    browser.waitUntil(
      function() {
      return (
        Accounts.getApprovalTitle(1,3).getText() === testData.accounts.grouptitle
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    waitUntilTrueFalse(
      `table tbody tr:nth-child(1) td:nth-child(6) ul li:nth-child(2)`,
      "5000",
      `Edit icon is not visible even after 5sec`,
      true
    );
    Accounts.getEditIcon(1,6,2).waitForVisible();
    Accounts.getEditIcon(1,6,2).click();
    browser.alertAccept();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79474",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79474",
        failObj
        )
      }
  })

  it.skip("Admin is able to navigate to TMD document_comments tab", ()=>{
    Login.adminLogin();
    browser.url(testData.zeidler.tmdapprovallink);
    expect(browser.getUrl()).to.eql(testData.zeidler.tmdapprovallink);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QCE030000007AU','Enter']);
    tmdAdmin.kiidRecordCheckbox.waitForVisible();
    tmdAdmin.kiidRecordCheckbox.click();
    tmdAdmin.getEyeIcon(1,10,4).waitForVisible();
    tmdAdmin.getEyeIcon(1,10,4).click();
    waitUntilTrueFalse(
      `//a[contains(text(),'Generated TMDs')]`,
      "10000",
      `Generated tab icon is not visible even after 10sec`,
      true
    );
    tmdAdmin.generatedKidsTab.waitForVisible();
    tmdAdmin.generatedKidsTab.click();
    tmdAdmin.getEyeIcon(1,6,2).waitForVisible();
    tmdAdmin.getEyeIcon(1,6,2).click();
    browser.waitUntil(
      function() {
      return (
        tmdAdmin.kiidDocumentTab.isVisible() === true
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    expect(tmdAdmin.kiidDocumentTab.isVisible()).to.eql(true);
    tmdRecipant.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "77649",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "77649",
        failObj
        )
      }
  })

  it.skip("Admin is not able to add comments for the previous records in the admin panel", ()=>{
    waitUntilTrueFalse(
      `//a[contains(text(),'Generated TMDs')]`,
      "10000",
      `Generated tab icon is not visible even after 10sec`,
      true
    );
    tmdAdmin.generatedKidsTab.waitForVisible();
    tmdAdmin.generatedKidsTab.click();
    tmdAdmin.getEyeIcon(2,6,2).waitForVisible();
    tmdAdmin.getEyeIcon(2,6,2).click();
    browser.waitUntil(
      function() {
      return (
        tmdAdmin.kiidDocumentTab.isVisible() === true
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    expect(tmdAdmin.kiidDocumentTab.isVisible()).to.eql(true);
    expect(tmdAdmin.getAddCommentBtn(1).isVisible()).to.eql(false);
    tmdRecipant.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79475",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79475",
        failObj
        )
      }
  })
});

describe.skip("Approval process for parallel and sequential", ()=>{
  it("User is able to add the comments parallel when the groups levels are same", ()=>{
    Login.adminLoginWithCookies();
    browser.url(testData.zeidler.fundpagelink);
    expect(browser.getUrl()).to.eql(testData.zeidler.fundpagelink);
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.pageheadings.fund);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QA Calibre Investment Management','Enter']);
    browser.waitUntil(
      function() {
      return (
        Accounts.fundName.getText() === testData.kiidfilteroption.fundname
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    waitUntilTrueFalse(
      `table tbody tr:nth-child(1) td:nth-child(8) ul li:nth-child(1)`,
      "5000",
      `Edit icon is not visible even after 5sec`,
      true
    );
    Accounts.getEditIcon(1,8,1).waitForVisible();
    Accounts.getEditIcon(1,8,1).click();
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.kiidfilteroption.fundname
      )
    },
    10000,
    "Accounts page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.kiidfilteroption.fundname);
    Accounts.kiidApprovalTab.waitForVisible();
    Accounts.kiidApprovalTab.click();
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.kiidfilteroption.fundname
      )
    },
    10000,
    "Accounts page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    Accounts.createApproval(testData.accounts.groupname,testData.accounts.grouplevel,testData.accounts.grouptitlelevel1);
    tmdRecipant.logout();
    Login.login(testData.zeidler.uatadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    tmdRecordingList.approvalCommentForApproval();
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitlelevel1){
        tmdRecordingList.getApproveBtn(2,i,1).waitForVisible();
        tmdRecordingList.getApproveBtn(2,i,1).click(); 
        browser.pause(1000);
        tmdRecordingList.noteField.waitForVisible();
        tmdRecordingList.noteField.setValue(testData.comments.approvecomment);
        tmdRecordingList.saveNoteBtn.waitForVisible();
        tmdRecordingList.saveNoteBtn.click();
        break
      };
    };
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitle){
        browser.pause(2000);
        tmdRecordingList.getApproveBtn(2,i,1).waitForVisible();
        tmdRecordingList.getApproveBtn(2,i,1).click();
        browser.pause(1000);
        tmdRecordingList.noteField.waitForVisible();
        tmdRecordingList.noteField.setValue(testData.comments.approvalcomments);
        tmdRecordingList.saveNoteBtn.waitForVisible();
        tmdRecordingList.saveNoteBtn.click();
        break
      };
    };
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79897",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79897",
        failObj
        )
      }
  })

  it("User can able to approved the levels parallel when the groups levels are same(1)", ()=>{
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitlelevel1){
        browser.pause(1000);
        tmdRecordingList.getApproveBtn(2,i,2).waitForVisible();
        tmdRecordingList.getApproveBtn(2,i,2).click();
        tmdRecordingList.approvalConfirmBtn.waitForVisible();
        tmdRecordingList.approvalConfirmBtn.click();
        tmdRecordingList.noteField.waitForVisible();
        tmdRecordingList.noteField.setValue(testData.comments.approvecomment);
        tmdRecordingList.approvalConfirmBtn.waitForVisible();
        tmdRecordingList.approvalConfirmBtn.click();
        tmdRecordingList.flashMessageForApproval.waitForVisible();
        expect(tmdRecordingList.flashMessageForApproval.getText()).to.eql(testData.kiidforapproval.approvedmessage);
        break
      };
    };
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitle){
        browser.pause(1000);
        tmdRecordingList.getApproveBtn(2,i,2).waitForVisible();
        tmdRecordingList.getApproveBtn(2,i,2).click();
        tmdRecordingList.approvalConfirmBtn.waitForVisible();
        tmdRecordingList.approvalConfirmBtn.click();
        tmdRecordingList.noteField.waitForVisible();
        tmdRecordingList.noteField.setValue(testData.comments.approvalcomments);
        tmdRecordingList.approvalConfirmBtn.waitForVisible();
        tmdRecordingList.approvalConfirmBtn.click();
        tmdRecordingList.flashMessageForApproval.waitForVisible();
        expect(tmdRecordingList.flashMessageForApproval.getText()).to.eql(testData.kiidforapproval.approvedmessage);
        break
      };
    };
    browser.scroll(-0,-500);
    waitUntilTrueFalse(
      `.text-primary`,
      "5000",
      `Back link is not visible even after 5sec`,
      true
    );
    tmdRecordingList.backLink.waitForVisible();
    tmdRecordingList.backLink.click();
    browser.waitUntil(
      function() {
      return (
        tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "20000",
      `avatar image is not visible even after 20sec`,
      true
    );
    Login.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79896",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79896",
        failObj
        )
      }
  })

  it("User can't be able to add comments for level 2 until the user approves the level 1 (Sequential)", ()=>{
    Login.adminLoginWithCookies();
    browser.url(testData.zeidler.fundpagelink);
    expect(browser.getUrl()).to.eql(testData.zeidler.fundpagelink);
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.pageheadings.fund);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QA Calibre Investment Management','Enter']);
    browser.waitUntil(
      function() {
      return (
        Accounts.fundName.getText() === testData.kiidfilteroption.fundname
      )
    },
    30000,
    "Accounts page is not visible even after 30s"
    );
    waitUntilTrueFalse(
      `table tbody tr:nth-child(1) td:nth-child(8) ul li:nth-child(1)`,
      "5000",
      `Edit icon is not visible even after 5sec`,
      true
    );
    Accounts.getEditIcon(1,8,1).waitForVisible();
    Accounts.getEditIcon(1,8,1).click();
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.kiidfilteroption.fundname
      )
    },
    10000,
    "Accounts page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    expect(Accounts.accountsHeading.getText()).to.eql(testData.kiidfilteroption.fundname);
    Accounts.kiidApprovalTab.waitForVisible();
    Accounts.kiidApprovalTab.click();
    browser.waitUntil(
      function() {
      return (
        Accounts.accountsHeading.getText() === testData.kiidfilteroption.fundname
      )
    },
    10000,
    "Accounts page is not visible even after 10s"
    );
    Accounts.accountsHeading.waitForVisible();
    Accounts.createApproval(testData.accounts.groupname,testData.accounts.grouplevel,testData.accounts.grouptitleforsequ);
    Accounts.createApproval(testData.accounts.groupname,testData.accounts.grouplevel2,testData.accounts.grouptitleforsequ2);
    tmdRecipant.logout();
    Login.login(testData.zeidler.uatadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
    function() {
    return (
    $(".btn__content.choices__item.choices__item--selectable").isVisible() === true
    )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    waitUntilTrueFalse(
    `#kiid_list_page > div:nth-child(2) .table__cell--status.col`,
    "5000",
    `Filters is not visible even after 5sec`,
    true
    );
    tmdRecordingList.kiidRecordIsin.waitForVisible();
    expect(tmdRecordingList.kiidRecordIsin.getText()).to.eql(testData.tmdapprovaltab.isinname);
    tmdRecordingList.getCreateNoteIcon(2).waitForVisible();
    tmdRecordingList.getCreateNoteIcon(2).click();
    tmdRecordingList.approvalPageHeading.waitForVisible();
    browser.waitUntil(
    function() {
    return (
        $(`h1.h5.page-header__title`).isVisible() === true
    )
    },
    30000,
    "Approval page is not visible even after 30s"
    );
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitleforsequ){
        tmdRecordingList.getApproveBtn(2,i,1).waitForVisible();
        tmdRecordingList.getApproveBtn(2,i,1).click(); 
        browser.pause(1000);
        tmdRecordingList.noteField.waitForVisible();
        tmdRecordingList.noteField.setValue(testData.comments.approvecomment);
        tmdRecordingList.saveNoteBtn.waitForVisible();
        tmdRecordingList.saveNoteBtn.click();
        break
      };
    };
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitleforsequ2){
        browser.pause(1000);
        tmdRecordingList.getApproveBtn(2,i,1).waitForVisible();
        tmdRecordingList.getApproveBtn(2,i,1).click(); 
        browser.pause(1000);
        tmdRecordingList.noteField.waitForVisible();
        tmdRecordingList.noteField.setValue(testData.comments.approvalcomments);
        tmdRecordingList.saveNoteBtn.waitForVisible();
        tmdRecordingList.saveNoteBtn.click();
        break
      };
    };
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79895",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79895",
        failObj
        )
      }
  })

  it("User is not able to approve the level 2 until level 1 is approved(Sequential)", ()=>{
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitleforsequ){
        browser.pause(1000);
        tmdRecordingList.getApproveBtn(2,i,2).waitForVisible();
        tmdRecordingList.getApproveBtn(2,i,2).click();
        tmdRecordingList.approvalConfirmBtn.waitForVisible();
        tmdRecordingList.approvalConfirmBtn.click();
        tmdRecordingList.noteField.waitForVisible();
        tmdRecordingList.noteField.setValue(testData.comments.approvalcomments);
        tmdRecordingList.approvalConfirmBtn.waitForVisible();
        tmdRecordingList.approvalConfirmBtn.click();
        tmdRecordingList.flashMessageForApproval.waitForVisible();
        expect(tmdRecordingList.flashMessageForApproval.getText()).to.eql(testData.kiidforapproval.approvedmessage);
        break
      };
    };
    for(i = 1; i<=10; i++){
      if(tmdRecordingList.getGroupLabelName(2,i,2).getText() === testData.accounts.grouptitleforsequ2){
        browser.pause(1000);
        tmdRecordingList.getApproveBtn(2,i,2).waitForVisible();
        tmdRecordingList.getApproveBtn(2,i,2).click();
        tmdRecordingList.approvalConfirmBtn.waitForVisible();
        tmdRecordingList.approvalConfirmBtn.click();
        tmdRecordingList.noteField.waitForVisible();
        tmdRecordingList.noteField.setValue(testData.comments.approvalcomments);
        tmdRecordingList.approvalConfirmBtn.waitForVisible();
        tmdRecordingList.approvalConfirmBtn.click();
        tmdRecordingList.flashMessageForApproval.waitForVisible();
        expect(tmdRecordingList.flashMessageForApproval.getText()).to.eql(testData.kiidforapproval.approvedmessage);
        break
      };
    };
    browser.scroll(-0,-500);
    waitUntilTrueFalse(
      `.text-primary`,
      "5000",
      `Back link is not visible even after 5sec`,
      true
    );
    tmdRecordingList.backLink.waitForVisible();
    tmdRecordingList.backLink.click();
    browser.waitUntil(
      function() {
      return (
        tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "20000",
      `avatar image is not visible even after 20sec`,
      true
    );
    Login.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79894",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79894",
        failObj
        )
      }
  })
});

describe.skip("Recipient", ()=>{
  it("Following fields are displayed in the All Recipients Page-Recipients Name-Contact Name-Delivery Method-Recipients Email", ()=>{
    Login.adminLogin();
    Login.launchUrl();
    tmdRecipant.addRecipantBtn.waitForVisible();
    tmdRecipant.addRecipantBtn.click();
    tmdRecipant.recipientNameLabel.waitForVisible();
    expect(tmdRecipant.recipientNameLabel.getText()).to.eql(testData.recipient.recipientnamelabel);
    tmdRecipant.recipientNameLabel.waitForVisible();
    expect(tmdRecipant.recipientContactNameLabel.getText()).to.eql(testData.recipient.recipientcontactnamelabel);
    tmdRecipant.getRecipantKiidCheckbox(5).waitForVisible();
    tmdRecipant.getRecipantKiidCheckbox(5).click();
    browser.scroll(0,500);
    tmdRecipant.emailContentLabel.waitForVisible();
    browser.pause(1000);
    expect(tmdRecipant.emailContentLabel.getText()).to.eql(testData.recipient.emailcontentlabel);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79898",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79898",
        failObj
        )
      }
  })

  it("Email,API,FTP are displayed in the Delivery method dropdown in the Add Recipient page", ()=>{
    tmdRecipant.addRecipient();
    browser.pause(1000);
    tmdRecipant.getRecipantKiidCheckbox(5).waitForVisible();
    tmdRecipant.getRecipantKiidCheckbox(5).click();
    browser.scroll(0,500);
    tmdRecipant.deliveryMethodField.waitForVisible();
    tmdRecipant.deliveryMethodField.click();
    tmdRecipant.getDeliveryMethodFtp(1).waitForVisible();
    expect(tmdRecipant.getDeliveryMethodFtp(1).isVisible()).to.eql(true);
    tmdRecipant.getDeliveryMethodFtp(2).waitForVisible();
    expect(tmdRecipant.getDeliveryMethodFtp(2).isVisible()).to.eql(true);
    tmdRecipant.getDeliveryMethodFtp(3).waitForVisible();
    expect(tmdRecipant.getDeliveryMethodFtp(3).isVisible()).to.eql(true);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79903",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79903",
        failObj
        )
      }
  })

  it("Admin can create the Email recipient for the TMD Dissemination", ()=>{
    tmdRecipant.allRecipientsLink.waitForVisible();
    tmdRecipant.allRecipientsLink.click();
    tmdRecipant.createRecipient(testData.recipient.recipientname, testData.recipient.contactname, testData.recipient.recipientcontactemail, testData.recipient.recipientphonenumber,testData.recipient.recipientname);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79899",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79899",
        failObj
        )
      }
  })

  it("Admin can create the FTP recipient for the TMD Dissemination", ()=>{
    tmdRecipant.ftpRecipientForDissemination(testData.recipient.ftprecipient,2,1,testData.recipient.hostdetails,testData.recipient.username,testData.recipient.sftppassword,testData.recipient.rootdirectory,testData.recipient.ftprecipient);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79900",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79900",
        failObj
        )
      }
  })

  it("Admin can create the SFTP recipient for the TMD Dissemination", ()=>{
    tmdRecipant.sftpRecipientForDissemination(testData.recipient.sftprecipient1,2,2,testData.recipient.hostdetails,testData.recipient.username,testData.recipient.rootdirectory,testData.recipient.sftprecipient1);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79901",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79901",
        failObj
        )
      }
  })

  it("Error message is displayed when the fund_name is removed from the Subject Line field in the Add Recipient page", ()=>{
    tmdRecipant.recipientCreationWithUncheckClient(testData.recipient.emailrecipient);
    tmdRecipant.emailAddressField.waitForVisible();
    tmdRecipant.emailAddressField.click();
    browser.setValue(".email_content_5 .choices__input--cloned",['jithendra@calibrecode.com','Enter']);
    tmdRecipant.subjectlineField.waitForVisible();
    tmdRecipant.subjectlineField.clearElement();
    tmdRecipant.saveBtn.waitForVisible();
    tmdRecipant.saveBtn.click();
    browser.scroll(0,500);
    tmdRecipant.sameRecipientErrorMessage.waitForVisible();
    expect(tmdRecipant.sameRecipientErrorMessage.getText()).to.eql(testData.recipient.recipienterrormessagelabel);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79967",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79967",
        failObj
        )
      }
  })

  it("Hostname,Username,Password,Root Directory,Chargeable are displayed when the FTP is selected in the delivery method", ()=>{
    tmdRecipant.addRecipient();
    tmdRecipant.getRecipantKiidCheckbox(5).waitForVisible();
    tmdRecipant.getRecipantKiidCheckbox(5).click();
    tmdRecipant.getDeliveryMethodFtp(2).waitForVisible();
    tmdRecipant.getDeliveryMethodFtp(2).click();
    browser.scroll(0,1000);
    browser.pause(1000);
    expect(tmdRecipant.hostDetailsLabel.getText()).to.eql(testData.recipient.hostdetailslabel);
    expect(tmdRecipant.userNameLabel.getText()).to.eql(testData.recipient.usernamelabel);
    expect(tmdRecipant.passwordLabel.getText()).to.eql(testData.recipient.passwordlabel);
    expect(tmdRecipant.rootDirectoryLabel.getText()).to.eql(testData.recipient.rootdirectorylabel);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79968",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79968",
        failObj
        )
      }
    })

  it("Endpoint,API Key,API Type,Username,Password,Chargeable are present when the API is selected in the delivery method", () =>{
    tmdRecipant.getDeliveryMethodFtp(3).waitForVisible();
    tmdRecipant.getDeliveryMethodFtp(3).click();
    browser.scroll(0,500);
    browser.pause(1000);
    expect(tmdRecipant.apiEndPointLabel.getText()).to.eql(testData.recipient.apiendpointlabel);
    expect(tmdRecipant.apiKeyLabel.getText()).to.eql(testData.recipient.apikeylabel);
    expect(tmdRecipant.apiTypeLabel.getText()).to.eql(testData.recipient.apitypelabel);
    expect(tmdRecipant.apiUserNameLabel.getText()).to.eql(testData.recipient.apiusernamelabel);
    expect(tmdRecipant.apiPasswordLabel.getText()).to.eql(testData.recipient.apipasswordlabel);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79969",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79969",
        failObj
        )
      }
    })

  it("The changes are made while editing a recipient are updated successfully", ()=>{
    tmdRecipant.editRecipient(testData.recipient.recipientname,testData.recipient.recipientname,testData.recipient.recipienteditname, testData.recipient.editcontactname, testData.recipient.editcontactemail, testData.recipient.editcontactphone);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79971",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79971",
        failObj
        )
      }
    })
  
    it("default email subject is displayed as New TMD for{fund_name} in the add recipient page", () =>{
      tmdRecipant.addRecipantBtn.waitForVisible();
      tmdRecipant.addRecipantBtn.click();
      tmdRecipant.getRecipantKiidCheckbox(5).waitForVisible();
      tmdRecipant.getRecipantKiidCheckbox(5).click();
      browser.scroll(0,500);
      tmdRecipant.subjectlineField.waitForVisible();
      tmdRecipant.subjectlineField.click();
      tmdRecipant.subjectlineField.waitForVisible();
      expect(tmdRecipant.subjectlineField.getValue()).to.eql(testData.recipient.recipientsubjectline);
      pass = 1
    });
  
    it("Add testrail result", async()=>{
      if(pass === 1){
          var successMessage = await addTestRailResult(
          testData.testrail.runid,
          "79972",
          passObj
        )
        pass = 0
      }
      else{
        successMessage = await addTestRailResult(
          testData.testrail.runid,
          "79972",
          failObj
          )
        }
      })
  
    it("Deleted recipients should be removed from the All recipients table", () =>{
      tmdRecipant.allRecipientsLink.waitForVisible();
      tmdRecipant.allRecipientsLink.click();
      tmdRecipant.deleteRecipient(testData.recipient.sftprecipient1);
      pass = 1
    });
  
    it("Add testrail result", async()=>{
      if(pass === 1){
          var successMessage = await addTestRailResult(
          testData.testrail.runid,
          "79970",
          passObj
        )
        pass = 0
      }
      else{
        successMessage = await addTestRailResult(
          testData.testrail.runid,
          "79970",
          failObj
          )
        }
    })

  it("Single Client is added successfully when the Add Client button in the Admin portal is clicked", ()=>{
    waitUntilTrueFalse(
      `#search`,
      "10000",
      `Rule card is not visible even after 10sec`,
      true
    );
    tmdRecipant.searchField.waitForVisible();
    browser.setValue("#search",[testData.recipient.ftprecipient,'Enter']);
    tmdRecipant.getRecipientName(1,1).waitForVisible();
    expect(tmdRecipant.getRecipientName(1,1).getText()).to.eql(testData.recipient.ftprecipient);
    waitUntilTrueFalse(
      `table tbody tr:nth-child(1) td:nth-child(4) ul li:nth-child(1) a`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    tmdRecipant.editRecipientPage.waitForVisible();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.editrecipientpage);
    tmdRecipant.clientMappingBtn.waitForVisible();
    tmdRecipant.clientMappingBtn.click();
    tmdRecipant.editRecipientPage.waitForVisible();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.clientheadingpage1);
    tmdRecipant.addClientBtn.waitForVisible();
    tmdRecipant.addClientBtn.click();
    tmdRecipant.clientField.waitForVisible();
    browser.setValue(".choices__input.choices__input--cloned",['QA Calibre Investment Management','Enter']);
    tmdRecipant.addClientPopupHeading.waitForVisible();
    tmdRecipant.addClientPopupHeading.click();
    tmdRecipant.clientSaveBtn.waitForVisible();
    tmdRecipant.clientSaveBtn.click();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79973",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79973",
        failObj
        )
      }
  })

  it("All the clients are mapped when user selects the Map all Clients?, checkbox while creating the TMD Recipient", ()=>{
    tmdRecipant.allRecipientsLink.waitForVisible();
    tmdRecipant.allRecipientsLink.click();
    waitUntilTrueFalse(
      `#search`,
      "10000",
      `Rule card is not visible even after 10sec`,
      true
    );
    tmdRecipant.searchField.waitForVisible();
    browser.setValue("#search",[testData.recipient.recipienteditname,'Enter']);
    tmdRecipant.getRecipientName(1,1).waitForVisible();
    expect(tmdRecipant.getRecipientName(1,1).getText()).to.eql(testData.recipient.recipienteditname);
    waitUntilTrueFalse(
      `table tbody tr:nth-child(1) td:nth-child(4) ul li:nth-child(1) a`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    tmdRecipant.clientMappingBtn.waitForVisible();
    tmdRecipant.clientMappingBtn.click();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.clientheadingpage);
    tmdRecipant.qaAmAccountLabel.waitForVisible();
    expect(tmdRecipant.qaAmAccountLabel.isVisible()).to.eql(true);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79976",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79976",
        failObj
        )
      }
  })

  it("Multiple client should be added when add client button is clicked", () =>{
    tmdRecipant.clientNameCheckbox.waitForVisible();
    tmdRecipant.clientNameCheckbox.click();
    tmdRecipant.bulkActionDropdown.waitForVisible();
    tmdRecipant.bulkActionDropdown.click();
    tmdRecipant.deleteOption.waitForVisible();
    tmdRecipant.deleteOption.click();
    tmdRecipant.applyToBtn.waitForVisible();
    tmdRecipant.applyToBtn.click();
    tmdRecipant.addClientBtn.waitForVisible();
    tmdRecipant.addClientBtn.click();
    tmdRecipant.clientField.waitForVisible();
    browser.setValue(".choices__input.choices__input--cloned",['QA Calibre Investment Management','Enter']);
    browser.setValue(".choices__input.choices__input--cloned",['TEST 123', 'Enter']);
    browser.setValue(".choices__input.choices__input--cloned",['z.CalibreCode', 'Enter']);
    tmdRecipant.addClientPopupHeading.waitForVisible();
    tmdRecipant.addClientPopupHeading.click();
    tmdRecipant.clientSaveBtn.waitForVisible();
    tmdRecipant.clientSaveBtn.click();
    browser.pause(1000);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79974",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79974",
        failObj
        )
      }
    })

  it("Client should be removed from the Client mapping page when delete icon is clicked", () =>{
    tmdRecipant.deleteClient(testData.recipient.clientname);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79975",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79975",
        failObj
        )
      }
    })

  it("Clients should be removed from client mapping page when clicked on delete bulk action", () =>{
    tmdRecipant.getClientCheckbox(1,1).waitForVisible();
    tmdRecipant.getClientCheckbox(1,1).click();
    tmdRecipant.bulkActionDropdown.waitForVisible();
    tmdRecipant.bulkActionDropdown.click();
    tmdRecipant.deleteOption.waitForVisible();
    tmdRecipant.deleteOption.click();
    tmdRecipant.applyToBtn.waitForVisible();
    tmdRecipant.applyToBtn.click();
    browser.pause(1000);
    tmdRecipant.searchField.waitForVisible();
    browser.setValue("#search",['TEST 123','Enter']);
    expect(tmdRecipant.noClientsAddedHeading.getText()).to.eql(testData.recipient.clientdeleteheading);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79977",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79977",
        failObj
        )
      }
  })

  it("New recipient with same name should not be saved successfully", () => {
    tmdRecipant.allRecipientsLink.waitForVisible();
    tmdRecipant.allRecipientsLink.click();
    tmdRecipant.createSameRecipient(testData.recipient.recipienteditname);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79981",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79981",
        failObj
        )
      }
  })

  it("Admin can create the FTP Recipient without giving the Root Directory path", ()=>{
    tmdRecipant.allRecipientsLink.waitForVisible();
    tmdRecipant.allRecipientsLink.click();
    tmdRecipant.ftpRecipientWithoutRootDirec(testData.recipient.ftprecipientwithoutrd,2,1,testData.recipient.hostdetails,testData.recipient.username,testData.recipient.sftppassword,testData.recipient.ftprecipientwithoutrd);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79978",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79978",
        failObj
        )
      }
  })

  it("Admin is able to create the chargeable recipients", ()=>{
    tmdRecipant.addRecipantBtn.waitForVisible();
    tmdRecipant.addRecipantBtn.click();
    tmdRecipant.recipantNameField.waitForVisible();
    tmdRecipant.recipantNameField.setValue(testData.recipient.recipientname);
    tmdRecipant.mapAllClientsCheckbox.waitForVisible();
    tmdRecipant.mapAllClientsCheckbox.click();
    tmdRecipant.getRecipantKiidCheckbox(5).waitForVisible();
    tmdRecipant.getRecipantKiidCheckbox(5).click();
    browser.scroll(0,500);
    browser.pause(1000);
    tmdRecipant.emailAddressField.waitForVisible();
    tmdRecipant.emailAddressField.click();
    browser.setValue(".email_content_5 .choices__input--cloned",['jithendra@calibrecode.com','Enter']);
    tmdRecipant.chargeableCheckbox.waitForVisible();
    tmdRecipant.chargeableCheckbox.click();
    tmdRecipant.saveBtn.waitForVisible();
    tmdRecipant.saveBtn.click();
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    tmdRecipant.editRecipientPage.waitForVisible();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.editrecipientpage);
    tmdRecipant.clientMappingBtn.waitForVisible();
    tmdRecipant.clientMappingBtn.click();
    tmdRecipant.editRecipientPage.waitForVisible();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.clientheadingpage2);
    tmdRecipant.addClientBtn.waitForVisible();
    tmdRecipant.addClientBtn.click();
    tmdRecipant.clientField.waitForVisible();
    browser.setValue(".choices__input.choices__input--cloned",['QA Calibre Investment Management','Enter']);
    tmdRecipant.addClientPopupHeading.waitForVisible();
    tmdRecipant.addClientPopupHeading.click();
    tmdRecipant.clientSaveBtn.waitForVisible();
    tmdRecipant.clientSaveBtn.click();
    tmdRecipant.logout();
    pass = 1
  });
});

describe.skip("Rules", ()=>{
  it("Sort by Dropdown is displayed in the Dissemination List Page", ()=>{
    Login.login(testData.zeidler.uatadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", ['QA Calibre Investment Management','Enter']);
    waitUntilTrueFalse(
      `.page-header .col-sm-12.col-lg-auto .nav-item:nth-child(3)`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getDisseminationLists(2).waitForVisible();
    tmdRecordingList.getDisseminationLists(2).click();
    expect(browser.getUrl()).to.eql(testData.zeidler.qadisseminationlistpage);
    disseminationLists.sortByDropdown.waitForVisible();
    expect(disseminationLists.sortByDropdown.isVisible()).to.eql(true);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79992",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79992",
        failObj
        )
      }
  })

  it("The following options are displayed when clicked on the Sort by Dropdown--Ascending (A-Z)--Descending (Z-A)", ()=>{
    disseminationLists.sortByDropdown.waitForVisible();
    disseminationLists.sortByDropdown.click();
    disseminationLists.ascendingOption.waitForVisible();
    expect(disseminationLists.ascendingOption.getText()).to.eql(testData.disseminationlist.ascending);
    disseminationLists.descendingOption.waitForVisible();
    expect(disseminationLists.descendingOption.getText()).to.eql(testData.disseminationlist.descending);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80009",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80009",
        failObj
        )
      }
  })

  it("All TMDs of that particular fund can be selected when the 'All TMDs' checkbox is enable/ticked in the Create a list Rule Pop-up", ()=>{
    disseminationLists.ruleCreationByClickingOnRuleBtn(testData.disseminationlist.rulelabel);
    disseminationLists.ruleAllKiidCheckbox.waitForVisible();
    disseminationLists.ruleAllKiidCheckbox.click();
    disseminationLists.ruleAllKiidCheckbox.waitForVisible();
    expect(disseminationLists.ruleAllKiidCheckbox.isEnabled()).to.eql(true);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79994",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79994",
        failObj
        )
      }
  })

  it("'Create a list Rule' is closed when clicked on the Cancel button", ()=>{
    disseminationLists.cancelBtn.waitForVisible();
    disseminationLists.cancelBtn.click();
    browser.pause(1000);
    expect(disseminationLists.ruleWindowHeading.isVisible()).to.eql(false);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79995",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79995",
        failObj
        )
      }
  })

  it("The following filters are displayed in the 'Create a list Rule' Fund name, Product APIR code and Share class name", ()=>{
    browser.refresh();
    disseminationLists.ruleCreationByClickingOnRuleBtn(testData.disseminationlist.rulelabel);
    disseminationLists.kiidSubFundFilterDropdown.waitForVisible();
    disseminationLists.kiidSubFundFilterDropdown.click();
    disseminationLists.codeFilterDropdown.waitForVisible();
    expect(disseminationLists.codeFilterDropdown.getText()).to.eql(testData.disseminationlist.filter);
    disseminationLists.nameFilterDropdown.waitForVisible();
    expect(disseminationLists.nameFilterDropdown.getText()).to.eql(testData.disseminationlist.shareclassfilter);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79996",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79996",
        failObj
        )
      }
  })

  it("User should be able create a rule by adding one Fund name filter in the 'Create a list rule' pop-up", ()=>{
    disseminationLists.selectKiidsFilterDropdown.waitForVisible();
    disseminationLists.selectKiidsFilterDropdown.click();
    browser.pause(1000);
    disseminationLists.filterSearchField.waitForVisible();
    disseminationLists.filterSearchField.setValue(testData.kiidfilteroption.fundname);
    disseminationLists.getSelectCurrencyFilterOption(1).waitForVisible();
    disseminationLists.getSelectCurrencyFilterOption(1).click();
    disseminationLists.selectLink.waitForVisible();
    disseminationLists.selectLink.click();
    disseminationLists.recipientSearchField.waitForVisible();
    browser.setValue(".choices__input--cloned",[testData.recipient.yopemail,'Enter']);
    browser.pause(1000);
    disseminationLists.saveChangesBtn.waitForVisible();
    disseminationLists.saveChangesBtn.click();
    waitUntilTrueFalse(
      `#dissemination_rule_list_search`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    browser.pause(1000);
    disseminationLists.searchField.waitForVisible();
    disseminationLists.searchField.setValue(testData.disseminationlist.rulelabel);
    disseminationLists.getRuleNameLabel(1).waitForVisible();
    browser.waitUntil(
      function() {
      return (
        disseminationLists.getRuleNameLabel(1).getText() === testData.disseminationlist.rulelabel
      )
    },
    30000,
    "Rule card is not visible even after 30s"
    );
    expect(disseminationLists.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulelabel);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79998",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79998",
        failObj
        )
      }
  })

  it("User should be able to create a rule by enabling the All TMDS checkbox in the 'Create a list rule' pop-up", ()=>{
    browser.refresh();
    disseminationLists.createRule(testData.disseminationlist.rulenameforalltmd, testData.recipient.recipienteditname, testData.recipient.ftprecipient,testData.disseminationlist.rulenameforalltmd);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80020",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80020",
        failObj
        )
      }
  })

  it("Rule is not created when the value for the selected filter is not added/entered in the 'Create a list rule' pop-up", ()=>{
    browser.refresh();
    waitUntilTrueFalse(
      `.btn-primary.create-new-rule`,
      "10000",
      `Rule button is not visible even after 10sec`,
      true
    );
    disseminationLists.createNewRuleBtn.waitForVisible();
    disseminationLists.createNewRuleBtn.click();
    disseminationLists.ruleWindowHeading.waitForVisible();
    expect(disseminationLists.ruleWindowHeading.isVisible()).to.eql(true);
    disseminationLists.ruleNameField.waitForVisible();
    disseminationLists.ruleNameField.setValue('QA Rule Test1');
    disseminationLists.recipientSearchField.waitForVisible();
    browser.setValue(".choices__input--cloned",['Sindhum','Enter']);
    browser.scroll(0,400);
    disseminationLists.saveChangesBtn.waitForVisible();
    disseminationLists.saveChangesBtn.click();
    disseminationLists.emptyFiltervalidationMessage.waitForVisible();
    expect(disseminationLists.emptyFiltervalidationMessage.getText()).to.eql(testData.disseminationlist.message);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80011",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80011",
        failObj
        )
      }
    })

  it("Error message is displayed when name of the rule is duplicated in the same case in the 'Create a list rule' pop-up", ()=>{
    disseminationLists.errorForRule();
    disseminationLists.ruleNameField.waitForVisible();
    disseminationLists.ruleNameField.setValue(testData.disseminationlist.rulelabel);
    disseminationLists.ruleAllKiidCheckbox.waitForVisible();
    disseminationLists.ruleAllKiidCheckbox.click();
    disseminationLists.recipientSearchField.waitForVisible();
    browser.setValue(".choices__input--cloned",['Sindhum','Enter']);
    disseminationLists.saveChangesBtn.waitForVisible();
    disseminationLists.saveChangesBtn.click();
    disseminationLists.validationMessage.waitForVisible();
    browser.pause(1000);
    expect(disseminationLists.validationMessage.getText()).to.eql(testData.disseminationlist.samerulevalidationmessage);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80012",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80012",
        failObj
        )
      }
    })

  it("Rule should not be created when no recipients is added", () =>{
    disseminationLists.errorForRule();
    disseminationLists.ruleNameField.waitForVisible();
    disseminationLists.ruleNameField.setValue('QA rule test automation');
    disseminationLists.ruleAllKiidCheckbox.waitForVisible();
    disseminationLists.ruleAllKiidCheckbox.click();
    disseminationLists.saveChangesBtn.waitForVisible();
    disseminationLists.saveChangesBtn.click();
    disseminationLists.messageForValidation.waitForVisible();
    browser.pause(2000);
    expect(disseminationLists.messageForValidation.getText()).to.eql(testData.disseminationlist.norecipientvalidation);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80013",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80013",
        failObj
        )
      }
  })

  it("User is able to delete the rule when clicked on the delete icon displayed next to the Recipients", ()=>{
    browser.refresh();
    disseminationLists.deleteRule(testData.disseminationlist.rulelabel);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79997",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79997",
        failObj
        )
      }
  })

  it("User should be able create a rule by adding one Share Class Name filter in the Create a list rule pop-up", ()=>{
    browser.refresh();
    disseminationLists.ruleCreationByClickingOnRuleBtn(testData.disseminationlist.rulenameforshareclass);
    disseminationLists.kiidSubFundFilterDropdown.waitForVisible();
    disseminationLists.kiidSubFundFilterDropdown.click();
    disseminationLists.nameFilterDropdown.waitForVisible();
    disseminationLists.nameFilterDropdown.click();
    browser.pause(10000);
    disseminationLists.selectKiidsFilterDropdown.waitForVisible();
    disseminationLists.selectKiidsFilterDropdown.click();
    disseminationLists.filterSearchField.waitForVisible();
    disseminationLists.filterSearchField.setValue(testData.filters.namefilter);
    disseminationLists.getSelectCurrencyFilterOption(4).waitForVisible();
    disseminationLists.getSelectCurrencyFilterOption(4).click();
    disseminationLists.selectLink.waitForVisible();
    disseminationLists.selectLink.click();
    disseminationLists.recipientSearchField.waitForVisible();
    browser.setValue(".choices__input--cloned",[testData.recipient.yopemail,'Enter']);
    browser.pause(1000);
    disseminationLists.saveChangesBtn.waitForVisible();
    disseminationLists.saveChangesBtn.click();
    browser.refresh();
    waitUntilTrueFalse(
      `#dissemination_rule_list_search`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    browser.pause(1000);
    disseminationLists.searchField.waitForVisible();
    disseminationLists.searchField.setValue(testData.disseminationlist.rulenameforshareclass);
    disseminationLists.getRuleNameLabel(1).waitForVisible();
    browser.waitUntil(
      function() {
      return (
        disseminationLists.getRuleNameLabel(1).getText() === testData.disseminationlist.rulenameforshareclass
      )
    },
    30000,
    "Rule card is not visible even after 30s"
    );
    expect(disseminationLists.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenameforshareclass);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79999",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "79999",
        failObj
        )
      }
    })

  it("User is able to edit the rule by Rule name in the Dissemination List Page", ()=>{
    waitUntilTrueFalse(
      `.dissemination-list .card--accordion-list:nth-child(1) .edit-new-rule`,
      "20000",
      `Rule card is not visible even after 20sec`,
      true
    );
    disseminationLists.getRuleEditIcon(1).waitForVisible();
    disseminationLists.getRuleEditIcon(1).click();
    disseminationLists.editRuleWindowTitle.waitForVisible();
    expect(disseminationLists.editRuleWindowTitle.isVisible()).to.eql(true);
    disseminationLists.ruleNameField.waitForVisible();
    disseminationLists.ruleNameField.setValue(testData.disseminationlist.rulenamelabel);
    browser.pause(1000);
    disseminationLists.saveChangesBtn.waitForVisible();
    disseminationLists.saveChangesBtn.click();
    browser.refresh();
    disseminationLists.searchField.waitForVisible();
    disseminationLists.searchField.setValue(testData.disseminationlist.rulenamelabel);
    disseminationLists.getRuleNameLabel(1).waitForVisible();
    browser.waitUntil(
      function() {
      return (
        disseminationLists.getRuleNameLabel(1).getText() === testData.disseminationlist.rulenamelabel
      )
    },
    30000,
    "Rule card is not visible even after 30s"
    );
    expect(disseminationLists.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamelabel);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80000",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80000",
        failObj
        )
      }
    })

  it("User should be able to search Rule in dissemination list", () =>{
    browser.refresh();
    disseminationLists.searchField.waitForVisible();
    disseminationLists.searchField.setValue(testData.disseminationlist.rulenamelabel);
    browser.pause(1000);
    disseminationLists.ruleLabelName.waitForVisible();
    expect(disseminationLists.ruleLabelName.getText()).to.eql(testData.disseminationlist.rulenamelabel);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80010",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80010",
        failObj
        )
      }
    })

  it("Number of recipients added to the rule should be displayed in rule card", () =>{
    var oldValue = disseminationLists.getTotalNumberRecipient(1).getText().split(" ");
    oldValue = parseInt(oldValue[1]);
    var newValue = disseminationLists.getTotalNumberRecipient(1).getText().split(" ");
    newValue = parseInt(newValue[2]);
    expect(oldValue + 1).to.eql(newValue);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80017",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80017",
        failObj
        )
      }
  })

  it("User is able to edit the Recipient in the Dissemination List Page", ()=>{
    waitUntilTrueFalse(
      `.dissemination-list .card--accordion-list:nth-child(1) .edit-new-rule`,
      "20000",
      `Rule card is not visible even after 20sec`,
      true
    );
    disseminationLists.getRuleEditIcon(1).waitForVisible();
    disseminationLists.getRuleEditIcon(1).click();
    disseminationLists.editRuleWindowTitle.waitForVisible();
    expect(disseminationLists.editRuleWindowTitle.isVisible()).to.eql(true);
    disseminationLists.recipientSearchField.waitForVisible();
    browser.setValue(".choices__input--cloned",[testData.recipient.ftprecipient,'Enter']);
    disseminationLists.editRuleWindowTitle.waitForVisible();
    disseminationLists.editRuleWindowTitle.click();
    browser.scroll(0,500);
    waitUntilTrueFalse(
      `.justify-content-between.d-flex .form-submit-rules:nth-child(2)`,
      "5000",
      `Save button is not visible even after 5sec`,
      true
    );
    disseminationLists.saveChangesBtn.waitForVisible();
    disseminationLists.saveChangesBtn.click();
    browser.refresh();
    disseminationLists.searchField.waitForVisible();
    disseminationLists.searchField.setValue(testData.disseminationlist.rulenamelabel);
    disseminationLists.getRuleNameLabel(1).waitForVisible();
    browser.waitUntil(
      function() {
      return (
        disseminationLists.getRuleNameLabel(1).getText() === testData.disseminationlist.rulenamelabel
      )
    },
    30000,
    "Rule card is not visible even after 30s"
    );
    expect(disseminationLists.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamelabel);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80002",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80002",
        failObj
        )
      }
  })

  it("User is able to search for the particular Sub Fund Name in the Search bar displayed in the Sub Fund Dropdown", ()=>{
    waitUntilTrueFalse(
      `.btn-primary.create-new-rule`,
      "10000",
      `Rule button is not visible even after 10sec`,
      true
    );
    disseminationLists.ruleCreationByClickingOnRuleBtn(testData.disseminationlist.rulelabel);
    disseminationLists.kiidSubFundFilterDropdown.waitForVisible();
    disseminationLists.kiidSubFundFilterDropdown.click();
    disseminationLists.codeFilterDropdown.waitForVisible();
    expect(disseminationLists.codeFilterDropdown.getText()).to.eql(testData.disseminationlist.filter);
    disseminationLists.nameFilterDropdown.waitForVisible();
    expect(disseminationLists.nameFilterDropdown.getText()).to.eql(testData.disseminationlist.shareclassfilter);
    disseminationLists.selectKiidsFilterDropdown.waitForVisible();
    disseminationLists.selectKiidsFilterDropdown.click();
    browser.pause(1000);
    disseminationLists.filterSearchField.waitForVisible();
    disseminationLists.filterSearchField.setValue(testData.kiidfilteroption.fundname);
    disseminationLists.getSelectCurrencyFilterOption(1).waitForVisible();
    expect(disseminationLists.getSelectCurrencyFilterOption(1).getText()).to.eql(testData.kiidfilteroption.fundname);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80014",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80014",
        failObj
        )
      }
  })

  it("User is able to search for the particular Shared class Name in the Search bar displayed in the Shared class Name Dropdown", ()=>{
    disseminationLists.kiidSubFundFilterDropdown.waitForVisible();
    disseminationLists.kiidSubFundFilterDropdown.click();
    disseminationLists.nameFilterDropdown.waitForVisible();
    disseminationLists.nameFilterDropdown.click();
    browser.pause(7000);
    disseminationLists.selectKiidsFilterDropdown.waitForVisible();
    disseminationLists.selectKiidsFilterDropdown.click();
    disseminationLists.filterSearchField.waitForVisible();
    disseminationLists.filterSearchField.setValue(testData.filters.namefilter);
    disseminationLists.getSelectCurrencyFilterOption(4).waitForVisible();
    expect(disseminationLists.getSelectCurrencyFilterOption(4).getText()).to.eql(testData.filters.namefilter);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80016",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80016",
        failObj
        )
      }
  })

  it("User is able to search for the particular Product APIR code in the Search bar displayed in the Product APIR code Dropdown", ()=>{
    disseminationLists.kiidSubFundFilterDropdown.waitForVisible();
    disseminationLists.kiidSubFundFilterDropdown.click();
    disseminationLists.codeFilterDropdown.waitForVisible();
    disseminationLists.codeFilterDropdown.click();
    browser.pause(12000);
    disseminationLists.selectKiidsFilterDropdown.waitForVisible();
    disseminationLists.selectKiidsFilterDropdown.click();
    disseminationLists.filterSearchField.waitForVisible();
    disseminationLists.filterSearchField.setValue(testData.tmdapprovaltab.isinname);
    disseminationLists.getSelectCurrencyFilterOption(769).waitForVisible();
    browser.waitUntil(
      function() {
      return (
        disseminationLists.getSelectCurrencyFilterOption(769).getText() === testData.tmdapprovaltab.isinname
      )
    },
    10000,
    "Kiid listing page is not visible even after 10s"
    );
    browser.refresh();
    disseminationLists.deleteRule(testData.disseminationlist.rulenameforalltmd);
    disseminationLists.deleteRule(testData.disseminationlist.rulenamelabel);
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "20000",
      `avatar image is not visible even after 20sec`,
      true
    );
    Login.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80015",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80015",
        failObj
        )
      }
  })
});

describe.skip("Rules creation for multiple filters", ()=>{
  it("User is able to create a rule by adding multiple 'Sub Fund' filter in the 'Create a list rule' pop-up", ()=>{
    Login.login(testData.zeidler.uatadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", ['QA Calibre Investment Management','Enter']);
    waitUntilTrueFalse(
      `.page-header .col-sm-12.col-lg-auto .nav-item:nth-child(3)`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getDisseminationLists(2).waitForVisible();
    tmdRecordingList.getDisseminationLists(2).click();
    expect(browser.getUrl()).to.eql(testData.zeidler.qadisseminationlistpage);
    waitUntilTrueFalse(
      `.btn-primary.create-new-rule`,
      "10000",
      `Rule button is not visible even after 10sec`,
      true
    );
    disseminationLists.ruleCreateMultiSubfund(testData.disseminationlist.rulenamewitlmultisubfund,testData.recipient.recipienteditname);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80021",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80021",
        failObj
        )
      }
  })

  it("User is able to create a rule by adding multiple Share Class Name' filter in the 'Create a list rule pop-up", ()=>{
    disseminationLists.ruleCreateMultiName(testData.disseminationlist.rulenamewitlmulticlass,testData.recipient.recipienteditname);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80022",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80022",
        failObj
        )
      }
  })

  it("User is able to create a rule by adding multiple 'Product APIR Code' filter in the 'Create a list rule pop-up", ()=>{
    disseminationLists.ruleCreateMultiIsin(testData.disseminationlist.rulenamewitlmultiisin,testData.recipient.recipienteditname);
    browser.refresh();
    disseminationLists.deleteRule(testData.disseminationlist.rulenamewitlmultisubfund);
    disseminationLists.deleteRule(testData.disseminationlist.rulenamewitlmulticlass);
    disseminationLists.deleteRule(testData.disseminationlist.rulenamewitlmultiisin);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81102",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81102",
        failObj
        )
      }
  })

  it("User is able to create a rule by adding a free recipient", ()=>{
    disseminationLists.createRuleFree(testData.disseminationlist.rulenamewithfree,testData.recipient.recipienteditname,testData.disseminationlist.rulenamewithfree);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80026",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80026",
        failObj
        )
      }
  })

  it("User is able to create a rule by adding a chargeable recipient", ()=>{
    disseminationLists.chargableRule(testData.disseminationlist.rulenamewithchargable,testData.recipient.recipientname);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80025",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80025",
        failObj
        )
      }
  })

  it("User is able to create rule by adding both Chargeable and free recipients", ()=>{
    disseminationLists.freeChargRecipients(testData.disseminationlist.rulenamewithfreechargable,testData.recipient.recipientname,testData.recipient.recipienteditname);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80027",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80027",
        failObj
        )
      }
  })

  it("User is able to add Bulk Recipients in the 'Create a list rule' pop-up", ()=>{
    disseminationLists.bulkRecipients(testData.disseminationlist.rulenamewithbulk,testData.recipient.recipientname,testData.recipient.recipienteditname,testData.recipient.ftprecipient);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80029",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80029",
        failObj
        )
      }
  })

  it("User is able to edit rule by adding new filters in the 'Dissemination List' Page", ()=>{
    waitUntilTrueFalse(
      `.dissemination-list .card--accordion-list:nth-child(1) .edit-new-rule`,
      "20000",
      `Rule card is not visible even after 20sec`,
      true
    );
    disseminationLists.getRuleEditIcon(1).waitForVisible();
    disseminationLists.getRuleEditIcon(1).click();
    disseminationLists.plusBtn.waitForVisible();
    disseminationLists.plusBtn.click();
    browser.pause(15000);
    disseminationLists.selectIsinFilterDropdown.waitForVisible();
    disseminationLists.selectIsinFilterDropdown.click();
    disseminationLists.getKiidFilterOptionCheckbox(1).waitForVisible();
    disseminationLists.getKiidFilterOptionCheckbox(1).click();
    disseminationLists.selectLink.waitForVisible();
    disseminationLists.selectLink.click();
    browser.pause(1000);
    disseminationLists.saveChangesBtn.waitForVisible();
    disseminationLists.saveChangesBtn.click();
    browser.refresh();
    disseminationLists.searchField.waitForVisible();
    disseminationLists.searchField.setValue(testData.disseminationlist.rulenamewithbulk);
    disseminationLists.getRuleNameLabel(1).waitForVisible();
    browser.waitUntil(
      function() {
      return (
        disseminationLists.getRuleNameLabel(1).getText() === testData.disseminationlist.rulenamewithbulk
      )
    },
    30000,
    "Rule card is not visible even after 30s"
    );
    expect(disseminationLists.getRuleNameLabel(1).getText()).to.eql(testData.disseminationlist.rulenamewithbulk);
    disseminationLists.getRuleNameLabel(1).click();
    disseminationLists.getFiltersName(1).waitForVisible();
    expect(disseminationLists.getFiltersName(1).getText()).to.eql(testData.filters.fund);
    disseminationLists.getFiltersName(2).waitForVisible();
    expect(disseminationLists.getFiltersName(2).getText()).to.eql(testData.filters.code);
    browser.refresh();
    disseminationLists.deleteRule(testData.disseminationlist.rulenamewithfree);
    disseminationLists.deleteRule(testData.disseminationlist.rulenamewithchargable);
    disseminationLists.deleteRule(testData.disseminationlist.rulenamewithfreechargable);
    disseminationLists.deleteRule(testData.disseminationlist.rulenamewithbulk);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80028",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80028",
        failObj
        )
      }
  })

  it("Rule is created by selecting one option from all filters in the 'Create a list rule' pop-up", ()=>{
    waitUntilTrueFalse(
      `.btn-primary.create-new-rule`,
      "10000",
      `Rule button is not visible even after 10sec`,
      true
    );
    disseminationLists.ruleCreate(testData.disseminationlist.rulenamewithallfilters,testData.recipient.recipienteditname);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80023",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80023",
        failObj
        )
      }
  })

  it("Rule is created by selecting multiple options from all filters in the 'Create a list rule' pop-up", ()=>{
    disseminationLists.createRuleMulti(testData.disseminationlist.rulenamewithallmultifilter,testData.recipient.recipienteditname);
    browser.refresh();
    disseminationLists.deleteRule(testData.disseminationlist.rulenamewithallfilters);
    disseminationLists.deleteRule(testData.disseminationlist.rulenamewithallmultifilter);
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "20000",
      `avatar image is not visible even after 20sec`,
      true
    );
    Login.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80024",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "80024",
        failObj
        )
      }
  })
});

describe.skip("Recipient and rule", ()=>{
  it("when admin remove client in Client mapping page in the client portal the removed client for particular recipient name should not be visible in the Client portal", ()=>{
    Login.adminLogin();
    Login.launchUrl();
    tmdRecipant.mandatoryRecipient(testData.recipient.name);
    tmdRecipant.logout();
    Login.login(testData.zeidler.qaadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForExist();
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", [testData.kiidfilteroption.fundname,'Enter']);
    waitUntilTrueFalse(
      `.page-header .col-sm-12.col-lg-auto .nav-item:nth-child(3)`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getDisseminationLists(2).waitForVisible();
    tmdRecordingList.getDisseminationLists(2).click();
    expect(browser.getUrl()).to.eql(testData.zeidler.qadisseminationlistpage);
    disseminationLists.createNewRuleBtn.waitForVisible();
    disseminationLists.createNewRuleBtn.click();
    disseminationLists.recipientSearchField.waitForVisible();
    browser.setValue(".choices__input--cloned",[testData.recipient.name]);
    disseminationLists.noFoundResult.waitForVisible();
    expect(disseminationLists.noFoundResult.getText()).to.eql(testData.disseminationlist.noresultsfound);
    browser.refresh();
    Login.logout();
    Login.adminLogin();
    Login.launchUrl();
    tmdRecipant.deleteRecipient(testData.recipient.name);
    tmdRecipant.deleteRecipient(testData.recipient.ftprecipientwithoutrd);
    tmdRecipant.deleteRecipient(testData.recipient.ftprecipient);
    tmdRecipant.deleteRecipient(testData.recipient.recipienteditname);
    tmdRecipant.deleteRecipient(testData.recipient.recipientname);
    tmdRecipant.logout();
    pass = 1 
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81101",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81101",
        failObj
        )
      }
  })
});

describe.skip("Dissemination", ()=>{
  it("When multiple email addresses are specified for a recipient, rule list is delivered to all email addresses", () =>{
    Login.adminLogin();
    Login.launchUrl();
    tmdRecipant.disseminationRunRecipient(testData.recipient.recipientname1, testData.recipient.editcontactemail, testData.recipient.recipientcontactemail);
    tmdRecipant.searchField.waitForVisible();
    browser.setValue("#search",['Sindhu M','Enter']);
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.editrecipientpage);
    tmdRecipant.clientMappingBtn.waitForVisible();
    tmdRecipant.clientMappingBtn.click();
    browser.pause(1000);
    tmdRecipant.addClientBtn.waitForVisible();
    tmdRecipant.addClientBtn.click();
    tmdRecipant.clientField.waitForVisible();
    browser.setValue(".choices__input.choices__input--cloned",[testData.kiidfilteroption.fundname,'Enter']);
    tmdRecipant.addClientPopupHeading.waitForVisible();
    tmdRecipant.addClientPopupHeading.click();
    tmdRecipant.clientSaveBtn.waitForVisible();
    tmdRecipant.clientSaveBtn.click();
    tmdRecipant.clientQaName.waitForVisible();
    expect(tmdRecipant.clientQaName.getText()).to.eql(testData.kiidfilteroption.fundname);
    tmdRecipant.disseminationRunRecipient1(testData.recipient.recipienteditname, testData.recipient.recipientemailauto, testData.recipient.recipientinvalidemail);
    tmdRecipant.searchField.waitForVisible();
    browser.setValue("#search",['Sindhum','Enter']);
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.editrecipientpage);
    tmdRecipant.clientMappingBtn.waitForVisible();
    tmdRecipant.clientMappingBtn.click();
    browser.pause(1000);
    tmdRecipant.addClientBtn.waitForVisible();
    tmdRecipant.addClientBtn.click();
    tmdRecipant.clientField.waitForVisible();
    browser.setValue(".choices__input.choices__input--cloned",[testData.kiidfilteroption.fundname,'Enter']);
    tmdRecipant.addClientPopupHeading.waitForVisible();
    tmdRecipant.addClientPopupHeading.click();
    tmdRecipant.clientSaveBtn.waitForVisible();
    tmdRecipant.clientSaveBtn.click();
    tmdRecipant.logout();
    Login.login(testData.zeidler.qaadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", [testData.kiidfilteroption.fundname,'Enter']);
    waitUntilTrueFalse(
      `.page-header .col-sm-12.col-lg-auto .nav-item:nth-child(3)`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getDisseminationLists(2).waitForVisible();
    tmdRecordingList.getDisseminationLists(2).click();
    expect(browser.getUrl()).to.eql(testData.zeidler.qadisseminationlistpage);
    disseminationLists.createRule(testData.disseminationlist.rulenameauto, testData.recipient.recipientname1, testData.recipient.recipienteditname);
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    Login.logout();
    pass = 1
  })

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81197",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81197",
        failObj
        )
      }
    })

  it("XLS file is sent to the email recipient when XLS is selected in the download type is selected in the Add Recipient page", ()=>{
    Login.adminLogin();
    Login.launchUrl();
    userEmail = tmdRecipant.returnPublicname();
    console.log("userEmail =" +userEmail);
    tmdRecipant.disseminationRunRecipient(testData.recipient.contactname, testData.recipient.editcontactemail, userEmail);
    tmdRecipant.searchField.waitForVisible();
    browser.setValue("#search",['jithendra','Enter']);
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.editrecipientpage);
    tmdRecipant.clientMappingBtn.waitForVisible();
    tmdRecipant.clientMappingBtn.click();
    browser.pause(1000);
    tmdRecipant.addClientBtn.waitForVisible();
    tmdRecipant.addClientBtn.click();
    tmdRecipant.clientField.waitForVisible();
    browser.setValue(".choices__input.choices__input--cloned",[testData.kiidfilteroption.fundname,'Enter']);
    tmdRecipant.addClientPopupHeading.waitForVisible();
    tmdRecipant.addClientPopupHeading.click();
    tmdRecipant.clientSaveBtn.waitForVisible();
    tmdRecipant.clientSaveBtn.click();
    tmdRecipant.clientQaName.waitForVisible();
    expect(tmdRecipant.clientQaName.getText()).to.eql(testData.kiidfilteroption.fundname);
    tmdRecipant.logout();
    Login.login(testData.zeidler.qaadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", [testData.kiidfilteroption.fundname,'Enter']);
    waitUntilTrueFalse(
      `.page-header .col-sm-12.col-lg-auto .nav-item:nth-child(3)`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getDisseminationLists(2).waitForVisible();
    tmdRecordingList.getDisseminationLists(2).click();
    expect(browser.getUrl()).to.eql(testData.zeidler.qadisseminationlistpage);
    disseminationLists.createRuleForDissemination(testData.disseminationlist.automationrulename,testData.recipient.contactname,testData.disseminationlist.automationrulename,testData.disseminationlist.automationrulename);
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    Login.logout();
  });

  it("CSV file should be sent to the email recipient when CSV is selected in the Add Recipient page", () =>{
    Login.adminLogin();
    Login.launchUrl();
    csvEmail = tmdRecipant.returnPublicname();
    console.log("csvEmail =" +csvEmail);
    tmdRecipant.disseminationRunRecipientCsv(testData.recipient.recipientnameauto, testData.recipient.recipientcontactemail, csvEmail);
    tmdRecipant.searchField.waitForVisible();
    browser.setValue("#search",['Recipient automation','Enter']);
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.editrecipientpage);
    tmdRecipant.clientMappingBtn.waitForVisible();
    tmdRecipant.clientMappingBtn.click();
    browser.pause(1000);
    tmdRecipant.addClientBtn.waitForVisible();
    tmdRecipant.addClientBtn.click();
    tmdRecipant.clientField.waitForVisible();
    browser.setValue(".choices__input.choices__input--cloned",[testData.kiidfilteroption.fundname,'Enter']);
    tmdRecipant.addClientPopupHeading.waitForVisible();
    tmdRecipant.addClientPopupHeading.click();
    tmdRecipant.clientSaveBtn.waitForVisible();
    tmdRecipant.clientSaveBtn.click();
    tmdRecipant.clientQaName.waitForVisible();
    expect(tmdRecipant.clientQaName.getText()).to.eql(testData.kiidfilteroption.fundname);
    tmdRecipant.logout();
    Login.login(testData.zeidler.qaadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", [testData.kiidfilteroption.fundname,'Enter']);
    waitUntilTrueFalse(
      `.page-header .col-sm-12.col-lg-auto .nav-item:nth-child(3)`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getDisseminationLists(2).waitForVisible();
    tmdRecordingList.getDisseminationLists(2).click();
    expect(browser.getUrl()).to.eql(testData.zeidler.qadisseminationlistpage);
    disseminationLists.createRuleForDissemination(testData.disseminationlist.autorulename, testData.recipient.recipientnameauto,testData.disseminationlist.autorulename,testData.disseminationlist.autorulename);
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    Login.logout();
  });

  it("KIID Dissemination is successful when the rule is created by selecting the RSA private key recipient", ()=>{
    Login.adminLogin();
    Login.launchUrl();
    sftkeyEmail = tmdRecipant.returnPublicname();
    console.log("sftkeyEmail =" +sftkeyEmail);
    tmdRecipant.sftpRecipientForDissemination(testData.recipient.sftprecipientkey,2,2,testData.recipient.hostdetails,testData.recipient.username,testData.recipient.rootdirectory,testData.recipient.sftprecipientkey);
    // tmdRecipant.searchField.waitForVisible();
    // browser.setValue("#search",[testData.recipient.sftprecipientkey,'Enter']);
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.editrecipientpage);
    tmdRecipant.clientMappingBtn.waitForVisible();
    tmdRecipant.clientMappingBtn.click();
    browser.pause(1000);
    tmdRecipant.addClientBtn.waitForVisible();
    tmdRecipant.addClientBtn.click();
    tmdRecipant.clientField.waitForVisible();
    browser.setValue(".choices__input.choices__input--cloned",[testData.kiidfilteroption.fundname,'Enter']);
    tmdRecipant.addClientPopupHeading.waitForVisible();
    tmdRecipant.addClientPopupHeading.click();
    tmdRecipant.clientSaveBtn.waitForVisible();
    tmdRecipant.clientSaveBtn.click();
    tmdRecipant.clientQaName.waitForVisible();
    expect(tmdRecipant.clientQaName.getText()).to.eql(testData.kiidfilteroption.fundname);
    tmdRecipant.logout();
    Login.login(testData.zeidler.qaadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", [testData.kiidfilteroption.fundname,'Enter']);
    waitUntilTrueFalse(
      `.page-header .col-sm-12.col-lg-auto .nav-item:nth-child(3)`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getDisseminationLists(2).waitForVisible();
    tmdRecordingList.getDisseminationLists(2).click();
    expect(browser.getUrl()).to.eql(testData.zeidler.qadisseminationlistpage);
    disseminationLists.createRuleForDissemination(testData.disseminationlist.rulenamewithsftpprivatekey, testData.recipient.sftprecipientkey,testData.disseminationlist.rulenamewithsftpprivatekey,testData.disseminationlist.rulenamewithsftpprivatekey);
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    Login.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81155",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81155",
        failObj
        )
      }
    })

  it("KIID Dissemination is successful when the rule is created by selecting SFTP Recipient with a password", ()=>{
    Login.adminLogin();
    Login.launchUrl();
    sftpwdEmail = tmdRecipant.returnPublicname();
    console.log("sftpwdEmail =" +sftpwdEmail);
    tmdRecipant.ftpRecipientForDissemination(testData.recipient.sftprecipient,2,2,testData.recipient.hostdetails,testData.recipient.username,testData.recipient.sftppassword,testData.recipient.rootdirectoryname,testData.recipient.sftprecipient);
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    expect(tmdRecipant.editRecipientPage.getText()).to.eql(testData.recipient.editrecipientpage);
    tmdRecipant.clientMappingBtn.waitForVisible();
    tmdRecipant.clientMappingBtn.click();
    browser.pause(1000);
    tmdRecipant.addClientBtn.waitForVisible();
    tmdRecipant.addClientBtn.click();
    tmdRecipant.clientField.waitForVisible();
    browser.setValue(".choices__input.choices__input--cloned",[testData.kiidfilteroption.fundname,'Enter']);
    tmdRecipant.addClientPopupHeading.waitForVisible();
    tmdRecipant.addClientPopupHeading.click();
    tmdRecipant.clientSaveBtn.waitForVisible();
    tmdRecipant.clientSaveBtn.click();
    tmdRecipant.clientQaName.waitForVisible();
    expect(tmdRecipant.clientQaName.getText()).to.eql(testData.kiidfilteroption.fundname);
    tmdRecipant.logout();
    Login.login(testData.zeidler.qaadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.uatadminhome);
    browser.url(testData.zeidler.qatmdpage);
    browser.waitUntil(
      function() {
      return (
      tmdRecordingList.zeilderUmHeading.isVisible() === true
      )
    },
    30000,
    "Kiid listing page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    tmdRecordingList.zeidlerDropdown.waitForVisible();
    tmdRecordingList.zeidlerDropdown.click();
    tmdRecordingList.searchField.waitForVisible();
    browser.setValue(".dropdown-choices__menu input", [testData.kiidfilteroption.fundname,'Enter']);
    waitUntilTrueFalse(
      `.page-header .col-sm-12.col-lg-auto .nav-item:nth-child(3)`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    tmdRecordingList.getDisseminationLists(2).waitForVisible();
    tmdRecordingList.getDisseminationLists(2).click();
    expect(browser.getUrl()).to.eql(testData.zeidler.qadisseminationlistpage);
    disseminationLists.createRuleForDissemination(testData.disseminationlist.rulenamewithsftppwd, testData.recipient.sftprecipient,testData.disseminationlist.rulenamewithsftppwd,testData.disseminationlist.rulenamewithsftppwd);
    waitUntilTrueFalse(
      `.navbar-nav > ul > a`,
      "5000",
      `Filters is not visible even after 5sec`,
      true
    );
    Login.logout();
    Login.login(testData.zeidler.qaadminhome, testData.login.qaadminusername, testData.login.qaadminpwd);
    expect(browser.getUrl()).to.eql(testData.zeidler.qaadminhome);
    Home.avatarImage.waitForVisible();
    Home.avatarImage.click();
    Home.adminPanel.waitForVisible();
    Home.adminPanel.click();
    expect(browser.getUrl()).to.eql(testData.zeidler.qaadminpage);
    browser.url(testData.zeidler.tmdapprovallink);
    expect(browser.getUrl()).to.eql(testData.zeidler.tmdapprovallink);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",[testData.tmdapprovaltab.isinname,'Enter']);
    tmdAdmin.approvedKiidCheckbox.waitForVisible();
    tmdAdmin.approvedKiidCheckbox.click();
    tmdAdmin.kiidBulkActionDropdown.waitForVisible();
    tmdAdmin.kiidBulkActionDropdown.click();
    tmdAdmin.changeStatusOption.waitForVisible();
    tmdAdmin.changeStatusOption.click();
    browser.pause(1000);
    tmdAdmin.selectBtn.waitForVisible();
    tmdAdmin.selectBtn.click();
    tmdAdmin.getFiledOption(9).waitForVisible();
    tmdAdmin.getFiledOption(9).click();
    tmdAdmin.applyToBtn.waitForVisible();
    tmdAdmin.applyToBtn.click();
    tmdRecipant.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81156",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81156",
        failObj
        )
      }
    })
});

describe.skip("Dissemination transaction", ()=>{
  it("User can filter dissemination by rule name and transaction date", () =>{
    Login.adminLogin();
    browser.url(testData.zeidler.qatransactionpage);
    expect(browser.getUrl()).to.eql(testData.zeidler.qatransactionpage);
    disseminationTransaction.kiidBtn.waitForVisible();
    disseminationTransaction.kiidBtn.click();
    disseminationTransaction.getTmdTab(5).waitForVisible();
    disseminationTransaction.getTmdTab(5).click();
    disseminationTransaction.filterDropdown.waitForVisible();
    disseminationTransaction.filterDropdown.click();
    disseminationTransaction.ruleNameFilter.waitForVisible();
    browser.setValue("form>div:nth-child(2)>div:nth-child(2) .choices__input--cloned",[testData.transaction.transrulename,'Enter']);
    //browser.setValue("form>div:nth-child(2)>div:nth-child(2) .choices__input--cloned",['QA 10 NOV TEST','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.applyFilterBtn.waitForVisible();
    disseminationTransaction.applyFilterBtn.click();
    expect(disseminationTransaction.getRuleNameLabel(1,4).getText()).to.eql(testData.transaction.transrulename);
    //expect(disseminationTransaction.getRuleNameLabel(1,5).getText()).to.eql(testData.transaction.rulename1);
    browser.pause(1000);
    disseminationTransaction.resetAllBtn.waitForVisible();
    disseminationTransaction.resetAllBtn.click();
    disseminationTransaction.transactionDateFilter.waitForVisible();
    disseminationTransaction.transactionDateFilter.click();
    // disseminationTransaction.previousMonthIcon.waitForVisible();
    // disseminationTransaction.previousMonthIcon.click();
    disseminationTransaction.transactionTodayDate.waitForVisible();
    disseminationTransaction.transactionTodayDate.click();
    disseminationTransaction.getTransactionDate(1).waitForVisible();
    disseminationTransaction.getTransactionDate(1).click();
    disseminationTransaction.applyFilterBtn.waitForVisible();
    disseminationTransaction.applyFilterBtn.click();
    expect(disseminationTransaction.transactionDateValue.isVisible()).to.eql(true);
    disseminationTransaction.resetAllBtn.waitForVisible();
    disseminationTransaction.resetAllBtn.click();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81149",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81149",
        failObj
        )
      }
    })

  it("User can filter dissemination by recipient name,recipient email and status", () =>{
    waitUntilTrueFalse(
    `form>div:nth-child(3)>div:nth-child(2) .choices__input--cloned`,
    "20000",
    `Rule card is not visible even after 20sec`,
    true
    );
    disseminationTransaction.transactionEmailFilter.waitForVisible();
    browser.setValue("form>div:nth-child(3)>div:nth-child(2) .choices__input--cloned",['sindhu@calibrecode.com','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.applyFilterBtn.waitForVisible();
    disseminationTransaction.applyFilterBtn.click();
    expect(disseminationTransaction.getRuleNameLabel(1,6).getText()).to.eql(testData.transaction.email);
    browser.pause(1000);
    disseminationTransaction.resetAllBtn.waitForVisible();
    disseminationTransaction.resetAllBtn.click();
    disseminationTransaction.recipientNameFilter.waitForVisible();
    browser.setValue("form>div:nth-child(3)>div:nth-child(3) .choices__input--cloned",[testData.transaction.name,'Enter']);
    //browser.setValue("form>div:nth-child(3)>div:nth-child(3) .choices__input--cloned",['Test KIID Rec','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.applyFilterBtn.waitForVisible();
    disseminationTransaction.applyFilterBtn.click();
    expect(disseminationTransaction.getRuleNameLabel(1,5).getText()).to.eql(testData.transaction.name);
    //expect(disseminationTransaction.getRuleNameLabel(1,6).getText()).to.eql(testData.transaction.recipientname);
    disseminationTransaction.resetAllBtn.waitForVisible();
    disseminationTransaction.resetAllBtn.click();
    browser.pause(1000);
    disseminationTransaction.statusFilter.waitForVisible();
    browser.setValue("form>div:nth-child(3)>div:nth-child(4) .choices__input--cloned",['Success','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.applyFilterBtn.waitForVisible();
    disseminationTransaction.applyFilterBtn.click();
    expect(disseminationTransaction.getRuleNameLabel(1,7).getText()).to.eql(testData.transaction.status);
    disseminationTransaction.resetAllBtn.waitForVisible();
    disseminationTransaction.resetAllBtn.click();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81153",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81153",
        failObj
        )
      }
    })

  it("Added filters are not reset when the number of rows for the page are modified", () =>{
    disseminationTransaction.statusFilter.waitForVisible();
    browser.setValue("form>div:nth-child(3)>div:nth-child(4) .choices__input--cloned",['Success','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.applyFilterBtn.waitForVisible();
    disseminationTransaction.applyFilterBtn.click();
    disseminationTransaction.getRuleNameLabel(1,7).waitForVisible();
    expect(disseminationTransaction.getRuleNameLabel(1,7).getText()).to.eql(testData.transaction.status);
    browser.scroll(0,1000);
    tmdAdmin.showDropdown.waitForVisible();
    tmdAdmin.showDropdown.click();
    tmdAdmin.showDropdownOption.waitForVisible();
    tmdAdmin.showDropdownOption.click();
    var oldValue = disseminationTransaction.transactionCount.getText().split(" ");
    oldValue = parseInt(oldValue[229]);
    var newValue = disseminationTransaction.transactionCount.getText().split(" ");;
    newValue = parseInt(newValue[118]);
    expect(oldValue - 111).to.eql(newValue);
    tmdRecipant.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81151",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81151",
        failObj
        )                          
      }
    })
});






