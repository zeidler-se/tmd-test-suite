import testData from "../constants/testData.json"
import { expect } from 'chai'
import Login from "../page-objects/login.page"
import {addTestRailResult} from "../helpers/apihelpers"
import { BrowserRouter } from "react-router-dom"
import test from "../page-objects/testdata.page"
import testLogin from "../page-objects/login.page"
import Home from "../page-objects/home.page"
import tmdRecordingList from "../page-objects/tmdrecordlisting.page"
import tmdRecipant from "../page-objects/tmdadminrecipant.page"
import tmdAdmin from "../page-objects/tmdadmin.page"
import disseminationLists from "../page-objects/disseminationlist.page"
import disseminationHistory from "../page-objects/disseminationhistory.page"
import disseminationTransaction from "../page-objects/disseminationtransaction.page"
import { badEnumValueMessage } from "graphql/validation/rules/ValuesOfCorrectType"
import Gmail from "../page-objects/gmail.page"
import gmailPage from "../page-objects/gmail.page"
import { waitUntilTrueFalse } from "../page-objects/function";
import Exavault from "../page-objects/exavault.page"


var passObj = {
  status_id: 1,
  comment: "Pass - Automation"
}
var failObj = {
  status_id: 5,
  comment: "Fail - Automation"
}
var pass = 0

let i;

const fsExtra = require("fs-extra");
const pathfile = require("path");
const folderpath = pathfile.join(
  __dirname,
  "..",
  "downloads"
);

describe.skip("Dissemination run", () =>{
  it("Status of the Dissemination Transactions should be displayed for recipients after the Dissemination is run(success)", () =>{
    Login.adminLoginWithCookies();
    browser.url(testData.zeidler.qatransactionpage);
    expect(browser.getUrl()).to.eql(testData.zeidler.qatransactionpage);
    disseminationTransaction.kiidBtn.waitForVisible();
    disseminationTransaction.kiidBtn.click();
    disseminationTransaction.getTmdTab(5).waitForVisible();
    disseminationTransaction.getTmdTab(5).click();
    waitUntilTrueFalse(
    `.btn.btn--light.text--white.has-icon.has-caret`,
    "20000",
    `Rule card is not visible even after 20sec`,
    true
    );
    disseminationTransaction.filterDropdown.waitForVisible();
    disseminationTransaction.filterDropdown.click();
    // disseminationTransaction.ruleNameFilter.waitForVisible();
    // browser.setValue("form>div:nth-child(2)>div:nth-child(2) .choices__input--cloned",['QA KIID Auto','Enter']);
    // disseminationTransaction.transactionHeading.waitForVisible();
    // disseminationTransaction.transactionHeading.click();
    // disseminationTransaction.transactionDateFilter.waitForVisible();
    // disseminationTransaction.transactionDateFilter.click();
    // disseminationTransaction.transactionTodayDate.waitForVisible();
    // disseminationTransaction.transactionTodayDate.click();
    // disseminationTransaction.transactionDateNextDay.waitForVisible();
    // disseminationTransaction.transactionDateNextDay.click();
    // disseminationTransaction.transactionEmailFilter.waitForVisible();
    // browser.setValue("form>div:nth-child(3)>div:nth-child(2) .choices__input--cloned",['sindhu@calibrecode.com','Enter']);
    // disseminationTransaction.transactionHeading.waitForVisible();
    // disseminationTransaction.transactionHeading.click();
    // disseminationTransaction.applyFilterBtn.waitForVisible();
    // disseminationTransaction.applyFilterBtn.click();
    // disseminationTransaction.emailLabel.waitForVisible();
    // expect(disseminationTransaction.emailLabel.getText()).to.eql(testData.transaction.email);
    // disseminationTransaction.statusLabel.waitForVisible();
    // expect(disseminationTransaction.statusLabel.getText()).to.eql(testData.transaction.status);
    // browser.scroll(0,300);
    // browser.pause(1000);
    // disseminationTransaction.resetAllBtn.waitForVisible();
    // disseminationTransaction.resetAllBtn.click();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81143",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81143",
        failObj
        )
      }
    })

  it.skip("Status of the Dissemination Transactions should be displayed for recipients after the Dissemination is run(success)", () =>{
    disseminationTransaction.transactionFilterForStatus();
    disseminationTransaction.transactionEmailFilter.waitForVisible();
    browser.setValue("form>div:nth-child(3)>div:nth-child(2) .choices__input--cloned",['cnusindhu92@gmail.com','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.applyFilterBtn.waitForVisible();
    disseminationTransaction.applyFilterBtn.click();
    disseminationTransaction.emailLabel.waitForVisible();
    expect(disseminationTransaction.emailLabel.getText()).to.eql(testData.transaction.emailauto);
    disseminationTransaction.statusLabel.waitForVisible();
    expect(disseminationTransaction.statusLabel.getText()).to.eql(testData.transaction.status);
    disseminationTransaction.resetAllBtn.waitForVisible();
    disseminationTransaction.resetAllBtn.click();
  });

  it("Status of the Dissemination Transactions should be displayed for recipients after the Dissemination is run(failed)", () =>{
    disseminationTransaction.transactionForEditedRecipient();
    disseminationTransaction.emailLabel.waitForVisible();
    expect(disseminationTransaction.emailLabel.getText()).to.eql(testData.recipient.recipientemailauto);
    disseminationTransaction.statusLabel.waitForVisible();
    expect(disseminationTransaction.statusLabel.getText()).to.eql(testData.transaction.statusfailed);
    disseminationTransaction.resetAllBtn.waitForVisible();
    disseminationTransaction.resetAllBtn.click();
  });

  it("Edited emails in the All Dissemination Transactions page are reflected in the Recipients", () =>{
    disseminationTransaction.transactionForEditedRecipient();
    disseminationTransaction.editIcon.waitForVisible();
    disseminationTransaction.editIcon.click();
    disseminationTransaction.emailField.waitForVisible();
    disseminationTransaction.emailField.setValue("cnusindhu92@gmail.com");
    disseminationTransaction.saveBtn.waitForVisible();
    disseminationTransaction.saveBtn.click();
    browser.pause(1000);
    browser.alertAccept();
    browser.url(testData.zeidler.qakiidrecipientpage);
    expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
    tmdRecipant.searchField.waitForVisible();
    browser.setValue("#search",['Sindhum','Enter']);
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    browser.scroll(0,500);
    tmdRecipant.getEmailAddressName(2).waitForVisible();
    expect(tmdRecipant.getEmailAddressName(2).getText()).to.eql(testData.transaction.emailaddress);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81150",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81150",
        failObj
        )
      }
    })

  it("User should be able to Retry the failed Dissemination by clicking on the Retry Option", () =>{
    disseminationTransaction.transactionForXlsCsv(testData.transaction.transrulename,testData.recipient.recipientemailauto);
    expect(disseminationTransaction.emailLabel.getText()).to.eql(testData.recipient.recipientemailauto);
    browser.pause(1000);
    expect(disseminationTransaction.statusLabel.getText()).to.eql(testData.transaction.statusfailed);
    disseminationTransaction.editIcon.waitForVisible();
    disseminationTransaction.editIcon.click();
    disseminationTransaction.emailField.waitForVisible();
    disseminationTransaction.emailField.setValue("cnusindhu92@gmail.com");
    disseminationTransaction.saveBtn.waitForVisible();
    disseminationTransaction.saveBtn.click();
    browser.pause(1000);
    browser.alertAccept();
    disseminationTransaction.transactionCheckbox.waitForVisible();
    disseminationTransaction.transactionCheckbox.click();
    disseminationTransaction.bulkActionDropdown.waitForVisible();
    disseminationTransaction.bulkActionDropdown.click();
    disseminationTransaction.retryOption.waitForVisible();
    disseminationTransaction.retryOption.click();
    disseminationTransaction.applyToBtn.waitForVisible();
    disseminationTransaction.applyToBtn.click();
    waitUntilTrueFalse(
      `.btn.btn--light.text--white.has-icon.has-caret`,
      "10000",
      `Rule card is not visible even after 10sec`,
      true
      );
    disseminationTransaction.filterDropdown.waitForVisible();
    disseminationTransaction.filterDropdown.click();
    disseminationTransaction.ruleNameFilter.waitForVisible();
    browser.setValue("form>div:nth-child(2)>div:nth-child(2) .choices__input--cloned",['QA KIID Auto','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.transactionDateFilter.waitForVisible();
    disseminationTransaction.transactionDateFilter.click();
    disseminationTransaction.transactionTodayDate.waitForVisible();
    disseminationTransaction.transactionTodayDate.click();
    disseminationTransaction.transactionDateNextDay.waitForVisible();
    disseminationTransaction.transactionDateNextDay.click();
    disseminationTransaction.transactionEmailFilter.waitForVisible();
    browser.setValue("form>div:nth-child(3)>div:nth-child(2) .choices__input--cloned",['cnnusindhu92@gmail.com','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.applyFilterBtn.waitForVisible();
    disseminationTransaction.applyFilterBtn.click();
    expect(disseminationTransaction.statusLabel.getText()).to.eql(testData.transaction.statusretry);
    disseminationTransaction.resetAllBtn.waitForVisible();
    disseminationTransaction.resetAllBtn.click();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81144",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81144",
        failObj
        )
      }
    })

  it("Emails deactivated in the Dissemination Transactions page are deleted from the Recipients", () =>{
    disseminationTransaction.transactionFilterForStatus();
    disseminationTransaction.transactionEmailFilter.waitForVisible();
    browser.setValue("form>div:nth-child(3)>div:nth-child(2) .choices__input--cloned",['cnuusindhu92@gmail.com','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.applyFilterBtn.waitForVisible();
    disseminationTransaction.applyFilterBtn.click();
    disseminationTransaction.deleteIcon.waitForVisible();
    disseminationTransaction.deleteIcon.click();
    browser.pause(1000);
    browser.alertAccept();
    browser.alertAccept();
    waitUntilTrueFalse(
      `.btn.btn--light.text--white.has-icon.has-caret`,
      "10000",
      `Rule card is not visible even after 10sec`,
      true
      );
    disseminationTransaction.filterDropdown.waitForVisible();
    disseminationTransaction.filterDropdown.click();
    disseminationTransaction.ruleNameFilter.waitForVisible();
    browser.setValue("form>div:nth-child(2)>div:nth-child(2) .choices__input--cloned",['QA KIID Auto','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.transactionDateFilter.waitForVisible();
    disseminationTransaction.transactionDateFilter.click();
    disseminationTransaction.transactionTodayDate.waitForVisible();
    disseminationTransaction.transactionTodayDate.click();
    disseminationTransaction.transactionDateNextDay.waitForVisible();
    disseminationTransaction.transactionDateNextDay.click();
    disseminationTransaction.transactionEmailFilter.waitForVisible();
    browser.setValue("form>div:nth-child(3)>div:nth-child(2) .choices__input--cloned",['cnuusindhu92@gmail.com','Enter']);
    disseminationTransaction.transactionHeading.waitForVisible();
    disseminationTransaction.transactionHeading.click();
    disseminationTransaction.applyFilterBtn.waitForVisible();
    disseminationTransaction.applyFilterBtn.click();
    expect(disseminationTransaction.statusLabel.getText()).to.eql(testData.transaction.statusdeactive);
    browser.url(testData.zeidler.qakiidrecipientpage);
    expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
    tmdRecipant.searchField.waitForVisible();
    browser.setValue("#search",['Sindhum','Enter']);
    tmdRecipant.recipientEditIcon.waitForVisible();
    tmdRecipant.recipientEditIcon.click();
    browser.scroll(0,500);
    expect(tmdRecipant.getEmailAddressName(2).isVisible()).to.eql(false);
    tmdRecipant.logout();
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81152",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81152",
        failObj
        )
      }
    })
});

describe("Dissemination run", () =>{
  it("XLSX file", () =>{
    Login.adminLoginWithCookies();
    disseminationTransaction.transactionForXlsCsv(testData.disseminationlist.automationrulename,testData.transaction.yopemail);
    expect(disseminationTransaction.emailLabel.getText()).to.eql(testData.transaction.yopemail);
    browser.newWindow('http://www.yopmail.com/en/');
    browser.pause(3000);
    var tabIds = browser.getTabIds();
    browser.switchTab(tabIds[1]);
    gmailPage.checkinbox_changeiframe(testData.transaction.yopemail);
    gmailPage.emailContent.waitForVisible();
    expect(gmailPage.emailContent.getText()).to.eql(testData.recipient.emailContent);
    gmailPage.xlsx.waitForVisible();
    console.log("xlsx =" +gmailPage.xlsx.getText());
    var xlsx = gmailPage.xlsx.getText().split(".");
    expect(xlsx[1]).to.eql(testData.alldisseminationtransaction.format);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81146",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81146",
        failObj
        )
      }
    })

  it("CSV file", () =>{
    disseminationTransaction.transactionForXlsCsv(testData.disseminationlist.autorulename,testData.transaction.yopemailcsv);
    disseminationTransaction.emailLabel.waitForVisible();
    expect(disseminationTransaction.emailLabel.getText()).to.eql(testData.transaction.yopemailcsv);
    browser.newWindow('http://www.yopmail.com/en/');
    browser.pause(3000);
    var tabIds = browser.getTabIds();
    browser.switchTab(tabIds[2]);
    gmailPage.checkinbox_changeiframe(testData.transaction.yopemailcsv);
    gmailPage.emailContent.waitForVisible();
    expect(gmailPage.emailContent.getText()).to.eql(testData.recipient.emailContent);
    gmailPage.xlsx.waitForVisible();
    console.log("xlsx =" +gmailPage.xlsx.getText());
    var xlsx = gmailPage.xlsx.getText().split(".");
    expect(xlsx[1]).to.eql(testData.alldisseminationtransaction.csvFormat);
    pass = 1
  });

  it("Add testrail result", async()=>{
    if(pass === 1){
        var successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81147",
        passObj
      )
      pass = 0
    }
    else{
      successMessage = await addTestRailResult(
        testData.testrail.runid,
        "81147",
        failObj
        )
      }
    })

  it("Validation for SFTP recipient with password", ()=>{
    //Login.adminLogin(); 
    Exavault.Login(testData.zeidler.exavaulturl,testData.recipient.username,testData.recipient.sftppassword,testData.zeidler.exavaultfileurl);
    Exavault.getFolderName(203).waitForVisible();
    expect(Exavault.getFolderName(203).getText()).to.eql(testData.recipient.rootdirectoryname);
    Exavault.getFolderName(203).click();
    browser.waitUntil(
      function() {
      return (
        browser.getUrl() === testData.zeidler.exavaultfilesurl1
      )
    },
    30000,
    "Exavault page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.exavaultfilesurl1);
    Exavault.filesName.waitForVisible();
    expect(Exavault.filesName.getText()).to.eql(testData.exavault.filename);
  });

  it("Validation for SFTP recipient for privatekey", ()=>{
    Exavault.getHomeLink(1).waitForVisible();
    Exavault.getHomeLink(1).click();
    browser.waitUntil(
    function() {
    return (
        Exavault.heading.isVisible() === true
    )
    },
    50000,
    "Exavault page is not visible even after 50s"
    );
    waitUntilTrueFalse(
      `.top-header .brand.pull-left g.letters`,
      "50000",
      `Exavault page is not visible even after 50sec`,
      true
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.exavaultfileurl);
    browser.scroll(0,15000);
    // browser.execute(function() {
    //   document.querySelector(`#file-list > div:nth-child(204) .folder.item.draggable .item-name .name-only`).scrollIntoView()
    // });
    // const elem = await $('#file-list > div:nth-child(204) .folder.item.draggable .item-name .name-only');
    // await elem.scrollIntoView();
    browser.scroll(`#file-list > div:nth-child(204) .folder.item.draggable .item-name .name-only`)
    browser.pause(1000);
    Exavault.getFolderName(204).waitForVisible();
    expect(Exavault.getFolderName(204).getText()).to.eql(testData.recipient.rootdirectory);
    Exavault.getFolderName(204).click();
    browser.waitUntil(
      function() {
      return (
        browser.getUrl() === testData.zeidler.exavaultfilesurl
      )
    },
    30000,
    "Exavault page is not visible even after 30s"
    );
    expect(browser.getUrl()).to.eql(testData.zeidler.exavaultfilesurl);
    Exavault.filesName.waitForVisible();
    expect(Exavault.filesName.getText()).to.eql(testData.exavault.filename);
    browser.pause(1000);
  });

  it.skip("Recipients and rule", () =>{  
    browser.newWindow('https://staging.zeidlerlegalservices.com/');
    browser.url(testData.zeidler.qakiidrecipientpage);
    expect(browser.getUrl()).to.eql(testData.zeidler.qakiidrecipientpage);
    // tmdRecipant.deleteRecipient(testData.transaction.transrecipientname);
    // tmdRecipant.deleteRecipient(testData.recipient.recipienteditname);
    // tmdRecipant.deleteRecipient(testData.recipient.contactname);
    // tmdRecipant.deleteRecipient(testData.recipient.recipientnameauto);
    // tmdRecipant.deleteRecipient(testData.recipient.sftprecipientkey);
    // tmdRecipant.deleteRecipient(testData.recipient.sftprecipient);
    // browser.pause(1000);
    // tmdRecipant.logout();
    // Login.login(testData.zeidler.qaadminhome, testData.login.qaclientusername, testData.login.qaclientpwd);
    // expect(browser.getUrl()).to.eql(testData.zeidler.qaadminhome);
    // browser.url(testData.zeidler.qatmdpage);
    // browser.waitUntil(
    //   function() {
    //   return (
    //   tmdRecordingList.zeilderUmHeading.isVisible() === true
    //   )
    // },
    // 30000,
    // "Kiid listing page is not visible even after 30s"
    // );
    // expect(browser.getUrl()).to.eql(testData.zeidler.qatmdpage);
    // tmdRecordingList.zeidlerDropdown.waitForVisible();
    // tmdRecordingList.zeidlerDropdown.click();
    // tmdRecordingList.searchField.waitForVisible();
    // browser.setValue(".dropdown-choices__menu input", [testData.kiidfilteroption.fundname,'Enter']);
    // waitUntilTrueFalse(
    //   `.page-header .col-sm-12.col-lg-auto .nav-item:nth-child(2)`,
    //   "5000",
    //   `Filters is not visible even after 5sec`,
    //   true
    // );
    // tmdRecordingList.getDisseminationLists(2).waitForVisible();
    // tmdRecordingList.getDisseminationLists(2).click();
    // expect(browser.getUrl()).to.eql(testData.zeidler.qadisseminationlistpage);
    // disseminationLists.deleteRule(testData.transaction.transrulename);
    // disseminationLists.deleteRule(testData.disseminationlist.autorulename);
    // disseminationLists.deleteRule(testData.disseminationlist.automationrulename);
    // disseminationLists.deleteRule(testData.disseminationlist.rulenamewithsftpprivatekey);
    // disseminationLists.deleteRule(testData.disseminationlist.rulenamewithsftppwd);
    // waitUntilTrueFalse(
    //   `.navbar-nav > ul > a`,
    //   "5000",
    //   `Filters is not visible even after 5sec`,
    //   true
    // );
    Login.logout();
  });

  it.skip("Delete the record in the TMD page", ()=>{
    Login.adminLogin();
    browser.url(testData.zeidler.tmdapprovallink);
    expect(browser.getUrl()).to.eql(testData.zeidler.tmdapprovallink);
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QCE030000007AU','Enter']);
    tmdAdmin.getIsinNameInApproval(1,2).waitForVisible();
    browser.waitUntil(
      function() {
      return (
        tmdAdmin.getIsinNameInApproval(1,2).getText() === testData.tmdapprovaltab.isinname
      );
      },
      20000,
      "isin is not displayed after 20s"
    );
    expect(tmdAdmin.getIsinNameInApproval(1,2).getText()).to.eql(testData.tmdapprovaltab.isinname);
    tmdAdmin.getDeleteIcon(1,10,5).waitForVisible();
    tmdAdmin.getDeleteIcon(1,10,5).click();
    browser.alertAccept();
    tmdAdmin.searchField.waitForVisible();
    browser.setValue("#search",['QCE030000007AU','Enter']);
    tmdAdmin.noRecordFoundMessage.waitForVisible();
    expect(tmdAdmin.noRecordFoundMessage.getText()).to.eql(testData.flashmessage.norecordsfound);
    browser.pause(1000);
    tmdRecipant.logout();
  });
});
